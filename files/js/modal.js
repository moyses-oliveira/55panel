var msgBox = function () {
    var me = this;
    this.uid = Math.floor((1 + Math.random()) * 0xffffff).toString(16);
    var id = 'modal_' + this.uid;
    this.locate = 'body #' + id;
    this.modal_tpl = '<div class="modal fade" id="' + id + '" tabindex="-1" role="dialog" aria-labelledby="' + id + 'Label" aria-hidden="true"></div>';
    this.modal_inner_tpl =
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '{close}' +
            '<h4 class="modal-title" id="' + id + 'Label"></h4>' +
            '</div>' +
            '<div class="modal-body"></div>' +
            '<div class="modal-footer"></div>' +
            '</div>' +
            '</div>';

    this.button_cancel_tpl = '<button type="button" class="btn btn-default btn-lg bg-red" data-dismiss="modal">{name}</button>';
    this.button_close_tpl = '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    this.button_tpl = '<button type="button" class="btn btn-default {cls}">{name}</button>';
    this.modal_inner_tpl = this.modal_inner_tpl.replace('{close}', this.button_close_tpl);

    this.show = function (title, msg) {
        if ($(me.locate).length < 1)
            $('body').append(me.modal_tpl);

        $(me.locate).html(me.modal_inner_tpl);
        $(me.locate + ' .modal-title').html(title);
        $(me.locate + ' .modal-body').html(msg);
        $(me.locate).modal({backdrop: 'static'});
		
		var body_locate = me.locate + ' .modal-body';
    };
    this.confirm = function (title, msg, funcConfirm) {
        if ($(me.locate).length < 1)
            $('body').append(me.modal_tpl);

        $(me.locate).html(me.modal_inner_tpl);
        $(me.locate + ' .modal-title').html(title);
        $(me.locate + ' .modal-body').html('<div class="confirm-msg">' + msg + '</div>');
        $(me.locate + ' .modal-footer').append(me.button_cancel_tpl.replace('{name}', 'Não'));
        var confirm = '<button type="button" class="btn btn-default btn-lg bg-green" onclick="msgBoxStatic.confirm.functions[\'' + me.uid + '\']();">Sim</button>';
        $(me.locate + ' .modal-footer').append(confirm);

        msgBoxStatic.confirm.functions[me.uid] = funcConfirm;

        $(me.locate).modal('show');
    };
    this.content = function (msg, title, funcAfterRender) {
        title = typeof title == 'undefined' ? '&nbsp;' : title;
        funcAfterRender = typeof funcAfterRender == 'undefined' ? function () {
        } : funcAfterRender;
        me.show(title, msg);
    };

    this.close = function () {
        $(me.locate).modal('hide');
		return false;
    }

}

var msgBoxStatic = {
    delay: 0,
    count: 0,
    modal: new msgBox(),
    confirm: {
        functions: [],
        show: function (title, text, fn) {
            msgBoxStatic.modal.confirm(title, text, fn);
        },
        redirect: function (title, text, url) {
            msgBoxStatic.confirm.show(title, text, function () {
                window.location.href = url;
            });
            return false;
        }
    },
    load: {
        functions: [],
        content: function (elm) {
            var tmp_title = $(elm).attr('title') ? $(elm).attr('title') : null;
            if (tmp_title === null)
                tmp_title = $(elm).attr('_title') ? $(elm).attr('_title') : null;

            var tmp_options = {url: $(elm).attr('href'), dataType: 'html', async: true};
            tmp_options.success = function (html) {
                msgBoxStatic.modal.content(html, tmp_title);
                msgBoxStatic.delay = 0;
            };
            if (msgBoxStatic.delay == 0) {
                msgBoxStatic.delay = 1;
                $.ajax(tmp_options);
            }
            return false;
        },
        url: function(title, content_url) {
            var tmp_options = {url: content_url, dataType: 'html', async: true};
            tmp_options.success = function (html) {
                msgBoxStatic.modal.content(html, title);
                msgBoxStatic.delay = 0;
            };
            if (msgBoxStatic.delay == 0) {
                msgBoxStatic.delay = 1;
                $.ajax(tmp_options);
            }
            return false;
        }
    }
};
msgBoxStatic.extra_modal = new msgBox();




function DivMsgWidget(elm) {
	elm = (typeof elm === "undefined") ? '' : elm + ' ';
	
	$(elm + 'td.action-column .delete, .nav-tabs li.tab-delete a').click(function(){
		var delete_url = $(this).attr('href');
		if(typeof url_before_delete == 'undefined') {
			msgBoxStatic.confirm.redirect('Atenção','Você deseja excluir o item?',$(this).attr('href'));
			return false;
		}
		var id = (/[\S]+delete\/([\d]+)/).exec(delete_url)[1];
		
		$.getJSON(url_before_delete + '/' + id, function(data) {
			if(data.type == 'error') {
				msgBoxStatic.modal.show('Atenção','<h3 class="text-center">' + data.errors.shift() + '</h3>');
			} else if(data.type == 'success') {
				msgBoxStatic.confirm.redirect('Atenção','Você deseja excluir o item?',delete_url);
			}
		}).fail(function() {
			msgBoxStatic.modal.show('Fail','Ocorreu um erro desconhecido no sistema.');
		});
		return false;
	});
	
	$(elm + 'a.load_modal_url').click(function(){
		msgBoxStatic.load.content(this);
		return false;
	});
	
    //Activate tooltips
    $(elm + '[data-toggle=tooltip]').tooltip();

    $(elm + '[data-widget=collapse]').click(function() {
        //Find the box parent        
        var box = $(this).parents(".box").first();
        //Find the body and the footer
        var bf = box.find(".box-body, .box-footer");
        if (!box.hasClass("collapsed-box")) {
            box.addClass("collapsed-box");
            //Convert minus into plus
            $(this).children(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
            bf.slideUp();
        } else {
            box.removeClass("collapsed-box");
            //Convert plus into minus
            $(this).children(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
            bf.slideDown();
        }
    });


    $(elm + '.btn-group[data-toggle=btn-toggle]').each(function() {
        var group = $(this);
        $(this).find(".btn").click(function(e) {
            group.find(".btn.active").removeClass("active");
            $(this).addClass("active");
            e.preventDefault();
        });

    });

    $(elm + '[data-widget=remove]').click(function() {
        //Find the box parent        
        var box = $(this).parents(".box").first();
        box.slideUp();
    });
	$(elm + 'a.update_target').each(function (){
		$(this).click(function(){
			var tmp_target = $(this).attr('target');
			var tmp_options = {type: 'GET', url: $(this).attr('href'), dataType: 'html', async: false};
			tmp_options.success = function (html) {
				$(tmp_target).html(html);
				if(typeof globalFormConfig != 'undefined') globalFormConfig(tmp_target);
				DivMsgWidget(tmp_target);
			};
			$.ajax(tmp_options);
			return false;
		});
	});
}