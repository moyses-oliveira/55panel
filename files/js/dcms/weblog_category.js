var tree = null;
var moduleURL = null;
function reloadTree()
{
	$.ajax({
		url: moduleURL + 'get_json_categories/'
	}).success(function (data) {
		$("#tree").fancytree('getTree').reload(JSON.parse(data));
		$("#tree").fancytree("getRootNode").visit(function(node){
			node.setExpanded(true);
		});
	}).fail(function () {
		location.href = location.href;
	});
}
$(document).ready(function () {
	moduleURL = BASE_URL  + $('#current_module').val() + '/';
	
	
	$('a.add-category').click(function(){
		msgBoxStatic.load.url('Nova Categoria', moduleURL + 'category-create' );
		return false;
	});
	
	
    tree = $("#tree").fancytree({
        source: {
			url: moduleURL + 'get_json_categories/', 
			cache: false,
			complete : function() {
				$("#tree").fancytree("getRootNode").visit(function(node){
					node.setExpanded(true);
				});
			}
		},
        titlesTabbable: true,
        extensions: ["dnd", "table", "gridnav", "persist"],
        persist: {
            store: "session"
        },
        dnd: {
            preventVoidMoves: true,
            preventRecursiveMoves: true,
            autoExpandMS: 400,
            focusOnClick: false,
            dragStart: function (node, data) {
                return true;
            },
            dragEnter: function (node, data) {
                return true;
            },
            dragDrop: function (node, data) {
	
			
                var inte = node.parent;
				
				data.otherNode.moveTo(node, data.hitMode);
				
                var list = [];
                var p = 0;
								
				var hasLevel3 = false;
				var top = node;
				while(top.parent != null)
					top = top.parent;
				
				$.each(top.children, function(_k, _v){
					list.push({wblcat_int_id: _v.data.id, wblcat_int_parent: null});
					if(_v.children != null) {
						$.each(_v.children, function(k, v){
							list.push({wblcat_int_id: v.data.id, wblcat_int_parent: _v.data.id});
							if(v.children != null) { hasLevel3 = true; }
						});
					}
				});
				
				if(hasLevel3){
					reloadTree();
					return true;
				}
				
				$.post( moduleURL + 'category_organize', { list:  list } , function (result) {
					if (result != 'ok')
						reloadTree();
					else
						$.Storage.saveItem('auth_reload', true);
					return false;
				});
					
                return true;
            }
        },
        table: {indentation: 20, nodeColumnIdx: 0},
        gridnav: {autofocusInput: false, handleCursorKeys: true},
        renderColumns: function (event, data) {
            var node = data.node;
            $tdList = $(node.tr).find(">td");
			$tdList.eq(1).append($('<a/>').addClass('link_action update').attr('href', moduleURL + 'category_update/' + node.data.id).append($('<i/>').addClass('fa fa-pencil')));
            $tdList.eq(1).addClass('action-column').css({'text-align': 'left'});
            $tdList.eq(1).append($('<a/>').addClass('link_action delete').attr('href', moduleURL + 'category_delete/' + node.data.id).append($('<i/>').addClass('fa fa-close')));
			
			$('.link_action.update').unbind('click').click(function () {
				msgBoxStatic.load.url('Nova Categoria', $(this).attr('href') );
				return false;
			});
			$('.link_action.delete').unbind('click').click(function () {
				var me = $(this);
				msgBoxStatic.modal.confirm(
					'Atenção',
					'<h3 class="text-center">Você deseja excluir o item?</h3>', 
					function(){
						msgBoxStatic.modal.show('Atenção', '<h3 class="text-center">Aguarde</h3>');
						$.get(me.attr('href'), function( data ) {
							if(data == 'SUCCESS')
								window.location.href = URI;
							else
								msgBoxStatic.modal.show('Atenção', '<h3 class="text-center">' + data + '</h3>');
						});
					}
				);
				return false;
            });
        },
        click: function (event, data) {
        },
        beforeexpand: function (node, isDeep, isAnim) {
            if (isNodeDblClicked) {
                isNodeDblClicked = false;
                return false;
            }
        }
    });
	
});