$(document).ready(function() {
	$.fn.formModal = function(){
		
		var me = this;
		me.update_target = '#modal_' + msgBoxStatic.modal.uid + ' .modal-body';
		me.SubmitAction = function(form, success_target){
			$(form).submit(function(){
				if(!validator.formValidate(this)) return false;
				var btContent = '';
				var self = $(this);
				var break_url = $(this).attr('action').split('?');
				var submit_url = break_url[0] + '/modal:1' + (typeof break_url[1] != 'undefined' ? '?' + break_url[1] + '%2Fmodal%3A1': '');
				
				var tmp_options = { type: 'POST', url: submit_url, data: $(this).serialize(), dataType: 'html' };
				tmp_options.success = function(html) {
					var data = { }
					try {
						var data = jQuery.parseJSON(html);
						
						if(typeof data.res == 'undefined'){
							alert('Resposta inválida.');
						}
						else if(data.errors.length > 0) {
							validator.showErrorBox(form, data.errors);	
						} 
						else if(typeof data.res.redirect != 'undefined') {
							window.location.href = data.res.redirect;
						} else {
							me.successAction(data.res, success_target);
						}
						if(data.type != 'success') {
							self.find("button[type=submit]").html(btContent)
							self.find("button[type=submit] span").removeClass("fa fa-spinner rotating");
							self.find("button[type=submit]").removeAttr('disabled');
						}
						$('.close').removeAttr('disabled');
						return false;
						
					} catch (err) {
						alert('Resposta inválida.');
					}
				}
				btContent = self.find("button[type=submit]").html();
                $(this).find("button[type=submit]").attr("disabled", "disabled");
				$('.close').attr('disabled', 'disabled');
                $(this).find("button[type=submit]").html("<span></span>")
                $(this).find("button[type=submit] span").addClass("fa fa-spinner rotating");
				
				
				$.ajax(tmp_options);
				return false;
			});
			
		};
		
		me.successAction = function(success_url, success_target){
			var tmp_options = { url: success_url, dataType: 'html' };
			tmp_options.success = function(html) {
				$(success_target).html(html);
				globalFormConfig(success_target);
				DivMsgWidget(success_target);
				msgBoxStatic.modal.close();
			};
			$.ajax(tmp_options);
		}
		
		me.each(function(tag_a) {
			var break_url = $(this).attr('href').split('?');
			var url = break_url[0] + '/modal:1' + (typeof break_url[1] != 'undefined' ? '?' + break_url[1] : '');
			$(this).attr('href', url);
			$(this).click(function() {
				var success_target = $(this).attr('success') ? $(this).attr('success') :  'body>div>.right-side>section.content>div';
				var tmp_title =  $(this).attr('title') ? $(this).attr('title') : null;
				if(tmp_title === null)
					tmp_title =  $(this).attr('_title') ? $(this).attr('_title') : null;
				
				var tmp_options = { url: $(this).attr('href'), dataType: 'html', async: false};
				tmp_options.success = function(html) { 
					msgBoxStatic.modal.content(html, tmp_title); 
					msgBoxStatic.delay = 0;
					globalFormConfig(me.update_target);
					DivMsgWidget(me.update_target);
					me.SubmitAction(me.update_target + ' form', success_target);
				};
				if(msgBoxStatic.delay == 0) { msgBoxStatic.delay = 1; $.ajax(tmp_options); }
				return false;
			});
			
		});
		
	}
	$('.load_form_modal').formModal();
})