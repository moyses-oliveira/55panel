function checkAll(check){
	$('.dev-email-messages-list-item input[type=checkbox]').each(function(){
		 $(this).prop('checked', check);
	});
	return false;
}
function getCheckUid(){
	var _uid = []; 
			
	$('.dev-email-messages-list-item input[type=checkbox]:checked').each(function(){
		_uid.push($(this).val());
	});
	return _uid;
}

$(document).ready(function(){
	$('.delete-permanently').click(function(){
		var _uid = getCheckUid()
		if(!_uid.length){
			msgBoxStatic.modal.show('Atenção', '<div class="text-center h3">Nenhum item foi selecionado</div>');
			return false;
		}
		var _url = $(this).attr('href');
		msgBoxStatic.confirm.show('Atenção','<div class="text-center h3">Você tem certeza que deseja excluir definitivamente os e-mails selecionados?</div>',function(){
			msgBoxStatic.modal.show('&nbsp;', '<div class="text-center h3">Aguarde</div>');
			$.ajax({
			  dataType: "json",
			  url: _url,
			  data: {uid: _uid},
			  success: function( data ) {
					if(!!data.success)
						window.location.href = data.redirect;
					else
						alert('Ação não identificada!');
				}
			});
		});
		return false;
	});
	$('.move-sel-to').each(function(){
		$(this).click(function(){
			var _uid = getCheckUid()
			if(!_uid.length){
				msgBoxStatic.modal.show('Atenção', '<div class="text-center h3">Nenhum item foi selecionado</div>');
				return false;
			}
			msgBoxStatic.modal.show('&nbsp;', '<div class="text-center h3">Aguarde</div>');
			$.ajax({
			  dataType: "json",
			  url: $(this).attr('href'),
			  data: {uid: _uid},
			  success: function( data ) {
					if(!!data.success)
						window.location.href = data.redirect;
					else
						alert('Ação não identificada!');
				}
			});
			return false;
		});
	});
	
	
	$('.dev-email-messages-list-item').each(function(){
		var uid = $(this).find('.mail-uid').val()
		$(this).find('.dev-email-messages-list-item-info').click(function(){
			window.location.href = ROOT + 'webmail/read/' + uid;
		});
		
		$(this).find('.dev-email-messages-list-item-toolbar .star').click(function(){
			var flagged = $(this).find('i').hasClass('fa-star');
			var enable = flagged ? 0 : 1;
			var star = $(this);
			star.find('i').addClass(flagged ? 'fa-star-o' : 'fa-star').removeClass(flagged ? 'fa-star' : 'fa-star-o');
			$.getJSON(ROOT + 'webmail/flag/' + uid + '/enable:' + enable, function( data ) {
				if(data.success) {
					star.find('i').addClass(flagged ? 'fa-star-o' : 'fa-star').removeClass(flagged ? 'fa-star' : 'fa-star-o');
				} else {
					alert('erro');
				}
			});
		});
	});
});