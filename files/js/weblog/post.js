var lsPostImg = {
	posUp: function(el){
		return lsPostImg.chPos(el, -1);
	},
	posDown: function(el){
		return lsPostImg.chPos(el, +1);
	},
	chPos: function(el, op) {
		var current = $(el).closest('.box-img').find('input.field_img_position');
		var current_val = current.val() * 1;
		var prev = $('#images .post-gallery input.field_img_position[value=' + (current_val + (1 *op)) + ']').first();
		if(prev.length < 1) return false;
		current.val(prev.val());
		prev.val(current_val);
		lsPostImg.fixPositions();
		return false;
		
	},
	fixPositions: function(){
		$('#images .post-gallery .box-img').sort(function (a, b) {
		   return (($(a).find('.field_img_position').val() * 1) - ($(b).find('.field_img_position').val() * 1));
		}).appendTo('#images .post-gallery');
		
	},
	forcePositions: function() {
		var position = 1;
		// Force positions sequ�nce
		$('#images .post-gallery .field_img_position').each(function(){
			$(this).val(position++);
		});
	}
	
}