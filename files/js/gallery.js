$(document).ready(function() {
	$('.add_img_from_gallery').each(function(){
		$(this).click(function(){
			staticGalleryPlugin.multiple = !(1  * $(this).attr('single'));
			staticGalleryPlugin.target = $(this).attr('target');
			staticGalleryPlugin.input_name = $(this).attr('input_name');
			staticGalleryPlugin.template = $(this).attr('template');
			msgBoxStatic.load.content(this);
			
			return false;
		});
		staticGalleryPlugin.configure($(this).attr('target'));
	});
	
	
	//galleryContainer.find('.box-img .actions a.check').hide();
});

var staticGalleryPlugin = {
	folder: '',
	multiple: true,
	target: '',
	input_name: '',
	template: '',
	templates: {},
	getFiles: function(){
		var list = [];
		$('#file-grid .box-img .actions a.check.on').each(function(){
			var folder = !!staticGalleryPlugin.folder ? staticGalleryPlugin.folder + '/' : '';
			list.push(folder + $(this).closest('.box-img').find('input.filename').val());
		});
		return list;
	},
	update: function(grid_url) {
		oURL.refresh_url = grid_url;
		$("#file-input").fileinput.defaults.uploadUrl = grid_url;
		staticGalleryPlugin.loading();
		$.ajax({ 
			url: grid_url, 
			dataType: 'html', 
			success: function(html) {	
				$('#file-manager-content').html(html);
			}
		});
	},
	loading: function() {
		$('#file-manager-content').html("<div class='col-md-12' style='margin-top: 50px;text-align:center;color: rgba(26, 93, 139, 0.7); font-size:100px;'><i class='fa fa-refresh fa-spin'></i></div>");
	},
	configure: function(target) {
		$(target).find('.box-img').each(function(){
			var boxImg = $(this);
			boxImg.find('a.check').hide();
			boxImg.find('div.actions .delete, div.msg .bg-red, div.msg .bg-green, div.name a').unbind('click');
			boxImg.find('div.actions .delete').click(function(){
				boxImg.closest('.box-img').addClass('remove');
				return false;
			});
			boxImg.find('div.msg .bg-red').click(function(){
				boxImg.closest('.box-img').removeClass('remove');
				return false;
			});
			boxImg.find('div.msg .bg-green').click(function(){
				boxImg.animate({ opacity: 0.0 }, 1000, function() { 
					boxImg.remove();
					$(target).trigger( "afterDelete" );
				});
				return false;
			});
			boxImg.unbind('mouseover').unbind('mouseout');
			boxImg.mouseover(function(){
				$(this).find('div.actions').addClass('show_delete');
				$(this).find('div.name').addClass('active');
			});
			boxImg.mouseout(function(){
				$(this).find('div.actions').removeClass('show_delete');
				$(this).find('div.name').removeClass('active');
			});
			$(this).find('div.name a').click(function(){
				msgBoxStatic.load.content(this);
				return false;
			});
		});
	},
	INIT: function(_folder){
		staticGalleryPlugin.folder = _folder;
		
		if(!staticGalleryPlugin.multiple)
			$('#file-grid .box-img a.check i').addClass('fa-square').removeClass('fa-check');
		
		$('#add-to-list').unbind('click');
		$('#add-to-list').click(function(){
			$('#file-grid .box-img .actions a.check.on').each(function(){
				var boxImg = $(this).closest('.box-img');
				var propName = boxImg.find('input.field_name').val();
				var properties = { 
					input_key: boxImg.find('input.field_pk').val(),
					img: boxImg.find('input.field_img').val(), 
					name: propName, 
					alt: propName, 
					title: propName, 
					thumb: boxImg.find('input.field_thumb').val()
				}
				if(typeof staticGalleryPlugin.templates[staticGalleryPlugin.template] != 'undefined') {
					var htmlData = staticGalleryPlugin.templates[staticGalleryPlugin.template] + '';
					$.each(properties ,function(k,item){
						var key = '{' + k + '}';
						while(htmlData.indexOf(key)  > -1) {
							htmlData = htmlData.replace(key, item);
						}
					});
					if(staticGalleryPlugin.multiple)
						$(staticGalleryPlugin.target).append(htmlData);
					else	
						$(staticGalleryPlugin.target).html(htmlData);
				} else {
					$(staticGalleryPlugin.target).append(boxImg[0].outerHTML);
				}
			});
			staticGalleryPlugin.configure(staticGalleryPlugin.target);
			$(staticGalleryPlugin.target).trigger( "afterInsert" );
			msgBoxStatic.modal.close();
			return false;
		});
		
		$('#file-grid .box-img').each(function(){
			var boxImg = $(this);
			boxImg.find('div.msg .bg-red').click(function(){
				boxImg.closest('.box-img').removeClass('remove');
				return false;
			});
			boxImg.find('div.msg .bg-green').click(function(){
				var filename = boxImg.find('input.field_name').val();
				boxImg.find('.msg').addClass('show-loading');
				$.ajax({ 
					url: oURL.current, 
					dataType: 'html', 
					data: { remove: filename },
					success: function(res) {
						var fail = false;
						try {
							var json = jQuery.parseJSON(res);
							if(!json) fail = 'Erro 500.';
							if(!json.success) fail = 'Erro 400.';
						} catch(err) {
							fail = 'Erro 500.';
						}
						if(fail) {
							boxImg.find('.msg').removeClass('show-loading');
							alert(fail);
							return false;
						}
						boxImg.animate({ opacity: 0.0 }, 1000, function() { 
							boxImg.remove(); 
							$('#file-manager-content .folder-info').toggle($('#file-grid .box-img').length < 1); 
						});
					}
				});
				return false;
			});
			boxImg.find('div.actions .delete').click(function(){
				boxImg.closest('.box-img').addClass('remove');
				return false;
			});
			boxImg.find('a.check').click(function(){
				if($(this).hasClass('on')) {
					$(this).removeClass('on');
				} else {
					if(!staticGalleryPlugin.multiple)
						$('#file-grid .box-img a.check').removeClass('on');
					
					$(this).addClass('on');
				}
				return false;
			});
			boxImg.mouseover(function(){
				$(this).find('div.actions').addClass('show_delete');
				$(this).find('div.name').addClass('active');
			});
			boxImg.mouseout(function(){
				$(this).find('div.actions').removeClass('show_delete');
				$(this).find('div.name').removeClass('active');
			});
			boxImg.find('.name a').click(function(){ return false; });
		});
		
		$('#btn-submit-directory').click(function(){
			var dir_name = $('#field-add_directory-name').val();
			if(!dir_name)
				return false;
				
			staticGalleryPlugin.loading();
			$.ajax({ 
				url: oURL.current,
				type: 'POST',
				data: { add_directory : { name : dir_name } },
				dataType: 'html', 
				success: function(html) {	
					$('#file-manager-content').html(html);
				}
			});
			return false;
		});
		
		$("#file-input").fileinput({
			language: 'pt-BR',
			uploadUrl: oURL.refresh_url, // server upload action
			uploadAsync: true,
			maxFileCount: 100
		}).on('fileuploaded', function(event, data, previewId, index) {
			staticGalleryPlugin.update(oURL.refresh_url);
			return false;
		});
		var imagepmodal = null;
		$('#form-directory').hide();
		
		$("#add_directory, #btn-close-add-dir").click(function () {
			$('#form-select-dir').toggle();
			$('#form-directory').toggle();
			return false;
		});
		
		$("#field-image-componente_site_int_componente").on('change', function () {
			var path = !$(this).val() ? '' : '/p:' + $(this).val();
			staticGalleryPlugin.update(oURL.select_path + path);
		});
		
		$("#rmdir").click(function () {
			staticGalleryPlugin.update($(this).attr('href'));
			return false;
		});
		
		$('#file-grid .box-img input').each(function(){
			$(this).attr('name', staticGalleryPlugin.input_name + $(this).attr('name').substring(2) );
		});
		
	}
}