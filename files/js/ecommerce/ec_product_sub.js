$(document).ready(function(){
	$("buttom.clear-price").click(function(){
		var delete_url = $(this).attr('href');
		if(typeof url_before_delete == 'undefined') {
			msgBoxStatic.confirm.redirect('Atenção','Você deseja limpar o preço deste sub-produto?',$(this).attr('href'));
			return false;
		}
		var id = (/[\S]+delete\/([\d]+)/).exec(delete_url)[1];
		
		$.getJSON(url_before_delete + '/' + id, function(data) {
			if(data.type == 'error') {
				msgBoxStatic.modal.show('Atenção','<h3 class="text-center">' + data.errors.shift() + '</h3>');
			} else if(data.type == 'success') {
				msgBoxStatic.confirm.redirect('Atenção','Você deseja limpar o preço deste sub-produto?',delete_url);
			}
		}).fail(function() {
			msgBoxStatic.modal.show('Fail','Ocorreu um erro desconhecido no sistema.');
		});
		return false;
	});
	$(".filters li").click(function(){
		$(".filters li").addClass("btn-primary").removeClass("btn-success");
		$(this).removeClass("btn-primary").addClass("btn-success");
		$(".filters-tab-group .tab-content").hide();
		$($(this).attr("for")).show();
		return false;
	});
	$(".options li").click(function(){
		$(".options li").addClass("btn-primary").removeClass("btn-success");
		$(this).removeClass("btn-primary").addClass("btn-success");
		$(".options-tab-group .tab-content").hide();
		$($(this).attr("for")).show();
		return false;
	});
	
	$(".filters-tab-group .tab-content").first().show();
	$(".options-tab-group .tab-content").first().show();
});