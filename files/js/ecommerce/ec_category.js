var tree = null;
function reloadTree()
{
	$.ajax({
		url: BASE_URL  + 'ecommerce.ec-category/get_json/'
	}).success(function (data) {
		$("#tree").fancytree('getTree').reload(JSON.parse(data));
		$("#tree").fancytree("getRootNode").visit(function(node){
			node.setExpanded(true);
		});
	}).fail(function () {
		location.href = location.href;
	});
}

$(document).ready(function () {
    tree = $("#tree").fancytree({
        source: {
			url: BASE_URL  + 'ecommerce.ec-category/get_json', 
			cache: false,
			complete : function() {
				$("#tree").fancytree("getRootNode").visit(function(node){
					node.setExpanded(true);
				});
			}
		},
        titlesTabbable: true,
        extensions: ["dnd", "table", "gridnav", "persist"],
        persist: {
            store: "session"
        },
        dnd: {
            preventVoidMoves: true,
            preventRecursiveMoves: true,
            autoExpandMS: 400,
            focusOnClick: false,
            dragStart: function (node, data) {
                return true;
            },
            dragEnter: function (node, data) {
                return true;
            },
            dragDrop: function (node, data) {
	
			
                var inte = node.parent;
				
				data.otherNode.moveTo(node, data.hitMode);
				
                var list = [];
                var p = 0;
								
				var hasLevel3 = false;
				var top = node;
				while(top.parent != null)
					top = top.parent;
				
				$.each(top.children, function(_k, _v){
					list.push({category_int_id: _v.data.id, category_int_parent: null});
					if(_v.children != null) {
						$.each(_v.children, function(k, v){
							list.push({category_int_id: v.data.id, category_int_parent: _v.data.id});
							if(v.children != null) { hasLevel3 = true; }
						});
					}
				});
				
				if(hasLevel3){
					reloadTree();
					return true;
				}
				
				$.post( BASE_URL  + 'ecommerce.ec-category/organize', { list:  list } , function (result) {
					if (result != 'ok')
						reloadTree();
					else
						$.Storage.saveItem('auth_reload', true);
					return false;
				});
					
                return true;
            }
        },
        table: {indentation: 20, nodeColumnIdx: 0},
        gridnav: {autofocusInput: false, handleCursorKeys: true},
        renderColumns: function (event, data) {
            var node = data.node;
            $tdList = $(node.tr).find(">td");
			$tdList.eq(1).append($('<a/>').addClass('link_action').attr('href', BASE_URL  + 'ecommerce.ec-category/update/' + node.data.id).append($('<i/>').addClass('fa fa-pencil')));
            $tdList.eq(1).addClass('action-column').css({'text-align': 'left'});
            $tdList.eq(1).append($('<a/>').addClass('link_action delete').attr('href', BASE_URL  + 'ecommerce.ec-category/delete/' + node.data.id).append($('<i/>').addClass('fa fa-close')));
			
			
			$('.link_action.delete').unbind('click').click(function () {
				var me = $(this);
				msgBoxStatic.modal.confirm(
					'Atenção',
					'Você deseja excluir o item?', 
					function(){
						$.get(me.attr('href'), function( data ) {
							if(data == 'SUCCESS')
								window.location.href = URI;
							else
								msgBoxStatic.modal.show('Atenção', '<h3 class="text-center">' + data + '</h3>');
						});
					}
				);
				return false;
            });
        },
        click: function (event, data) {
        },
        beforeexpand: function (node, isDeep, isAnim) {
            if (isNodeDblClicked) {
                isNodeDblClicked = false;
                return false;
            }
        }
    });
	
});