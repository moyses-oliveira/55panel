var lsProductImg = {
	posUp: function(el){
		return lsProductImg.chPos(el, -1);
	},
	posDown: function(el){
		return lsProductImg.chPos(el, +1);
	},
	chPos: function(el, op) {
		var current = $(el).closest('.box-img').find('input.field_img_position');
		var current_val = current.val() * 1;
		var prev = $('#tab-images .product-gallery input.field_img_position[value=' + (current_val + (1 *op)) + ']').first();
		if(prev.length < 1) return false;
		current.val(prev.val());
		prev.val(current_val);
		lsProductImg.fixPositions();
		return false;
		
	},
	fixPositions: function(){
		$('#tab-images .product-gallery .box-img').sort(function (a, b) {
		   return (($(a).find('.field_img_position').val() * 1) - ($(b).find('.field_img_position').val() * 1));
		}).appendTo('#tab-images .product-gallery');
		
	},
	forcePositions: function() {
		var position = 1;
		// Force positions sequênce
		$('#tab-images .product-gallery .field_img_position').each(function(){
			$(this).val(position++);
		});
	}
	
}

$(document).ready(function(){
	validator.vTypes.push('date_exhibition');
	validator.warnings.date_exhibition = 'O data de <strong>Fim de exibição</strong> deve ser maior que a data de <strong>Início de exibição</strong>.';
	validator.test.date_exhibition = function(v){
		var dtt_start = $('#field-product-product_dtt_start').data('DateTimePicker').viewDate().toDate();
		var dtt_end = $('#field-product-product_dtt_end').data('DateTimePicker').viewDate().toDate();
		
		if(!$('#field-product-product_dtt_end').val().length)
			return true;
		
		return dtt_start < dtt_end;
	}
	
	
	$("ul.steps li a").each(function(){
		$(this).click(function(){
			if($(this).hasClass("act")) return false;
			if($(this).hasClass("disabled")) return false;
			
			
			if(!validator.formValidate($("ul.steps li a.act").first().attr("href"))) return false;
			
			$(".warnings .box-danger").slideUp( "slow");
			$(".tab-content").hide();
			$($(this).attr("href")).show();
			$("ul.steps li a").removeClass("act");
			var disabled_btns = "ul.steps li a[disabled=disabled], ul.steps li button[disabled=disabled]";
			if($(disabled_btns).length > 0)
				$(disabled_btns).first().removeAttr("disabled");
			
			$(this).addClass("act");
			
			var form_action_url = $('#form-product').attr('action').replace( /#.*/, "");
			if(!is_new_product)
				$('#form-product').attr('action', form_action_url + $(this).attr("href") + '-show');
			
			window.location.hash = $(this).attr("href") + '-show';
			$("#tab-filters #tab-filter-0").show();
			return false;
		});
	});
	
	if($("#action-value").val() == 'create') {
		$("ul.steps li a, ul.steps li button").each(function(k,i) {
			if(k > 1) $(this).attr("disabled","disabled");
		});
	}
	
	$(".tab-content").hide();
	if(!window.location.hash) {
		$(".tab-content").first().show(); 
	} else {
		$("ul.steps li a").removeClass("act");
		var hash = window.location.hash;
		hash = hash.substr(0, hash.length - 5);
		$("ul.steps li a[href=" + hash  + "]").addClass("act");
		$(hash).show();
		$('#form-product').attr('action', $('#form-product').attr('action') + window.location.hash);
	}
	/*
	if(!$('#chk-offer').is(':checked'))
		$('fieldset.offer input[type=text]').attr('disabled', 'disabled').addClass('bg-gray');
		
	$('#chk-offer').change(function(){
		if($('#chk-offer').is(':checked'))
			$('fieldset.offer input[type=text]').removeAttr('disabled').removeClass('bg-gray');
		else
			$('fieldset.offer input[type=text]').attr('disabled', 'disabled').addClass('bg-gray');
	});
	*/
	$('#form-product input[type=text].url_or_alias').keyup(function (e) {
		var v = $(this).val();
		if(v.substring(0, 5).toLowerCase() == 'http:') return true;
		if(v.substring(0, 6).toLowerCase() == 'https:') return true;
		$(this).val(v.toLowerCase());
	});
	
	validator.vTypes.push("url_or_alias");
	validator.test.url_or_alias = function(v){
		if(v.substring(0, 5).toLowerCase() == 'http:') return true;
		if(v.substring(0, 6).toLowerCase() == 'https:') return true;
		return validator.test.regex(v, '/^[0-9A-Za-z_\\-\\.]+$/');
	};
	
	$("#tab-filters #tab-filter-0").show();
	$("#tab-filters .filters li").click(function(){
		$("#tab-filters .filters li").addClass("btn-primary").removeClass("btn-success");
		$(this).removeClass("btn-primary").addClass("btn-success");
		$("#tab-filters .tab-content").hide();
		$($(this).attr("for")).show();
		return false;
	});
	$("div.uncheck").click(function(){
		$(this).parent().find("input[type=radio]").attr("checked",false);
		return false;
	});
});