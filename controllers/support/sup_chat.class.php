<?php
class sup_chat extends controller 
{
	public static function CONFIG() {
		static::$base = 'support';
		static::$table_prefix = 'sup_listener';
		static::$list_action = 'support_list_listener';
		static::$save_action = 'support_save_listener';
		static::$delete_action = 'support_delete_listener';
		static::$model_default = 'access_control/support_listener';
		static::$title = 'Atendente';
		static::$view_params = array('listener_int_user');
		static::$grid_params = array('listener_int_user');
	}
	
	
	public static function index() {
		
		H::css('support/supportCenter.css');
		H::js(array('jquery.playSound.js', 'support/supportCenter.js'));
		$api = new API();
		$message = $status = null;
		$comments = array();
		$api->addAction(static::$base, 'support_get_listener_by_user', 'run', array('listener_int_user'=>static::$user->id));
		$res = $api->callMethod();
		if(!!count($res->errors)): echo $res->errors[0]; die; endif;
		
		if(isset($res->data[0]->message)):
			$messages = array('NOT_FOUND' => 'Você não está definido como atendente');
			$message = $messages[$res->message];
			$status = H::DANGER;
		else:
			$listener = current($res->data);
			$_SESSION['listener_int_id'] = $listener->listener_int_id;
			$_SESSION['listener_vrc_name'] = $listener->listener_vrc_name;
		endif;
			
		$api->addAction(static::$base, 'support_get_customer_by_listener', 'run', array('listener_int_id'=>$_SESSION['listener_int_id']));
		$res = $api->callMethod();
		$room_is_open = !!$res->data;
		if($room_is_open):
			$customer_int_id = $res->data[0]->customer_int_id;
			$api->addAction('ecommerce', 'ligue_store_get_customer_by_id', 'run', array('customer_int_id'=>$customer_int_id));
			$res = $api->callMethod();
			$_SESSION['customer'] = $res->data[0]->name;
		else:
			$status = H::INFO;
			$message = 'Aguardando a entrada de um cliente.';
		endif;
		
		
		//$customer = 'CUSTOMER_NAME';
		H::vars(compact('message', 'customer', 'comments', 'room_is_open', 'status'));
		H::config('index.php');

		return static::RENDER(static::$title);
	}
	
	public static function close() {
		$api = new API();
		$params = array();
		$params['listener_int_id'] = $_SESSION['listener_int_id'];
		$params['room_close_status'] = 'CLOSED_BY_LISTENER';
		$api->addAction(static::$base, 'support_close_room', 'run', $params);
		$res = $api->callMethod();
		if(!!count($res->errors)): 
			header("HTTP/1.0 500 Internal error");
			header("Status: 500 Internal error");
			echo $res->errors[0];
			return;
		endif;
		$msg = $res->data[0]->message;
		if($msg != 'OK'):
			header("HTTP/1.0 500 Internal error");
			header("Status: 500 Internal error");
			echo $msg;
			return;
		else:
			header("HTTP/1.1 200 OK");
			echo $msg;
		endif;
	}
	
	public static function open() {
		$api = new API();
		$params = array();
		$params['listener_int_id'] = $_SESSION['listener_int_id'];
		$api->addAction(static::$base, 'support_open_room', 'run', $params);
		$res = $api->callMethod();
		if(!!count($res->errors)): 
			header("HTTP/1.0 500 Internal error");
			header("Status: 500 Internal error");
			echo $res->errors[0];
			return;
		endif;
		$msg = $res->data[0]->message;
		if($msg != 'OPEN'):
			header("HTTP/1.0 500 Internal error");
			header("Status: 500 Internal error");
			echo $msg;
			return;
		else:
			header("HTTP/1.1 200 OK");
			echo $msg;
		endif;
	}
	
	public static function ping() {
		header("HTTP/1.1 200 OK");
		echo 'success';
	}
	
	public static function comments() {
		$api = new API();
		$comment_bgn_id = $_GET['comment_bgn_id'];
		$params = array(
			'listener_int_id' => $_SESSION['listener_int_id'],
			'room_user_int_relationship'=> $_SESSION['listener_int_id'], 
			'room_user_enm_relationship'=> 'LISTENER'
		);
		$api->addAction(static::$base, 'support_get_room_status', 'run', $params );
		if(isset($_POST['comment'])):
			$params = array(
				'room_user_int_relationship'=>$_SESSION['listener_int_id'], 
				'room_user_enm_relationship'=>'LISTENER', 
				'comment_vrc_comment' => $_POST['comment'],
				'last_comment_bgn_id' => $comment_bgn_id
			);
			
			$api->addAction(static::$base, 'support_add_room_comment', 'run', $params );
			$responses = $api->callMethodCombo();
			$res = $responses[1];
		else:
			$params = array(
					'room_user_int_relationship'=>$_SESSION['listener_int_id'], 
					'room_user_enm_relationship'=>'LISTENER', 
					'comment_bgn_id' => $comment_bgn_id
					);
			$api->addAction(static::$base, 'support_list_comments', 'run', $params );
			$responses = $api->callMethodCombo();
			$res = $responses[1];
		endif;
		foreach($responses as $res):
			if(!!count($res->errors)): 
				echo current((array)$res->errors); 
				die; 
			endif;
		endforeach;
		
		$comments = (array)$res->data;
		if(isset($comments[0]->message)):
			$messages = array('NOT_FOUND' => 'Você não está definido como atendente');
			$res = (object)array('comment_bgn_id'=>0, 'comments'=>'', 'status'=>0);
			echo json_encode($res);
			return true;
		endif;
		if(!!$comments) $comment_bgn_id = end($comments)->comment_bgn_id;
		
		$tpl1 = '
		<li class="clearfix">
			<div class="chat-body clearfix">
				<div class="header h4 ">
					<small class="text-muted "><i class="fa fa-clock-o"></i> {dtt}</small>
					<strong class="pull-right primary-font col-md-6 text-left">{user}</strong>
				</div>
				<div class="clear"></div>
				<p>{comment}</p>
			</div>
		</li>';

		$tpl2 = '
		<li class="clearfix">
			<div class="chat-body clearfix">
				<div class="header h4 text-success">
					<strong class="primary-font">{user}</strong> 
					<small class="pull-right text-muted col-md-6 text-left"><i class="fa fa-clock-o"></i> {dtt}</small>
				</div>
				<div class="clear"></div>
				<p>{comment}</p>
			</div>
		</li>';
		$htmlComments = '';
		foreach($comments as $cmt):
			$isListener = $cmt->type == 'LISTENER';
			$tpl = $isListener ? $tpl2 : $tpl1;
			$name = $isListener ? $_SESSION['listener_vrc_name'] :  $_SESSION['customer'];
			$htmlComments .= str_replace(array('{dtt}','{user}','{comment}'), array(CData::format('d/m/Y H:i',$cmt->dtt_added),$name, $cmt->comment), $tpl);
		endforeach;
		
		$json = $responses[0]->data[0];
		$json->comment_bgn_id = $comment_bgn_id;
		$json->comments = $htmlComments;
		
		echo json_encode($json);
		return true;
	}
}