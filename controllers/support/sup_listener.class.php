<?php
class sup_listener extends controller 
{
	public static function CONFIG() {
		static::$base = 'support';
		static::$table_prefix = 'sup_listener';
		static::$list_action = 'support_list_listener';
		static::$save_action = 'support_save_listener';
		static::$delete_action = 'support_delete_listener';
		static::$model_default = 'access_control/support_listener';
		static::$title = 'Atendente';
		static::$view_params = array('listener_int_user', 'listener_vrc_name');
		static::$grid_params = array('listener_int_user', 'listener_vrc_name');
	}
	
	
	public static function index() {
		$api = new API();
		$api->addAction(static::$base, static::$table_prefix, 'info');
		$info = $api->callMethod();
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		
		foreach($info->info as $k=>$i)
			if($i->pk)
				$info->pk = $k;
			
		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		$totalRecords = 0;
		
		
		$api->addAction('base', 'acs_list_user', 'run', array('int_start'=>0,'int_limit'=>null,'vrc_name'=>''));
		$list = $api->callMethod();
		if(count($list->data)) $totalRecords = current($list->data)->total_records;
		$info->pagination = new Pagination($current_page,$limit,$totalRecords);
		$list_users = $list->data;
		
		
		$api->addAction(static::$base, static::$list_action, 'run', array('int_start'=>$start,'int_limit'=>$limit,'vrc_name'=>''));
		$list = $api->callMethod();
		if(count($list->data)) $totalRecords = current($list->data)->total_records;
		$info->pagination = new Pagination($current_page,$limit,$totalRecords);
		$info->data = $list->data;
		
		$users = array();
		foreach($list_users as $u)
			$users[$u->user_int_id] = $u->user_vrc_name;
			
		foreach($info->data as &$d)
			$d->listener_int_user = $users[$d->listener_int_user];
			
		
		
		H::vars($info);
		if(!file_exists(H::path().H::file()))
			H::path('views/default/');

		return static::RENDER(static::$title);
	}
		

		
	public static function create() { static::form();}
	public static function update() { static::form(H::cod());}
	private static function form($id = null) {	
		$errors = array();
		$vars = new stdClass();
		static::formJS();
		static::formCSS();
		$api = new API();
		$vars->isUpdated = false;
		
		$api->addAction('base', 'acs_list_user', 'run', array('int_start'=>0,'int_limit'=>null,'vrc_name'=>''));
		$list = $api->callMethod();
		$list_users = $list->data;
		$vars->options = array(array('', 'Selecione um usuário'));
		foreach($list_users as $u)
			$vars->options[] = array($u->user_int_id,$u->user_vrc_name);
		
		$api->addAction(static::$base,static::$table_prefix,'info');
		$info = $api->callMethod();
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		$vars->labels = $vars->types = $vars->errors = array();
		foreach($info->info as $k=>$i):
			$vars->labels[$k] = $i->label;
			$vars->types[$k] = $i->type;
			if($i->pk)
				$pk = $k;
			
		endforeach;
		
		if($id):
			$params = array('pk'=>$id);
			$api->addAction(static::$base,static::$table_prefix,'single', $params);
			$vars->data = $api->callMethod()->data;
		else:
			$vars->data = $info->data;
		endif;
		
		$pathImgs = 'files/image/support/listeners/';
		if(isset($_POST[static::$table_prefix])):
			$data = array();
			
			
			$post = $_POST[static::$table_prefix];
			$post['user_int_id'] = static::$user->id;
			$post['listener_vrc_image'] = $vars->data->listener_vrc_image;
			$post[$pk] = $id;
			$api->addAction(static::$base, static::$save_action, 'run', $post);
			$res = $api->callMethod();
			
			if(count($res->errors)):
				$vars->errors = $res->errors;
			else: 
				$data = current($res->data);
				
				$f = $_FILES['listener_vrc_image'];
				if(strlen($f['tmp_name']) > 0):
					$post = array_merge($post,(array)$data);
					$ext = pathinfo($f['name'], PATHINFO_EXTENSION);
					$img = sprintf('%s.%s', md5($data->listener_int_id), $ext);
					move_uploaded_file($f['tmp_name'], $pathImgs . $img);
					$post['listener_vrc_image'] = $img;
					
					$api->addAction(static::$base, static::$save_action, 'run', $post);
					$res = $api->callMethod();
					$data = current($res->data);
				endif;
				
				
				if(isset($data->message)):
					$msgs = array('REPEATED'=>'Este atendente já esta cadastrado.');
					$vars->errors[] = $msgs[$data->message];
				else:
					$_SESSION['updated'] = true;
					H::redirect(H::module(), 'update', $data->listener_int_id);
				endif;	
			endif;
		endif;
		$vars->isUpdated = isset($_SESSION['updated']) ? $_SESSION['updated'] : false;
		$_SESSION['updated'] = false;
		$vars->allow = !!$id;
		$vars->pathImgs = $pathImgs;
		H::vars($vars);
		H::file('form.php');
		return static::RENDER(static::$title);
	}
	
}