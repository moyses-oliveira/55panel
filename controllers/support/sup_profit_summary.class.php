<?php
class sup_profit_summary extends controller 
{
	public static function CONFIG() {
		static::$base = 'support';
		static::$table_prefix = 'sup_listener';
		static::$list_action = 'support_list_listener';
		static::$save_action = 'support_save_listener';
		static::$delete_action = 'support_delete_listener';
		static::$model_default = 'access_control/support_listener';
		static::$title = 'Atendente';
		static::$view_params = array('listener_int_user');
		static::$grid_params = array('listener_int_user');
	}
	
	
	public static function index() {
		$api = new API();
		static::formJS();
		static::formCSS();
		$summary = $summary_day = $results = null;
		$params = array('int_start'=>0,'int_limit'=>10000);
		$api->addAction(static::$base, 'support_list_listener', 'run', $params);
		$listeners = $api->callMethod()->data;
		if($_GET):
			$params = array();
			$params['dte_start'] = CData::setDateBr($_GET['dte_start']);
			$params['dte_end'] = CData::setDateBr($_GET['dte_end']);
			$params['listener_int_id'] = 4;
			$api->addAction(static::$base, 'support_listener_profit', 'run', $params);
			$api->addAction(static::$base, 'support_listener_profit_results_day', 'run', array());
			$api->addAction(static::$base, 'support_listener_profit_results', 'run', array());
			$res = $api->callMethodCombo();
			$summary = array_shift($res)->data;
			$summary_day = array_shift($res)->data;
			$results = array_shift($res)->data[0];
		endif;
		H::vars(compact('summary','summary_day','results','listeners'));
		H::config('index.php');

		return static::RENDER(static::$title);
	}
}