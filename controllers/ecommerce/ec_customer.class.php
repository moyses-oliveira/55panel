<?php
class ec_customer extends controller
{

    public static $table_prefix = 'ec_customer';
    public static $title = 'Clientes';

    public static function CONFIG()
    {
        H::vars(array('table_prefix' => static::$table_prefix));
    }

    public static function index()
    {	
		$vars = new stdClass();
		$api = new API();
        H::css(array('form.css'));
		
		$api->addAction('ecommerce','ec_customer','info');
		$ec_maker_info = $api->callMethod();
		if(!!count($ec_maker_info->errors)):
			echo $ec_maker_info->errors[0];
			die;
		endif;
		$vars->info = $ec_maker_info->info;
				
		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		$totalRecords = 0;
		
		$params = array('int_access'=>ACESSO, 'int_start'=>$start, 'int_limit'=>$limit, 'vrc_name'=>@$_REQUEST['find'] );
		$api->addAction('ecommerce','ec_list_customer','run', $params);
		$vars->data = $api->callMethod()->data;
		
        H::vars($vars);
        H::config('index.php');
        return static::RENDER(static::$title, 'Lista');
    }
	
	private static function formFiles()
    {
        H::js(array(
            'form.js',
            'jquery/jquery.migrate.1.2.1.js',
            'jquery/jquery.metadata.js',
            'jquery/jquery.ui.core.js',
            'jquery/jquery.ui.widget.js',
            'jquery/jquery.ui.datepicker.js',
            'jquery/jquery.ui.datepicker-pt-BR.js',
            'datepicker.invoke.js',
            'updateSelect.js',
            'chosen/chosen.jquery.js',
            'jquery/jquery.maskMoney.js'
        ));

        H::css(array('form.css', 'boxes.css', 'smoothness/jquery-ui.css', 'chosen/chosen.css'));
    }
	
	public static function update() {
		$api = new API();
		$id = H::cod();
		if(!$id)
			return URL::er404();
		
		$params = array('customer_int_id'=>$id);
		$api->addAction('ecommerce','ec_get_customer','run', $params);
		
		if(!count($api->callMethod()->data))
			return URL::er404();
		
		static::formFiles();
		
		$errors=array();
		$update = false;
		if(isset($_POST[static::$table_prefix])):
			$params = $_POST[static::$table_prefix];
			$params['customer_int_id'] = $id;
			$api->addAction('ecommerce','ec_update_customer','run', $params);
			$msg = $api->callMethod()->data[0]->message;
			if($msg == 'REPEATED')
				$error[] = 'Este e-mail já está cadastrado.';
			
			$update = true;
		endif;
		
		$params = array('customer_int_id'=>$id);
		$api->addAction('ecommerce','ec_customer','info');
		$api->addAction('ecommerce','ec_get_customer','run', $params);
		$api->addAction('ecommerce','ec_list_address','run', $params);
		$responses = $api->callMethodCombo();
		$info = array_shift($responses)->info;
		$data = array_shift($responses)->data[0];
		$addresses = array_shift($responses)->data;
		$allow=true;
		
		H::vars(compact('info','address_info','data','addresses', 'allow', 'errors', 'update'));
		H::config('form.php');
		return static::RENDER(static::$title, 'Lista');
			
	}
	
    public static function delete() {
			$id = H::cod();
			if(!$id)
				return URL::er404();
			
			$api = new API();
			$params = array('customer_int_id'=>$id, 'int_usro_id' => static::$user->id);
			$api->addAction('ecommerce','ec_delete_customer', 'run',$params);
			$res = $api->callMethod();
			
			if(count($res->errors)):
				var_dump($res);
			else:
				$response = current($res->data)->message;
				if($response == 'SUCCESS'):
					H::redirect(H::module(), 'index');
				else:
					echo $response;
				endif;
			endif;
	}
	
}
