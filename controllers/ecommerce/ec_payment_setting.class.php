<?php
require_once('ec_setting_base.class.php');
class ec_payment_setting extends ec_setting_base
{

    public static $table_prefix = 'setting';
    public static $title = 'Métodos de Pagamento';

    public static function index()
    {	
		$vars = new stdClass();
		$errors = array();
		$api = new API();
		
		
		$fields = array();
		static::loadScripts();
		
		$payment_methods = static::getMethods();
		
		if(!isset($_SESSION['success']))
				$_SESSION['success'] = false;
			
		H::vars(compact('payment_methods', 'fields', 'errors'));
		$vars->success = $_SESSION['success'] ? true : false;
		$_SESSION['success'] = false;
		H::vars($vars);
		H::config('index.php');
		return static::RENDER(static::$title, 'Seleção');
    }
		
	public static function pagseguro() {
		$vars = new stdClass();
		$errors = array();
		$vars->fields = array(
			array('type'=>'TEXT','alias'=>'pagseguro_email','label'=>'E-mail','cls'=>'not_null email','length'=>255),
			array('type'=>'TEXT','alias'=>'pagseguro_token','label'=>'Token','cls'=>'not_null','length'=>255),
			array('type'=>'SELECT','alias'=>'pagseguro_sandbox','label'=>'Credenciais de Teste','cls'=>'','length'=>255,
				'options'=>array(array('n', 'Não'), array('s', 'Sim'))
				)
		);
		
		
		$payment_methods = static::getMethods();
		$vars->payment_method = null;
		foreach($payment_methods as $p)
			if($p->alias == 'pagseguro')
				$vars->payment_method = $p;
		
		if(isset($_GET['chstat'])):
			static::chPaymentStatus($vars->payment_method->payment_int_id, $_GET['chstat'] == 'on' ? 1 : 0);
			H::redirect(H::module(), H::action());
		endif;
		
		
		if(isset($_POST[static::$table_prefix])):
			$config = $_POST[static::$table_prefix];
			foreach($vars->fields as $f)
				if(!isset($config[$f['alias']]))
					$config[$f['alias']] = null;
			
			$gatewayPagSeguro = new paymentGateway('pagseguro', array(
				'client'=>$config['pagseguro_email'],
				'token'=>$config['pagseguro_token']
			), $config['pagseguro_sandbox'] == 's' );
			if(!$gatewayPagSeguro->check_credentials()->valid):
				$errors[] = 'Credencial Inválida';
			endif;
		endif;
		
		static::saveSettings($vars, $errors);
		
		if($vars->success):
			static::chPaymentStatus($vars->payment_method->payment_int_id, 1);
			$vars->payment_method->enabled = 1;
		endif;
		
		H::vars($vars);
		H::config('form-pag-seguro.php');
		return static::RENDER(static::$title, 'Pag Seguro');
	}
		
	public static function mp() {
		$vars = new stdClass();
		$errors = array();
		
		$sandbox_fields = array('type'=>'SELECT','alias'=>'mercadopago_sandbox','label'=>'Credenciais de Teste','cls'=>'','length'=>255,
				'options'=>array(array('n', 'Não'), array('s', 'Sim'))
		);
		$vars->fields = array(
			array('type'=>'TEXT','alias'=>'mercadopago_client','label'=>'Client ID','cls'=>'not_null','length'=>255),
			array('type'=>'TEXT','alias'=>'mercadopago_token','label'=>'Token','cls'=>'not_null','length'=>255)
			#,$sandbox_fields
		);
		
		
		$payment_methods = static::getMethods();
		$vars->payment_method = null;
		foreach($payment_methods as $p)
			if($p->alias == 'mercadopago')
				$vars->payment_method = $p;
		
		if(isset($_GET['chstat'])):
			static::chPaymentStatus($vars->payment_method->payment_int_id, $_GET['chstat'] == 'on' ? 1 : 0);
			H::redirect(H::module(), H::action());
		endif;
		
		
		if(isset($_POST[static::$table_prefix])):
			$config = $_POST[static::$table_prefix];
			foreach($vars->fields as $f)
				if(!isset($config[$f['alias']]))
					$config[$f['alias']] = null;
			
			$gatewayMP = new paymentGateway('mercadopago', array(
				'client'=>$config['mercadopago_client'],
				'token'=>$config['mercadopago_token']
			), false );
			$check_credentials = $gatewayMP->check_credentials();
			if(!$check_credentials->valid):
				$errors[] = 'Credencial Inválida';
			endif;
		endif;
		
		static::saveSettings($vars, $errors);
		
		if($vars->success):
			static::chPaymentStatus($vars->payment_method->payment_int_id, 1);
			$vars->payment_method->enabled = 1;
		endif;
		
		H::vars($vars);
		H::config('form-mp.php');
		return static::RENDER(static::$title, 'Mercado Pago');
	}
	
	public static function deposit(){
		$vars = new stdClass();
		$errors = array();
	
		$default = 
'Banco: Nome do Banco
Agência: xxxxxx
Conta: xxxxxxxxx';


		$payment_methods = static::getMethods();
		$vars->payment_method = null;
		foreach($payment_methods as $p)
			if($p->alias == 'deposit')
				$vars->payment_method = $p;
		
		if(isset($_GET['chstat'])):
			static::chPaymentStatus($vars->payment_method->payment_int_id, $_GET['chstat'] == 'on' ? 1 : 0);
			H::redirect(H::module(), H::action());
		endif;


		$vars->fields = array(
			array('type'=>'TEXTAREA','alias'=>'deposit_acc','label'=>'Dados para depósito','cls'=>'not_null','length'=>9000, 'default'=>$default)
		);
		if(isset($_POST[static::$table_prefix])):
			$config = $_POST[static::$table_prefix];
			foreach($vars->fields as $f)
				if(!isset($config[$f['alias']]))
					$config[$f['alias']] = null;
				
		endif;
		static::saveSettings($vars, $errors);
		
		if($vars->success):
			static::chPaymentStatus($vars->payment_method->payment_int_id, 1);
			$vars->payment_method->enabled = 1;
		endif;
		
		H::vars($vars);
		H::config('form-deposit.php');
		return static::RENDER(static::$title, 'Depósito / Transferência');
	}
	
	private static function getMethods(){
		$api = new API();
		$params = array('chr_language'=>'pt_br' );
		$api->addAction('ecommerce','ec_list_payment','run', $params);
		return $api->callMethod()->data;
		
	}
	private static function chPaymentStatus($payment_int_id, $tny_enabled){
		$api = new API();
		$post_payment_methods = isset($_POST['payment']) ? $_POST['payment'] : array();$params = array('payment_int_id'=>$payment_int_id, 'tny_enabled'=>$tny_enabled);
		$api->addAction('ecommerce','ec_save_payment_enabled','run', $params);
		$response = $api->callMethod();
	}
}
