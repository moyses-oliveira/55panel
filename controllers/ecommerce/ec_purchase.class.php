<?php
class ec_purchase extends controller
{
    public static $table_prefix = 'setting';
    public static $title = 'Controle de Vendas';

    public static function CONFIG()
    {
        H::vars(array('table_prefix' => static::$table_prefix));
    }

	public static function index()
	{
		$vars = new stdClass();
		$api = new API();	
        H::css(array('form.css'));

		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		$totalRecords = 0;
		
		$api->addAction('ecommerce','ec_options_purchase_status','run', array());
		$status = $api->callMethod()->data;
		
		$vars->status = array(array('','Selecione o status.'));
		foreach($status as $s)
			$vars->status[] = array($s->alias, $s->label);
			
		$status = @$_REQUEST['status'];
		if(empty($status)) $status  = null;
		$params = array('int_start'=>$start, 'int_limit'=>$limit, 'int_customer'=>null, 'vrc_name'=>@$_REQUEST['find'], 'vrc_status_alias'=>$status);
		$api->addAction('ecommerce','ec_list_purchase','run', $params);
		$vars->data = $api->callMethod()->data;
		
        H::vars($vars);
        H::config('index.php');
        return static::RENDER(static::$title, 'Lista');
	}

	public static function view()
	{
		# Compra
		$vars = new stdClass();
		$api = new API();
		$path = sprintf('files/personal/%s/comments', URL_ALIAS);
		if(isset($_POST['comment'])):
			$errors = array();
			$params = array();
			$params['purchase_comment_int_id'] = null;
			$params['purchase_comment_int_purchase'] = H::cod();
			$params['purchase_comment_int_sender'] = static::$user->id;
			$params['purchase_comment_tny_customer'] = 0;
			$params['purchase_comment_vrc_file'] = '';
			$params['purchase_comment_vrc_comment'] = $_POST['comment'];
			
			//UPLOAD
			$f = $_FILES['purchase_comment_vrc_file'];
			if(strlen($f['tmp_name']) > 0):
				$ext = pathinfo($f['name'], PATHINFO_EXTENSION);
				
				if(!in_array(strtolower($ext), array('jpg', 'jpeg', 'png', 'bmp', 'gif', 'pdf', 'txt'))):
					$errors[] = 'Extensão do arquivo inválida!';
				else:
					
					if(!file_exists($path)):
						$oldmask = umask(0);
						mkdir($path,0775);
						umask($oldmask);
					endif;
					$newFileName = sprintf('%s.%s', md5(uniqid()), $ext);
					move_uploaded_file($f['tmp_name'], $path . '/' . $newFileName );
				endif;
				$params['purchase_comment_vrc_file'] = $newFileName;
			endif;
			
			if(!count($errors)):
				$api->addAction('ecommerce', 'ec_save_purchase_comment', 'run', $params);
				$res = $api->callMethod();
				$errors = $res->errors;
			endif;
			
			
			if(!!$errors):
				return URL::er500();
			else:
				H::redirect(H::module(), H::action(), H::cod());
			endif;
		endif;

        H::css(array(
			'form.css', 
			'boxes.css'
		));
		
		
		$api->addAction('base', 'acs_list_user', 'run', array('int_start'=>0,'int_limit'=>100000,'vrc_name'=>''));
		$list_users = $api->callMethod()->data;
		$users = array();
		foreach($list_users as $u)
			$users[$u->user_int_id] = $u->user_vrc_name;
		
		

		$api->addAction('ecommerce', 'ec_get_purchase', 'run', array('purchase_int_id' => H::cod(),'customer_int_id' => null));
		$api->addAction('ecommerce', 'ec_list_purchase_item', 'run', array('purchase_int_id' => H::cod()));
		$api->addAction('ecommerce', 'ec_list_purchase_method', 'run', array('purchase_method_int_purchase' => H::cod()));
		$api->addAction('ecommerce', 'ec_list_purchase_comment', 'run', array('purchase_int_id' => H::cod()));
			
		$res = $api->callMethodCombo();

		foreach($res as $r):
			if(!!$r->errors):
				echo URL::er500();
				die;
			endif;
		endforeach;
		
		$vars->purchase = current($res[0]->data);
		
		$vars->items = $res[1]->data; 
		$vars->methods = $res[2]->data; 
		$vars->comments = $res[3]->data; 
		$vars->users = $users;
		$vars->address = $vars->purchase;
		$vars->allow = true;
		$vars->path = URL::root() . $path;
		
		
        H::vars($vars);
        H::config('view.php');
        return static::RENDER(static::$title, 'Lista');
	}
	
	public static function set_status(){
		$params = array(
				'purchase_vrc_hash' => $_REQUEST['hash'],
				'purchase_status_vrc_alias' => URL::friend(3)
		);
		
		$api = new API();		
		$api->addAction('ecommerce', 'ec_purchase_change_status_by_hash', 'run', $params);
		$res = $api->callMethod();
		
		$errors = $res->errors;
		
		if(!!$errors):var_dump($errors); return null; endif;
		
		$res = current($res->data)->res;
		if($res == 'SUCCESS')
			H::redirect(H::module(), 'view', H::cod());
		else
			echo $res;
	}
}