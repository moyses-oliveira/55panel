<?php
require_once('ec_setting_base.class.php');
class ec_shipping_setting extends ec_setting_base
{

    public static $table_prefix = 'setting';
    public static $title = 'Formas de Envio';

    public static function index()
    {	
		$vars = new stdClass();
		$errors = array();
		$api = new API();
		
		$fields = array(
			array('type'=>'TEXT','alias'=>'post_cep','label'=>'CEP do Remetente para cálculo de frete','cls'=>'not_null cep','length'=>255,'default'=>''),
			array('type'=>'TEXT','alias'=>'shipping_fixed','label'=>'Valor do Frete fixo','cls'=>'not_null decimal_2','length'=>11,'default'=>'0.00')
		);
		
		static::saveSettings($vars, $errors);
		
		if(!isset($_SESSION['success']))
			$_SESSION['success'] = false;
		
		
		$params = array('chr_language'=>'pt_br' );
		$api->addAction('ecommerce','ec_list_shipping','run', $params);
		$shipping_methods = $api->callMethod()->data;
		
		if($vars->success):
			$post_shipping_methods = isset($_POST['shipping']) ? $_POST['shipping'] : array();
			
			foreach($shipping_methods as $s):
				$enabled = in_array($s->shipping_int_id, $post_shipping_methods);
				$params = array('shipping_int_id'=>$s->shipping_int_id, 'tny_enabled'=>$enabled);
				$api->addAction('ecommerce','ec_save_shipping_enabled','run', $params);
			endforeach;
			
			$response = $api->callMethodCombo();
			
			$_SESSION['success'] = true;
			H::redirect(H::module(), H::action());
			die;
		endif;
		
		
		H::vars(compact('shipping_methods', 'fields', 'errors'));
		$vars->success = $_SESSION['success'] ? true : false;
		$_SESSION['success'] = false;
		H::vars($vars);
		H::config('index.php');
		return static::RENDER('Formas de Envio', 'Seleção');
    }
	
}
