<?php
class ec_category extends controller
{

    public static $table_prefix = 'cad_category';
    public static $title = 'Categoria';

    public static function CONFIG()
    {
        H::vars(array('table_prefix' => static::$table_prefix));
    }

    public static function index()
    {	
		H::js(array(
            'jquery/jquery.migrate.1.2.1.js',
            'jquery/jquery.metadata.js',
            'jquery/jquery-ui.1.11.min.js',
            'jquery/jquery.rowsorter.min.js',
			'fancytree/jquery.fancytree-all.js',
			'ecommerce/cookie.jquery.js',
			'ecommerce/ec_category.js'
		));
		H::css(array('fancytree/ui.fancytree.min.css'));

		$vars = new stdClass();

        H::vars($vars);
        H::config('index.php');
        return static::RENDER(static::$title, 'Lista');
    }

    public static function view()
    {
		H::css(array('form.css'));
		$vars = new stdClass();
		$api = new API();		
		$params = array('pk'=>H::cod());
		$api->addAction('ecommerce','ec_category','single', $params);
		$vars = $api->callMethod();

		$api->addAction('ecommerce','ec_category_description','info');
		$vars->desc_info = $api->callMethod()->info;
		
		$params = array( 'int_category'=>H::cod() );
		$api->addAction('ecommerce','ec_list_category_description','run', $params);
		$vars->desc_data_list = $api->callMethod()->data;

		if (empty($vars->data))
			return URL::er404();

		$vars->allow = true;
		H::vars($vars);
		H::config('view.php');
		return static::RENDER(static::$title, 'Visualizar');
    }
	
    public static function create() { return static::form('Criar'); }
    public static function update() { return static::form('Editar', H::cod()); }
	/*
    private static function formFiles()
    {
        H::js(array(
            'form.js',
            'jquery/jquery.migrate.1.2.1.js',
            'jquery/jquery.metadata.js',
            'jquery/jquery.ui.core.js',
            'jquery/jquery.ui.widget.js',
            'jquery/jquery.ui.datepicker.js',
            'jquery/jquery.ui.datepicker-pt-BR.js',
            'datepicker.invoke.js',
            'updateSelect.js',
            'chosen/chosen.jquery.js',
            'jquery/jquery.maskMoney.js'
        ));

        H::css(array('form.css', 'boxes.css', 'smoothness/jquery-ui.css', 'chosen/chosen.css'));
    }
	*/
    private static function form($tit, $id = null)
    {	
		static::formJS();
		static::formCSS();
		$vars = new stdClass();
		$api = new API();
		
		$api->addAction('ecommerce','ec_category','info');
		$api->addAction('ecommerce','ec_category_description','info');
		
		$responses =  $api->callMethodCombo();
		$info = array_shift($responses);
		$vars->info = $info->info;
		$info_desc = array_shift($responses);
		$vars->info_desc = $info_desc->info;

		
		$category_int_id = null;
		if(H::action() == 'update'):
			if(!$id)
				return URL::er404();
			
			$params = array('pk'=>$id);
			$api->addAction('ecommerce','ec_category','single', $params);
			$vars->data = $api->callMethod()->data;
			
			if(!$vars->data)
				return URL::er404();
			
			$params = array( 'int_category'=>$id );
			$api->addAction('ecommerce','ec_list_category_description','run', $params);
			$vars->desc_data_list = $api->callMethod()->data;
			
			$vars->allow = true;
		else:
			$vars->data = $info->data;
			$vars->desc_data_list = array();
			$vars->desc_data_list[] = $info_desc->data;
		endif;
		
		$vars->errors = array();
		if(count($_POST)):
			$data = array();
			$int_item_num = 0;
			
			foreach($_POST['desc'] as $i=>$post_desc):
				foreach($post_desc as $j=>$v) $vars->desc_data_list[$i]->{$j} = $v;
				$post_desc['int_category']=$id;
				$post_desc['int_item_num']=$int_item_num;
				$api->addAction('ecommerce','ec_save_category_description','run',$post_desc);
				$int_item_num++;
			endforeach;
			
			$category_params = array(
					'category_int_id' => $id,
					'category_int_priority' => 1,
					'category_tny_enabled' => 1,
					'category_int_parent' => $vars->data->category_int_parent,
					'int_usro_id'=>static::$user->id
				);
			
			$api->addAction('ecommerce','ec_save_category','run', $category_params);
			$responses = $api->callMethodCombo();
			$data = array_pop($responses)->data;

			if(isset($data[0]->message)):
				$vars->errors[] = $data[0]->message;
			endif;
			/*
			foreach($responses as $res):
				if(count($vars->errors)) continue;
				if(count($res->errors)):
					$vars->errors = $res->errors;
				endif;
			endforeach;
			*/
			if(!count($vars->errors)) H::redirect(H::module(), 'index');
		endif;
        H::vars($vars);
		H::config('form.php');
        return static::RENDER(static::$title, $tit);
    }
	
    public static function get_json() {
		$api = new API();
		$params = array('chr_language'=>'pt_br');
		$api->addAction('ecommerce','ec_list_category_tree','run', $params);
		$data = $api->callMethod()->data;
		$json = array();
		$parent = null;
		foreach($data as $k=>$d):
			if(empty($d->pid)):
				if(!empty($parent))
					$json[] = $parent;
				
				$parent = $d;
				$parent->children = array();
			else:
				$parent->children[] = $d;
			endif;
		endforeach;
		if(!empty($parent))
			$json[] = $parent;
		
		
		echo json_encode((array)$json);
		return;
	}
	
    public static function organize() {
		$api = new API();
		
		$list = isset($_POST['list']) ? $_POST['list'] : array();
		foreach($list as $k=>$params):
			$params['category_int_priority'] = $k;
			$api->addAction('ecommerce','ec_organize_category_tree','run', $params);
		endforeach;
		if(count($list)) $api->callMethodCombo();
	
		return 'ok';
	}
	
    public static function delete() {
			$id = H::cod();
			if(!$id)
				return URL::er404();
			
			$api = new API();
			$params = array('category_int_id'=>$id, 'int_usro_id' => static::$user->id);
			$api->addAction('ecommerce','ec_delete_category', 'run',$params);
			$res = $api->callMethod();
			
			$msgs = array(
				'INVALID_PK'=>'Categoria não encontrada',
				'NOT_EMPTY'=>'Esta categoria não pode ser excluida porque existem produtos cadastrados nela.',
				'PARENT'=>'Esta categoria não pode ser excluida porque possui sub-categorias.',
			);
			if(count($res->errors)):
				var_dump($res);
			else:
				$msg = current($res->data)->message;
				echo $msg == 'SUCCESS' ? $msg : $msgs[$msg];
			endif;
	}
}
