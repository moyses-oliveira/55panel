<?php
class ec_maker extends controller
{

    public static $table_prefix = 'ec_product_maker';
    public static $title = 'Fabricante';

    public static function CONFIG()
    {
        H::vars(array('table_prefix' => static::$table_prefix));
    }

    public static function index()
    {	
		$vars = new stdClass();
		$api = new API();
		$api->addAction('ecommerce','ec_product_maker','info');
		$ec_maker_info = $api->callMethod();
		if(!!count($ec_maker_info->errors)):
			echo $ec_maker_info->errors[0];
			die;
		endif;
		$vars->info = $ec_maker_info->info;
				
		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		$totalRecords = 0;
		
		$params = array('int_start'=>$start, 'int_limit'=>$limit, 'vrc_name'=>null);
		$api->addAction('ecommerce','ec_list_product_maker','run', $params);
		$vars->data = $api->callMethod()->data;
		
        H::vars($vars);
        H::config('index.php');
        return static::RENDER(static::$title, 'Lista');
    }

    public static function create() { return static::form('Criar'); }
    public static function update() { return static::form('Editar', H::cod()); }
	
    private static function form($tit, $id = null)
    {	
		static::formJS();
		static::formCSS();
		$vars = new stdClass();
		$api = new API();
		
		$api->addAction('ecommerce','ec_product_maker','info');
		$info = $api->callMethod();
		$vars->info_label = $info->info;
		$vars->allow = false;
		
		if(H::action() == 'update'):
			if(!$id)
				return URL::er404();
			
			$params = array('pk'=>$id);
			$api->addAction('ecommerce','ec_product_maker','single', $params);
			$vars->data = $api->callMethod()->data;
			$vars->allow = true;
		else:
			$vars->data = $info->data;
		endif;
		
		$vars->errors = array();
		if(isset($_POST['maker'])):
			$data = array();
			$post = $_POST['maker'];
			$post['product_maker_int_id'] = $id;
			$post['product_maker_vrc_logo'] = null;
			$post['int_usro_id'] = static::$user->id;
			$post['chr_flag'] =  H::action() == 'update' ? 'U' : 'I';
			
			$api->addAction('ecommerce','ec_save_product_maker', 'run',$post);
			$res = $api->callMethod();
			
		
			if(count($res->errors)):
				$vars->errors = $res->errors;
			else: 
				$data = current($res->data);
				if(isset($data->message)):
					$msgs = array(
						'REPEATED' => 'Esta marca já esta cadastrada.',
						'INVALID_ACTION' => 'Ação inválida.'
					);
					$vars->errors[] = $msgs[$data->message];
				endif;	
				H::redirect(H::module(), 'index');
			endif;
		endif;
        H::vars($vars);
		H::config('form.php');
        return static::RENDER(static::$title, $tit);
    }

    public static function delete() {
		$id = H::cod();
		if(!$id)
			return URL::er404();
		
		$api = new API();
		$params = array('product_maker_int_id'=>$id, 'int_usro_id' => static::$user->id);
		$api->addAction('ecommerce','ec_delete_product_maker', 'run',$params);
		$res = $api->callMethod();
		
		if(!count($res->errors)):
			$msg = current($res->data)->message;
			echo $msg == 'SUCCESS' ? $msg : $msgs[$msg];
		else:
			echo '500';
		endif;
	}
}
