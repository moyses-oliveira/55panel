<?php
require_once('ec_setting_base.class.php');

class ec_setting extends ec_setting_base {
	
	public static $table_prefix = 'setting';
	public static $settings = null;
		
	public static function index() {
		$vars = new stdClass();
		$errors = array();
	
		$vars->fields = array(
			array('type'=>'TEXT','alias'=>'smtp_name','label'=>'Nome de envio','cls'=>'not_null','length'=>255, 'default'=>'Sistema'),
			array('type'=>'TEXT','alias'=>'smtp_host','label'=>'Host','cls'=>'not_null','length'=>255),
			array('type'=>'TEXT','alias'=>'smtp_email','label'=>'E-mail','cls'=>'not_null mail','length'=>255),
			array('type'=>'PASSWORD','alias'=>'smtp_password','label'=>'Senha','cls'=>'not_null','length'=>255),
			array('type'=>'TEXT','alias'=>'smtp_port','label'=>'Porta','cls'=>'not_null integer','length'=>7, 'default'=>587),
			array('type'=>'SELECT','alias'=>'smtp_secure','label'=>'SMTP Secure','cls'=>'','length'=>255,
				'options'=>array(array('', 'Nenhum'),array('tls', 'TLS'), array('ssl', 'SSL'))
				)
		);
		if(isset($_POST[static::$table_prefix])):
			$config = $_POST[static::$table_prefix];
			foreach($vars->fields as $f)
				if(!isset($config[$f['alias']]))
					$config[$f['alias']] = null;

			$test = static::testSmtp($config);
			if($test->response != 'success'):
				$errors[] = $test->message;
			endif;
		endif;
		static::saveSettings($vars, $errors);
		H::vars($vars);
		H::config('index.php');
		return static::RENDER('Configuração', 'SMTP');
		
	}
	
	public static function sysmsg(){
		$vars = new stdClass();
		$errors = array();
	
		$vars->fields = array();
		$vars->fields[] = 
			array('type'=>'TEXT','alias'=>'sys_message_mail','label'=>'E-mail que receberá mensagens do sistema.','cls'=>'not_null email','length'=>255);
		$vars->fields[] = 
			array('type'=>'TEXT','alias'=>'sys_message_name','label'=>'Nome referente ao e-mail ','cls'=>'not_null','length'=>255);
			
		if(isset($_POST[static::$table_prefix])):
			$post = $_POST[static::$table_prefix];
			$mail = $post['sys_message_mail'];
			$code = substr(md5($mail),3,6);
			$vars->fields[0]['readonly'] = true;
			$vars->fields[1]['readonly'] = true;
			
			$vars->fields[] = 
				array(
					'type'=>'TEXT',
					'alias'=>'sys_code_mail',
					'label'=>'Código de confirmação de e-mail.',
					'cls'=>'not_null',
					'length'=>6
				);
			if(isset($post['sys_code_mail'])):
				if($code != strtolower($post['sys_code_mail'])):
					$errors[] = 'Código de confirmação invalido';
				else:
					unset($vars->fields[2]);
				endif;
			else:
				$errors[] = 'Em alguns minutos você receberá um e-email com um código de validação, insira no campo "Código de confirmação de e-mail"';
				$mails = array($mail);
				$subject = COMPANY . ' - Código de confirmação de e-mail.';
				$msg = 'Código de confirmação: '. $code;
				static::sendNotification($mails,$subject, $msg);
			endif;
		endif;

		
		static::saveSettings($vars, $errors);
		H::vars($vars);
		H::config('index.php');
		return static::RENDER('Configuração', 'Mensagens do Sistema');			
	}
	
	public static function use_terms(){
		static::loadScripts();
		static::formJS(array('tinymce/tinymce.min.js'));
		$vars = new stdClass();
		$vars->errors = array();
		$api = new API();
		if(isset($_POST[static::$table_prefix])):
			$params = $_POST[static::$table_prefix];
			$params['user_int_id'] = static::$user->id;
			$api->addAction('ecommerce', 'ec_save_use_terms', 'run', $params);
			$res = $api->callMethod();
			if(count($res->errors)):
				$vars->errors = $res->errors;
			else: 
				$data = current($res->data);
				$_SESSION['saved'] = true;
				H::redirect(H::module(), 'use_terms');
			endif;
		endif;	
		$api->addAction('ecommerce','ec_get_use_terms','run', array());
		$vars->terms = $api->callMethod()->data[0]->terms;
			
		$vars->saved = isset($_SESSION['saved']) ? $_SESSION['saved'] : false;
		$_SESSION['saved'] = false;
		H::vars($vars);
        H::config('use_terms.php');
        return static::RENDER(static::$title, 'Lista');
	}
	
	
	private static function testSmtp($conf) {
		try { 
			#require '../framework/tools/phpmailer/class.phpmailer.php';
			$conf = (object)$conf;
			$mail = new PHPMailer();
			$mail->isSMTP();
			$mail->SMTPAuth = true;
			$mail->Timeout = 15;
			$mail->SMTPSecure = $conf->smtp_secure;
			$mail->Host = $conf->smtp_host;
			$mail->Port = $conf->smtp_port;
			$mail->Username = $conf->smtp_email;
			$mail->Password = $conf->smtp_password;
			$mail->From = $conf->smtp_email;
			$mail->FromName = $conf->smtp_name;

			if($mail->smtpConnect()){
				$mail->smtpClose();
				return (object)array('response'=>'success', 'message'=>'Salvo com sucesso.');
			} else {
				return (object)array('response'=>'error', 'message'=>'Credencial inválida.');
			}
		} catch(Exception $ex) {
			return (object)array('response'=>'warning', 'message'=>$ex->getMessage());
		}
	}
	
}