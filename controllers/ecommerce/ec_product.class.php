<?php
class ec_product extends controller
{
    public static $table_prefix = 'cad_produto';
    public static $title = 'Produto';

    public static function CONFIG()
    {		
        H::vars(array('table_prefix' => static::$table_prefix, 'subitemaction'=>false));
    }

    public static function index()
    {	
		$vars = new stdClass();
		$api = new API();

        H::css(array('form.css'));

		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		
		$params = array('chr_language'=>'pt_br', 'int_start'=>$start, 'int_limit'=>$limit, 'vrc_name'=>@$_REQUEST['find'] );
		$api->addAction('ecommerce','ec_list_product','run', $params);
		$vars->data = $api->callMethod()->data;

		$totalRecords = count((array)$vars->data) ? current((array)$vars->data)->total_records : 0;
		$vars->pagination = new Pagination($current_page,$limit,$totalRecords);
		
        H::vars($vars);
        H::config('index.php');
        return static::RENDER(static::$title, 'Lista');
    }
	
	
    public static function create() { return static::form('Criar'); }
    public static function update() { return static::form('Editar', H::cod()); }

    private static function formFiles()
    {
		static::formCSS(array(
			'smoothness/jquery-ui.css', 
			'chosen/chosen.css', 
			'fileinput.min.css', 
			'gallery.css',
			'ecommerce/ec_product.css'
		));
		
		static::formJS(array(
			'jquery/jquery.maskMoney.js',
			'fileinput/fileinput.js', 
			'fileinput/fileinput_locale_pt-BR.js',
			'tinymce/tinymce.min.js',
			'gallery.js'
        ));
    }

    private static function form($tit, $id = null)
    {	
		static::formFiles();
		H::js(array('ecommerce/ec_product.js'));
		$vars = new stdClass();
		$api = new API();
		
		/*
        static::loadModel('software_mais/vwe_cad_usuario_model');
		$usuario = new vwe_cad_usuario();
		$usuario->usuario_int_empresa = ACESSO;
		$vars->users = array();
		foreach($usuario->findAll() as $u)
			$vars->users[$u->usuario_int_id] = $u->usuario_vrc_nome;
		*/
		
		$api->addAction('ecommerce','ec_product','info');
			
		$api->addAction('ecommerce','ec_product_price','info',  array( 'int_product' => $id ));
		$api->addAction('ecommerce','ec_product_description','info');
		$api->addAction('ecommerce','ec_product_attribute','info');
		$api->addAction('ecommerce','ec_product_image','info');
		$api->addAction('ecommerce','ec_list_product_sub_filter_item','run', array( 'product_int_id'=>$id, 'product_sub_int_id'=>null, 'chr_language'=>'pt_br' ));
		
		// Executa todas as requisiçoes na API e retorna a lista de repostas
		$list_info = $api->callMethodCombo();
		
		$info = array_shift($list_info);
		$vars->info = $info->info;
		$vars->data = $info->data;
		
		$info_price = array_shift($list_info);
		$vars->info_price = $info_price->info;
		$vars->data_price = $info_price->data;
		
		$info_desc = array_shift($list_info);
		$vars->info_desc = $info_desc->info;
		
		$info_attr = array_shift($list_info);
		$vars->info_attr = $info_attr->info;
		
		$info_img = array_shift($list_info);
		$vars->info_img = $info_img->info;
		
		$filters_res = array_shift($list_info);
		$vars->filter_list = $filters_res->data;

		$category_int_id = null;
		
		$vars->isUpdated = isset($_SESSION['isUpdated']);
		$_SESSION['isUpdated'] = null;
		
		if(H::action() == 'update'):
			if(!$id)
				return URL::er404();
				
			$vars->allow = true;

			$params = array('pk'=>$id);
			$api->addAction('ecommerce','ec_product','single', $params);
			$product = $api->callMethod()->data;
			$vars->data = $product;
			
			if(!$vars->data)
				return URL::er404();
			
			
			$params = array( 'int_product' => $id );
			$api->addAction('ecommerce','ec_get_product_price','run', $params);
			
			$params = array( 'int_product' => $id );
			$api->addAction('ecommerce','ec_list_product_description','run', $params);
			
			$params = array( 'int_product' => $id );
			$api->addAction('ecommerce','ec_list_product_attribute','run', $params);
			
			$params = array( 'int_product' => $id );
			$api->addAction('ecommerce','ec_list_product_image','run', $params);
			
			$params = array( 'int_product' => $id, 'chr_language'=>'pt_br');
			$api->addAction('ecommerce','ec_list_product_sub','run', $params);
			
			$api->addAction('ecommerce','ec_list_product_price_history','run', array( 'int_product' => $id));
			
			// Executa todas as requisiçoes na API e retorna a lista de repostas
			$exec = $api->callMethodCombo();
			
			$vars->data_price =  array_shift($exec)->data[0];
			$vars->desc_data_list = array_shift($exec)->data;
			$vars->attr_data_list = array_shift($exec)->data;
			$vars->img_data_list = array_shift($exec)->data;
			$vars->subproducts = array_shift($exec)->data;
			$vars->price_history = array_shift($exec)->data;
			
		else:
			$vars->desc_data_list = array();
			$vars->desc_data_list[] = $info_desc->data;
			$vars->attr_data_list = array();
			$vars->attr_data_list[] = $info_attr->data;
			$vars->img_data_list = array();
			$vars->subproducts = array();
			$vars->price_history = array();
		endif;
		
		$vars->errors = array();
		if(count($_POST)):
			$data = array();
			$int_item_num = 0;
			if(isset($_POST['desc'])):
				foreach($_POST['desc'] as $i=>$post_desc):
					foreach($post_desc as $j=>$v) $vars->desc_data_list[$i]->{$j} = $v;
					$post_desc['int_product']=$id;
					$api->addAction('ecommerce', 'ec_save_product_description', 'run', $post_desc);
					$int_item_num++;
				endforeach;
			endif;
			
			if(isset($_POST['filter'])):
				foreach($_POST['filter'] as $filters):
					foreach($filters as $filter):
						$api->addAction('ecommerce', 'ec_save_product_sub_filter_item', 'run', array('psf_int_filter_item'=>$filter));
					endforeach;
				endforeach;
			endif;
			
			if(isset($_POST['attr'])):
				foreach($_POST['attr'] as $i=>$post_attr):
					$api->addAction('ecommerce', 'ec_save_product_attribute', 'run', $post_attr);
				endforeach;
			endif;
			
			if(isset($_POST['product_img'])):
				foreach($_POST['product_img'] as $i=>$post_img):
					$post_img['prod_img_vrc_description'] = '';
					$api->addAction('ecommerce', 'ec_save_product_image', 'run', $post_img);
				endforeach;
			endif;
			
			$params = $_POST['product'];
			$params['product_dtt_start'] = CData::setDateTimeBr($params['product_dtt_start']);
			if(!empty($params['product_dtt_end'])) $params['product_dtt_end'] = CData::setDateTimeBr($params['product_dtt_end']);
			$params['product_int_id'] = $id;
			$params['int_usro_id'] = static::$user->id;
			
			$api->addAction('ecommerce','ec_save_product','run',$params);
			$response = (array)$api->callMethodCombo();
			
			$res = end($response);
			if(count($res->errors)):
				$vars->errors = $res->errors;
			else:
				$saveData = current((array)$res->data);
				if(isset($saveData->message)):
					$vars->errors[] = $saveData->message;
				elseif(!empty($id)):
					$_SESSION['isUpdated'] = true;
					H::redirect(H::module(), 'update', $saveData->product_int_id);
				else:
					$_SESSION['isUpdated'] = true;
					H::redirect(H::module(), 'sub-item', $saveData->product_int_id, '?first=1#');
				endif;
			endif;
		endif;
		
		$api->addAction('ecommerce','ec_options_category','run', array('chr_language'=>'pt_br'));
		$categories_res =  $api->callMethod();
		$vars->categories = array(array('','Selecione uma categoria'));
		foreach($categories_res->data as $c) $vars->categories[] = array($c->id, $c->name);
		
		$api->addAction('ecommerce','ec_options_product_maker','run');
		$fab_res =  $api->callMethod();
		$vars->makers = array(array('','Selecione o fabricante'));
		foreach($fab_res->data as $c) $vars->makers[] = array($c->id, $c->name);

        H::vars($vars);
		H::config('form.php');
        return static::RENDER(static::$title, $tit);
    }
	
	public static function sub_item(){
		
		static::formFiles();
		H::js(array('ecommerce/ec_product.js', 'ecommerce/ec_product_sub.js'));
		$vars = new stdClass();
		$api = new API();
		$prid = H::cod();
		$id = URL::friend(3);
		$vars->errors = $vars->errors2 = array();

		$api->addAction('ecommerce','ec_list_product_description','run', array( 'int_product' => $prid ));
		$api->addAction('ecommerce','ec_product_sub','info');
		$api->addAction('ecommerce','ec_product_sub_filter_item','info');
		$api->addAction('ecommerce','ec_product_sub_price','info');
		$api->addAction('ecommerce','ec_options__size_class','run', array());
		$api->addAction('ecommerce','ec_options__weight_class','run', array());
		$api->addAction('ecommerce','ec_list_product_sub_filter_item','run', array( 'product_int_id'=>($id ? $prid : null), 'product_sub_int_id'=>$id, 'chr_language'=>'pt_br' ));
		$api->addAction('ecommerce','ec_list_product_sub_option_item','run', array( 'product_int_id'=>($id ? $prid : null), 'product_sub_int_id'=>$id, 'chr_language'=>'pt_br' ));
		$api->addAction('ecommerce','ec_list_product_sub_price_history', 'run', array('int_product_sub'=>$id));
		$api->addAction('ecommerce','ec_list_product_sub_stock_history','run', array('int_product_sub'=>$id));

		$list_info = $api->callMethodCombo();
		
		$data_prod = array_shift($list_info);
		if(!$data_prod->data)
			return URL::er404();
		
		$vars->product_name =$data_prod->data[0]->product_description_vrc_name;
		$info = array_shift($list_info);
		$vars->info = $info->info;
		$vars->data = $info->data;
		
		$vars->info_filter = array_shift($list_info)->info;
		$info_price = array_shift($list_info);
		$vars->info_price = $info_price->info;
		$vars->data_price = $info_price->data;
		$size_class_res =  array_shift($list_info);
		$weight_class_res =  array_shift($list_info);
		$vars->filter_list = array_shift($list_info)->data;
		$vars->option_list = array_shift($list_info)->data;
		$vars->price_history = array_shift($list_info)->data;
		$vars->stock_history = array_shift($list_info)->data;
		$vars->users = static::getUsers();
		
		$vars->isUpdated = isset($_SESSION['isUpdated']);
		$_SESSION['isUpdated'] = null;

		$vars->size_classes = array();
		
		
		
		foreach($size_class_res->data as $c)
			$vars->size_classes[] = array($c->id, $c->name);
		
		$vars->weight_classes = array();
		foreach($weight_class_res->data as $c)
			$vars->weight_classes[] = array($c->id, $c->name);
		
		$vars->allow = true;
		$vars->subitemaction = true;

		if(!!$id):
			$api->addAction('ecommerce','ec_product_sub','single', array('pk'=>$id));
			$params = array( 'int_product_sub' => $id );
			$api->addAction('ecommerce','ec_get_product_sub_price','run', $params);
			$response = $api->callMethodCombo();
			$product = array_shift($response);
			$price = array_shift($response);
			$vars->data = $product->data;
			if($price->data):
				$vars->data_price = $price->data[0];
			endif;
		endif;
		
		if(isset($_POST['product_sub_price'])):
			$params = $_POST['product_sub_price'];
			$params['product_sub_int_id'] = $id;
			$params['int_usro_id'] = static::$user->id;
			$api->addAction('ecommerce', 'ec_save_product_sub_price', 'run', $params);
			$res = $api->callMethod();
			if(!!$res->errors):
				$er = array(
					'NOT_FOUND'=>'Sub-produto não encontrado.',
					'REPEATED'=>'Esse valor já está cadsatrado para este Sub-produto.');
				$vars->errors2[] = $er[$res->errors[0]];
			else:
				$_SESSION['isUpdated'] = true;
				H::redirect(H::module(), 'sub-item', H::cod(), URL::friend(3));
			endif;
		endif;
		
		if(isset($_POST['product'])):
			$data = array();
			$int_item_num = 0;
			
			if(isset($_POST['filter'])):
				foreach($_POST['filter'] as $filters):
					foreach($filters as $filter):
						$api->addAction('ecommerce', 'ec_save_product_sub_filter_item', 'run', array('psf_int_filter_item'=>$filter));
					endforeach;
				endforeach;
			endif;
			
			if(isset($_POST['option'])):
				foreach($_POST['option'] as $options):
					foreach($options as $option):
						$api->addAction('ecommerce', 'ec_save_product_sub_option_item', 'run', array('psf_int_option_item'=>$option));
					endforeach;
				endforeach;
			endif;
			
			$params = $_POST['product'];
			if(!isset($params['product_sub_dcm_stock_current'])) $params['product_sub_dcm_stock_current'] = '0.000';
			$params['product_sub_int_id'] = $id;
			$params['product_sub_int_product'] = $prid;
			$params['int_usro_id'] = static::$user->id;
			
			$api->addAction('ecommerce', 'ec_save_product_sub', 'run', $params);
			$response = $api->callMethodCombo();
			$res = end($response);
			if(count($res->errors)):
				$vars->errors = $res->errors;
			else:
				$saveData = current((array)$res->data);
				if(isset($saveData->message)):
					$vars->errors[] = $saveData->message;
				else:
					$_SESSION['isUpdated'] = true;
					H::redirect(H::module(), 'sub-item', H::cod(), $saveData->product_sub_int_id);
				endif;
			endif;
		endif;
		
        H::vars($vars);
		H::config('sub-form.php');
        return static::RENDER(static::$title, 'Variação do produto: ' . $vars->product_name);
	}
	
	public static function upgrade_stock(){
		
		$vars = new stdClass();
		$api = new API();
		$id = URL::friend(3);
		
		$api->addAction('ecommerce','ec_product_sub_stock','info');
		$vars->info = $api->callMethod()->info;
		$vars->errors = array();
		
		$params = array('pk'=>$id);
		$api->addAction('ecommerce','ec_product_sub','single', $params);
		$product = $api->callMethod()->data;
		
		if(!$product)
			return URL::er404();
		
		if(isset($_POST['stock'])):
			$params = $_POST['stock'];
			$params['int_usro_id'] = static::$user->id;
			$params['prod_stock_int_product_sub'] = $id;
			$api->addAction('ecommerce','ec_save_product_sub_stock','run', $params);
			$response = $api->callMethod();
			
			$res = array('redirect'=>H::link(H::module(),'sub-item',H::cod(),$id));
			echo static::jsonData($response->errors, $res);
			return true;
			
		endif;
        H::vars($vars);
		H::config('upgrade_stock.php');
        return static::RENDER(static::$title, 'Controle de Estoque');
		
	}
	
	public static function attr(){
		H::render(H::path().'attr.php');
	}

    public static function gallery()
    {
        return static::loadAction('storage/gallery', 'modal',true, array(URL_ALIAS));
    }
	
    public static function delete() {
			$id = H::cod();
			if(!$id)
				return URL::er404();
			
			$api = new API();
			$params = array('product_int_id'=>$id, 'int_usro_id' => static::$user->id);
			$api->addAction('ecommerce','ec_delete_product', 'run',$params);
			$res = $api->callMethod();
			
			if(count($res->errors)):
				var_dump($res);
			else:
				$response = current($res->data)->message;
				if($response == 'SUCCESS'):
					H::redirect(H::module(), 'index');
				else:
					echo $response;
				endif;
			endif;
	}
	
    public static function sub_item_delete() {
		$id = H::cod();
		$sid = URL::friend(3);
		
		$api = new API();
		$params = array('product_sub_int_id'=>$sid, 'int_usro_id' => static::$user->id);
		$api->addAction('ecommerce','ec_delete_product_sub', 'run',$params);
		$res = $api->callMethod();
		
		if(count($res->errors)):
			var_dump($res);
		else:
			$response = current($res->data)->message;
			if($response == 'SUCCESS'):
				H::redirect(H::module(), 'update', H::cod());
			else:
				echo $response;
			endif;
		endif;
	}
	
    public static function sub_item_price_clear() {
		$id = H::cod();
		$sid = URL::friend(3);
		
		$api = new API();
		$params = array('product_sub_int_id'=>$sid, 'int_usro_id' => static::$user->id);
		$api->addAction('ecommerce','ec_clear_product_sub_price', 'run',$params);
		$res = $api->callMethod();
		
		if(count($res->errors)):
			var_dump($res);
		else:
			$response = current($res->data)->message;
			if($response == 'SUCCESS'):
				H::redirect(H::module(), 'sub-item', $id, $sid);
			else:
				echo $response;
			endif;
		endif;
	}
	
	protected static function getUsers() {
		/*
		static::loadModel('software_mais/vwe_cad_usuario_model');
		$usuario = new vwe_cad_usuario();
		$usuario->usuario_int_empresa = ACESSO;
		$users = array();
		foreach($usuario->findAll() as $u)
			$users[$u->usuario_int_id] = $u->usuario_vrc_nome;
		*/
		$users = array();
		return $users;
	}
}
