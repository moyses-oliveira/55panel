<?php
class ec_custom_shipping extends controller
{
	public static $table_prefix = 'custom_shipping';
    public static $title = 'Entrega Customizada';

    public static function CONFIG()
    {
        H::vars(array('table_prefix' => static::$table_prefix));
    }
	
	
	
    public static function index()
    {	
		$vars = new stdClass();
		$api = new API();
		$api->addAction('ecommerce','ec_custom_shipping_region','info');
		$info = $api->callMethod();
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		$vars->info = $info->info;
		
		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		$totalRecords = 0;
		
		$params = array('int_start'=>$start, 'int_limit'=>$limit );
		$api->addAction('ecommerce','ec_list_custom_shipping_region','run', $params);
		$vars->data = $api->callMethod()->data;

	
        H::vars($vars);
        H::config('index.php');
        return static::RENDER(static::$title, 'Lista');
	}
	
	
    public static function create() { return static::form('Criar'); }
    public static function update() { return static::form('Editar', H::cod()); }

	private static function form($tit, $id = null)
    {	
		static::formJS();
		static::formCSS();
		$vars = new stdClass();
		$api = new API();
		
		$res = $api->callMethod();
		$allow = false;
		$api->addAction('ecommerce','ec_custom_shipping_region','info');
		$api->addAction('ecommerce','ec_custom_shipping_region_weight','info');
		$api->addAction('ecommerce', 'select_zone', 'run', array('zone_int_country'=>30));
		$responses = $api->callMethodCombo();
		$saved = @$_SESSION['saved'];
		$_SESSION['saved'] = false;
		$info = $responses[0]->info;
		$info_regw = $responses[1]->info;
		$ZoneData = $responses[2];
		$errors = $ZoneData->errors;
		if(!!$errors):
			echo $errors[0];
			die;
		endif;
		$zones = array(array('','Selecione um estado'));
		foreach($ZoneData->data as $z)
			$zones[] = array($z->key, $z->value);
		
				
		if(isset($_POST['cshr'])):
			$data = array();
			$post = $_POST['cshr'];
			$post['region_int_id'] = $id;
			$post['int_usro_id'] = static::$user->id;
			
			foreach($_POST['regw'] as $region_post):
				$api->addAction('ecommerce','ec_save_custom_shipping_region_weight', 'run', $region_post);
			endforeach;
			
			$api->addAction('ecommerce','ec_save_custom_shipping_region', 'run', $post);
			$responses = $api->callMethodCombo();
			$res = end($responses);
			if(count($res->errors)):
				$errors = $res->errors;
			else: 
				$data = current($res->data);
				if(isset($data->message)):
					$msgs = array(
						'REPEATED' => 'Esta região já esta cadastrada.'
					);
					$errors[] = $msgs[$data->message];
				else:
					$_SESSION['saved'] = true;
					H::redirect(H::module(), 'update', $data->region_int_id);
				endif;	
			endif;
		endif;
		
		if(H::action() == 'update'):
			if(!$id)
				return URL::er404();
			

			$params = array('pk'=>$id);
			$api->addAction('ecommerce','ec_custom_shipping_region','single', $params);
			
			$params = array('regw_int_region'=>$id);
			$api->addAction('ecommerce','ec_list_custom_shipping_region_weight','run', $params);
			$responses = (array)$api->callMethodCombo();
			$data = $responses[0]->data;
			$items = $responses[1]->data;
			if(!$data)
				return URL::er404();
						
			$allow = true;
		else:
			$data = $responses[0]->data;
		endif;
		
        H::vars(compact('allow', 'info', 'info_regw', 'errors', 'data', 'zones', 'items', 'saved'));
		H::config('form.php');
        return static::RENDER(static::$title, $tit);
    }
	
	public static function delete(){
		$id = H::cod();
		if(!$id)
			return URL::er404();
		
		$api = new API();
		$params = array('region_int_id'=>$id, 'int_usro_id' => static::$user->id);
		$api->addAction('ecommerce','ec_delete_custom_shipping_region', 'run',$params);
		$res = $api->callMethod();
		
		if(count($res->errors)):
			var_dump($res);
		else:
			$response = current($res->data)->message;
			if($response == 'SUCCESS'):
				H::redirect(H::module(), 'index');
			else:
				echo $response;
			endif;
		endif;
	}
}