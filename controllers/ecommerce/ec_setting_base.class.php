<?php
class ec_setting_base extends controller {
	
	public static $table_prefix = 'setting';
	public static $settings = null;
	
	public static function CONFIG(){
		H::vars(array('table_prefix' => static::$table_prefix));
	}
	
	
	protected static function loadScripts(){
		static::formJS(array('tinymce/tinymce.min.js'));
		static::formCSS();
	}
	
	
	protected static function saveSettings(&$vars, &$errors){
		static::loadScripts();
		$vars->data = static::getSettings();
		$vars->success = false;
		$vars->errors = $errors;
		$api = new API();
		if(isset($_POST[static::$table_prefix]))
			foreach($_POST[static::$table_prefix] AS $alias=>$value)
				$vars->data->{$alias} = $value;
		
		if(isset($_POST[static::$table_prefix]) && !count($errors)):
			
			foreach($_POST[static::$table_prefix] AS $alias=>$value):
				$params = array('setting_vrc_alias'=>$alias, 'setting_vrc_value'=>$value, 'intUrsoId'=>static::$user->id);
				$api->addAction('ecommerce', 'ec_save_setting', 'run', $params);
			endforeach;
			$responses = $api->callMethodCombo();
			foreach($responses as $res):
				if(!!$res->errors)
					return URL::er500();
					
			endforeach;
			$vars->success = true;
			#H::redirect(H::module(),H::action());
		endif;
	}
	
	protected static function getSettings(){
		if(!static::$settings):
			$api = new API();
			$api->addAction('ecommerce', 'ec_show_setting', 'run', array());
			$res = $api->callMethod();
			static::$settings = current($res->data);
		endif;
		return static::$settings;
	}
}