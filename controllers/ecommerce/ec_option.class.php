<?php
class ec_option extends controller
{

    public static $table_prefix = 'ec_option';
    public static $title = 'Opções';

    public static function CONFIG()
    {
        H::vars(array('table_prefix' => static::$table_prefix));
    }

    public static function index()
    {	
		$vars = new stdClass();
		$api = new API();
		$api->addAction('ecommerce','ec_option','info');
		$ec_option_info = $api->callMethod();
		if(!!count($ec_option_info->errors)):
			echo $ec_option_info->errors[0];
			die;
		endif;
		$vars->info = $ec_option_info->info;
		
		$api->addAction('ecommerce','ec_option_label','info');
		$label_info = $api->callMethod()->info;
		foreach($label_info as $k=>$v) $vars->info->$k = $v;
		
		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		$totalRecords = 0;
		
		$params = array('chr_language'=>'pt_br', 'int_start'=>$start, 'int_limit'=>$limit, 'vrc_name'=>null );
		$api->addAction('ecommerce','ec_list_option','run', $params);
		$vars->data = $api->callMethod()->data;
		
        H::vars($vars);
        H::config('index.php');
        return static::RENDER(static::$title, 'Lista');
    }

    public static function create() { return static::form('Criar'); }
    public static function update() { return static::form('Editar', H::cod()); }

    private static function form($tit, $id = null)
    {	
		static::formJS();
		static::formCSS();
		$vars = new stdClass();
		$api = new API();
		
		$api->addAction('ecommerce','ec_list_language','run');
		$info = $api->callMethod();
		$vars->languages = $info->data;
		
		$vars->flags = array();
		foreach($vars->languages as $l)
			$vars->flags[$l->alias] = $l->flag;
		
		$api->addAction('ecommerce','ec_option','info');
		$info = $api->callMethod();
		$vars->info = $info->info;
		$vars->data = $info->data;
		
		$api->addAction('ecommerce','ec_option_label','info');
		$info = $api->callMethod();
		$vars->info_label = $info->info;
		
		$api->addAction('ecommerce','ec_vwe_option_value_item_language','info');
		$info_value =  $api->callMethod();
		$vars->info_value = $info_value->info;
		
		if(H::action() == 'update'):
			if(!$id)
				return URL::er404();
			

			$params = array('pk'=>$id);
			$api->addAction('ecommerce','ec_option','single', $params);
			$vars->data = $api->callMethod()->data;
			
			if(!$vars->data)
				return URL::er404();
			
			$params = array( 'option_int_id'=>$id );
			$api->addAction('ecommerce','ec_list_option_label','run', $params);
			$vars->label_list = $api->callMethod()->data;
			
			$params = array( 'option_int_id'=>$id );
			$api->addAction('ecommerce','ec_list_option_item','run', $params);
			$vars->item_list = $api->callMethod()->data;
			
			$vars->allow = true;
		else:
			$vars->label_list = array();
			$vars->item_list = array();
			foreach($vars->languages as $l):
				$data = $info->data;
				$data->option_label_chr_language = $l->alias;
				$vars->label_list[] = $info->data;
				
				$item = $info_value->data;
				$item->item_id = md5(rand() . rand());
				$item->lang = $l->alias;
				$item->flag = $l->flag;
				$item->language = $l->name;
				$vars->item_list[] = (object)array('item_id'=>null, 'lang'=>'pt_br', 'value'=>'', 'flag'=>'br.png', 'language'=>'', 'id'=>'' );
			endforeach;
		endif;
		
		$vars->errors = array();
		if(count($_POST)):
			$data = array();
			
			foreach($_POST['label'] as $i=>$post_data):
				foreach($post_data as $j=>$v) $vars->label_list[$i]->{$j} = $v;
				$post_data['int_option']=$id;
				$api->addAction('ecommerce','ec_save_option_label','run',$post_data);
			endforeach;
			
			
			foreach($_POST['value'] as $i=>$post_data):
				#foreach($post_data as $j=>$v) $vars->item_list[$i]->{$j} = $v;
				$api->addAction('ecommerce','ec_save_option_value','run',$post_data);
			endforeach;
			
			foreach($_POST['item'] as $option_item_int_id):
				$api->addAction('ecommerce','ec_save_option_item','run',array('option_item_chr_id'=>$option_item_int_id));
			endforeach;
			
			$params = array( 'option_int_id' => $id, 'option_tny_single' => $_POST['option']['option_tny_single'], 'int_usro_id'=> static::$user->id );
			
			$api->addAction('ecommerce','ec_save_option', 'run',$params);
			$responses = (array)$api->callMethodCombo();
			$res = end($responses);
			if(count($res->errors)) $vars->errors = $res->errors;
			else H::redirect(H::module(), 'index');
			
		endif;
        H::vars($vars);
		H::config('form.php');
        return static::RENDER(static::$title, $tit);
    }
	
    public static function delete() {
		$id = H::cod();
		if(!$id)
			return URL::er404();
		
		$api = new API();
		$params = array('option_int_id'=>$id, 'int_usro_id' => static::$user->id);
		$api->addAction('ecommerce','ec_delete_option', 'run',$params);
		$res = $api->callMethod();
		
		if(!count($res->errors)):
			$msg = current($res->data)->message;
			echo $msg == 'SUCCESS' ? $msg : $msgs[$msg];
		else:
			echo '500';
		endif;
	}
	
}
