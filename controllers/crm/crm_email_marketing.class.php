<?php
require_once('crm_setting_base.class.php');
class crm_email_marketing extends crm_setting_base 
{
	public static $mail = null;
	public static function CONFIG() {
		static::$base = 'crm';
		static::$table_prefix = 'crm_email_template';
		static::$list_action = 'crm_list_email_template';
		static::$save_action = 'crm_save_email_template';
		static::$delete_action = 'crm_delete_email_template';
		static::$title = 'Template';
		static::$view_params = array('tpl_vrc_subject');
		static::$grid_params = array('tpl_vrc_subject');
		H::JS(array('tinymce/tinymce.min.js'));
	}
	
	public static function send() {
		$api = new API();
		$id = H::cod();
		
		$params = array('pk'=>$id);
		$api->addAction(static::$base,static::$table_prefix,'single', $params);
		$data = $api->callMethod()->data;
		$param = array('int_start'=>0, 'int_limit'=>'999999', 'vrc_name'=>null);
		$api->addAction(static::$base,'crm_list_email_group','run', $param);
		$res = $api->callMethod();
		$groups = $res->data;
		var_dump($res);
		$allow = true;
		H::vars(compact('data', 'allow'));
		H::file('send.php');
		return static::RENDER(static::$title, $data->tpl_vrc_subject);
	}
	
	public static function run(){
		$api = new API();
		$id = H::cod();
		$json = (object)array('finish'=>true, 'success'=>true);
		$params = array('pk'=>$id);
		$api->addAction(static::$base,static::$table_prefix, 'single', $params);
		$template_data = $api->callMethod()->data[0];
		
		$setting = static::getSettings();
		
		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		$totalRecords = 0;
		if(!isset($id)):
			echo json_encode(array('finish'=>true, 'success'=>true, 'message'=>'"H::cod()" param cannot be null!'));
		endif;
		if(!isset($_POST['group'])):
			echo json_encode(array('finish'=>true, 'success'=>true, 'message'=>'"group" param not found!'));
		endif;
		$params = array();
		$params['group_int_id'] = $_POST['group'];
		$params['tpl_int_id'] = $id;
		$params['user_int_id'] = static::$user->id;
		$api->addAction('ecommerce','crm_list_email_for_send','run', $params);
		
		$api->addAction('ecommerce','crm_get_email_send_status','run', array());
		$response = (array)$api->callMethodCombo();
		$vars->data = array_shift($response)->data;
		$vars->status = array_shift($response)->data;
		
		foreach($list as $to):
			$json->finish = false;
			$data = new stdClass();
			$data->company = $setting->smtp_company;
			$data->name = $to['name'];
			$data->datetime = date('d/m/Y H:i');
			$data->title = $template_data->tpl_vrc_subject;
			$data->sender = $setting->smtp_email;
			$data->domain = 'www.' . $setting->smtp_host;
			$data->url = 'http://www.' . $setting->smtp_host;
			
			$html = file_get_contents('views/crm/crm_email_marketing/template.html');
			$array_find = $array_replace = array();
			foreach($data as $k=>$v):
				$array_find[] = '{'.$k.'}';
				$array_replace[] = $v;
			endforeach;
			$data->content = str_replace($array_find, $array_replace, $template_data->tpl_txt_template);
			$html = str_replace($array_find, $array_replace, $html);
			
			$json = new stdClass();
			$json->success = static::submitMsg($setting, $to['name'], $to['email'], $template_data->tpl_vrc_subject, $html);
			usleep(50);
		endforeach;
		echo json_encode($json);
		return true;
	}
	
	public static function setting() {
		$vars = new stdClass();
		$errors = array();
		static::$table_prefix = 'setting';
		
		$vars->fields = array(
			array('type'=>'TEXT','alias'=>'smtp_company','label'=>'Nome da Empresa','cls'=>'not_null','length'=>255, 'default'=>''),
			array('type'=>'TEXT','alias'=>'smtp_host','label'=>'Host','cls'=>'not_null','length'=>255),
			array('type'=>'TEXT','alias'=>'smtp_email','label'=>'E-mail','cls'=>'not_null mail','length'=>255),
			array('type'=>'PASSWORD','alias'=>'smtp_password','label'=>'Senha','cls'=>'not_null','length'=>255),
			array('type'=>'TEXT','alias'=>'smtp_port','label'=>'Porta','cls'=>'not_null integer','length'=>7, 'default'=>587),
			array('type'=>'SELECT','alias'=>'smtp_secure','label'=>'SMTP Secure','cls'=>'','length'=>255,
				'options'=>array(array('', 'Nenhum'),array('tls', 'TLS'), array('ssl', 'SSL'))
				)
		);
		if(isset($_POST[static::$table_prefix])):
			$config = $_POST[static::$table_prefix];
			foreach($vars->fields as $f)
				if(!isset($config[$f['alias']]))
					$config[$f['alias']] = null;

			$test = static::testSmtp($config);
			if($test->response != 'success'):
				$errors[] = $test->message;
			endif;
		endif;
		static::saveSettings($vars, $errors);
		H::vars($vars);
		H::config('setting.php');
		return static::RENDER('Configuração', 'SMTP');
		
	}
	
	private static function submitMsg($conf, $toName, $toEmail, $subject, $content){
		$user = static::$user;
		
		// Tipo de mensagem
		static::mailConf($conf);
		static::$mail->clearAddresses();
		static::$mail->addAddress($toEmail, $toName);
		
		// Define a mensagem (Texto e Assunto)
		if(isset($_FILES['attachments'])):
			$files = $_FILES['attachments'];
			foreach($files['name'] as $k=>$name):
				if(!empty($name)):
					static::$mail->addAttachment($files['tmp_name'][$k], $name);
				endif;
			endforeach;
		endif;
		static::$mail->Subject = $subject; // Assunto da mensagem
		static::$mail->Body = $content;
		if (!static::$mail->Send()):
			return new Exception('Erro: ' . static::$mail->ErrorInfo);
			#else:
			#static::$imap->saveMessageInSent('Sent',static::$mail->getSentMIMEMessage());
		endif;
		return true;
	}
	
	private static function testSmtp($conf) {
		try { 
			static::mailConf($conf);

			if(static::$mail->smtpConnect()){
				static::$mail->smtpClose();
				return (object)array('response'=>'success', 'message'=>'Salvo com sucesso.');
			} else {
				return (object)array('response'=>'error', 'message'=>'Credencial inválida.');
			}
		} catch(Exception $ex) {
			return (object)array('response'=>'warning', 'message'=>$ex->getMessage());
		}
	}
	
	private static function mailConf($conf) { 
		if(!is_object(static::$mail)):
			static::$mail = new PHPMailer();
			static::$mail->isSMTP();
			static::$mail->SMTPAuth = true;
			static::$mail->Timeout = 15;
			static::$mail->SMTPSecure = $conf->smtp_secure;
			static::$mail->Host = $conf->smtp_host;
			static::$mail->Port = $conf->smtp_port;
			static::$mail->Username = $conf->smtp_email;
			static::$mail->Password = $conf->smtp_password;
			static::$mail->From = $conf->smtp_email;
			static::$mail->FromName = $conf->smtp_company;
			static::$mail->MessageID = sprintf('%s-%s', md5(uniqid()), $conf->smtp_email);
			static::$mail->IsHTML(true);
			static::$mail->CharSet = 'utf-8';
			static::$mail->smtpConnect();
			#static::$mail->SMTPDebug = 4;
		endif;
	}
	
}