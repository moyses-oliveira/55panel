<?php
class crm_email_template extends controller 
{
	public static function CONFIG() {
		static::$base = 'crm';
		static::$table_prefix = 'crm_email_template';
		static::$list_action = 'crm_list_email_template';
		static::$save_action = 'crm_save_email_template';
		static::$delete_action = 'crm_delete_email_template';
		static::$title = 'Templates';
		static::$view_params = array('tpl_vrc_subject');
		static::$grid_params = array('tpl_vrc_subject');
		H::JS(array('tinymce/tinymce.min.js'));
	}
}