<?php
class crm_email_group extends controller 
{
	public static function CONFIG() {
		static::$base = 'crm';
		static::$table_prefix = 'crm_email_group';
		static::$list_action = 'crm_list_email_group';
		static::$save_action = 'crm_save_email_group';
		static::$delete_action = 'crm_delete_email_group';
		static::$title = 'Grupos';
		static::$view_params = array('group_vrc_name');
		static::$grid_params = array('group_vrc_name');
	}
}