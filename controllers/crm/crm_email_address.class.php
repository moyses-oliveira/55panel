<?php
class crm_email_address extends controller 
{
	public static function CONFIG() {
		static::$base = 'crm';
		static::$table_prefix = 'crm_email_address';
		static::$list_action = 'crm_list_email_address';
		static::$save_action = 'crm_save_email_address';
		static::$delete_action = 'crm_delete_email_address';
		static::$title = 'E-mail';
		static::$view_params = array('email_vrc_name', 'email_vrc_email');
		static::$grid_params = array('email_vrc_name', 'email_vrc_email');
	}

	public static function create() { static::form(); }
	public static function update() { static::form(H::cod()); }
	private static function form($id = null) {	
		$errors = array();
		static::formJS();
		static::formCSS();
		
		$isSaved = !!@$_SESSION['saved'];
		if(isset($_SESSION['saved'])) 
			unset($_SESSION['saved']);
		
		$api = new API();
		$api->addAction(static::$base,static::$table_prefix,'info');
		$info = $api->callMethod();
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		$labels = $types = $errors = $lenght = array();
		foreach($info->info as $k=>$i):
			$labels[$k] = $i->label;
			$types[$k] = $i->type;
			$lenght[$k] = $i->lenght;
			
			if($i->pk)
				$pk = $k;
			
		endforeach;
		
		$api->addAction(static::$base,'crm_options_email_group_rel','run', array('email_int_id'=>H::cod()));
		$options_res = $api->callMethod();
		$groups = $options_res->data;
		if($id):
			$params = array('pk'=>$id);
			$api->addAction(static::$base,static::$table_prefix,'single', $params);
			$data = $api->callMethod()->data;
		else:
			$data = $info->data;
		endif;
		
		
		if(isset($_POST[static::$table_prefix])):
			CObject::getData($data, $_POST[static::$table_prefix]);
			$_POST[static::$table_prefix][$pk] = $id;
			$data = array();
			$post = $_POST[static::$table_prefix];
			$post['user_int_id'] = static::$user->id;
			$api->addAction(static::$base, static::$save_action, 'run', $post);
			$res = $api->callMethod();
			
			if(count($res->errors)):
				$errors = $res->errors;
			else: 
				$data = current($res->data);
				if(isset($data->message)):
					$msgs = array('REPEATED'=>'Este item já está cadastrado.');
					$errors[] = $msgs[$data->message];	
				else:
					$_SESSION['saved'] = true;
					static::add_groups($data->{$pk});
					H::redirect(H::module(), 'update', $data->{$pk});
				endif;
			endif;
		endif;
		$allow = !!$id;
		
		H::vars(compact('labels', 'types', 'lenght', 'errors', 'data', 'isSaved', 'allow', 'groups'));
		H::file('form.php');
		return static::RENDER(static::$title);
	}
	
	private static function add_groups($email_int_id) {
		$api = new API();
		$param = array(
			'email_int_id'=>$email_int_id,
			'user_int_id' => static::$user->id
		);
		$api->addAction(static::$base, 'crm_clear_email_group_rel', 'run', $param);
		$api->callMethod();
		
		if(isset($_POST['group'])):
			foreach($_POST['group'] as $group_int_id):
				$param = array(
					'email_group_int_email'=>$email_int_id,
					'email_group_int_group'=>$group_int_id,
					'user_int_id' => static::$user->id
				);
				$api->addAction(static::$base, 'crm_save_email_group_rel', 'run', $param);
			endforeach;
			$response = (array)$api->callMethodCombo();
			foreach($response as $res):
				if(!$res->data):
					var_dump($response);die;
				endif;
			endforeach;
		endif;
	}
}