<?php
class diary extends controller{
    public static $table_prefix = 'diary';
	
	public static function CONFIG() {
		static::$title = 'Agenda';
        H::vars(array('table_prefix' => static::$table_prefix));
	}
	
	public static function index(){
		static::formJS(array(
            'fullcalendar/moment.min.js',
            'fullcalendar/fullcalendar.js',
            'fullcalendar/lang-all.js'
        ));

        static::formCSS(array('diary/fullcalendar.css'));


		return static::RENDER(static::$title);
	}
	
	public static function eventos(){
		$api = new API();
		$params = array(
			'dte_start'=>$_REQUEST['start'],
			'dte_end'=>$_REQUEST['end'],
			'diary_int_user'=>static::$user->id
		);
		$api->addAction(static::$base, 'acs_list_diary', 'run', $params);
		$data = $api->callMethod()->data;
		
		$result = array();
		foreach($data as $d):
            $a = new stdClass();
            $a->start = $d->diary_vrc_datetime;
            $a->title = $d->diary_vrc_event;
            $a->url = H::link(H::module(), 'view', $d->diary_int_id, 'modal:1');
            $result[] = $a;
        endforeach;
		
		echo json_encode($result);
		return true;
	}
	
    public static function create() { return static::form('Criar'); }
    public static function update() { return static::form('Editar', H::cod()); }
	
    private static function form($tit, $id = null)
    {	
		static::formJS();
		static::formCSS();
		$vars = new stdClass();
		$api = new API();
		
		$api->addAction(static::$base, 'acs_diary','info');
		$info = $api->callMethod();
		$vars->info_label = $info->info;
		$vars->allow = false;
		
		if(H::action() == 'update'):
			if(!$id)
				return URL::er404();
			
			$params = array('diary_int_id'=>$id);
			$api->addAction(static::$base, 'acs_get_diary','run', $params);
			$vars->data = $api->callMethod()->data[0];
			$vars->allow = true;
		else:
			$vars->data = $info->data;
		endif;
		
		$vars->errors = array();
		if(isset($_POST[static::$table_prefix])):
			$data = array();
			$post = $_POST[static::$table_prefix];
			$post['diary_int_id'] = $id;
			$post['diary_int_user'] = static::$user->id;
			$post['user_int_id'] = static::$user->id;
			$post['diary_dte_date'] = CData::setDateBr($post['diary_dte_date']);

			$api->addAction(static::$base, 'acs_save_diary', 'run',$post);
			$res = $api->callMethod();
			
			if(count($res->errors)):
				$vars->errors = $res->errors;
			else: 
				$data = current($res->data);
				if(isset($data->message)):
					$msgs = array(
						'DUPLICATE' => 'Registro duplicado.'
					);
					$vars->errors[] = $msgs[$data->message];
				endif;	
				H::redirect(H::module(), 'index');
			endif;
		endif;
        H::vars($vars);
		H::config('form.php');
        return static::RENDER(static::$title, $tit);
    }
	
	public static function view(){
		$vars = new stdClass();
		$api = new API();
		$id =H::cod();
		$api->addAction(static::$base, 'acs_diary','info');
		$info = $api->callMethod();
		$vars->info_label = $info->info;
		$vars->allow = false;
			
		$params = array('diary_int_id'=>$id);
		$api->addAction(static::$base, 'acs_get_diary','run', $params);
		$vars->data = $api->callMethod()->data[0];

        H::vars($vars);
		return static::RENDER(static::$title);
	}
	
	
	public static function delete() {
		$params = array('pk' => H::cod(), 'user_int_id' => static::$user->id);
		if(!$params['pk'])
			return URL::er500('Pk not found.');
		
		$api = new API();
		$api->addAction(static::$base, 'acs_delete_diary', 'run', $params);
		$data = $api->callMethod();
		
		
		$msgs = array('INVALID_PK'=>'Item não encontrado');
		
		if(!count($data->errors)):
			$msg = current($data->data)->message;
			H::redirect(H::module(), 'index');
		else:
			echo '500';
		endif;
	}
	
}