<?php
class gallery extends controller
{

	public static $table_prefix = 'cad_site';

    public static function CONFIG()
    {
        $linq = new Linq();
        H::vars(array('table_prefix' => self::$table_prefix));
        H::js(array( 'chosen/chosen.jquery.js','fileinput/fileinput.js', 'fileinput/fileinput_locale_pt-BR.js', 'form.js', 'gallery.js' ));
        H::css(array('form.css', 'boxes.css', 'chosen/chosen.css', 'gallery.css', 'fileinput.min.css'));
    }
	
    public static function index() { return self::modal(URL_ALIAS, 'Galeria'); }
	
    public static function modal($root_path = null, $subtitle = null)
    {
		H::path('views/storage/gallery/');
		sleep(1);
		if(!isset($root_path))
			return URL::er404();
		
		$pathGalleryImg = 'files/personal/'. $root_path .'/gallery/images';
		$pathGalleryThumb = 'files/personal/'. $root_path .'/gallery/thumbs';
        $path_root = $pathGalleryImg . '/';
		if (!file_exists($path_root))
            H::mkDir($path_root);
		
		
		if (($path = URL::getVar('p')) != null)
            $path = urldecode($path);
		
		$path_complete = H::path_combine(array($pathGalleryImg, $path));
        $path_url = H::path_combine(array($pathGalleryImg, $path), false, '/');
        $path_thumb = H::path_combine(array($pathGalleryThumb, $path), false, '/');
		
		$current_dirs = from(scandir($path_root))->where('$v ==> is_dir("' . $path_root . '".$v) &&($v != ".") && ($v != "..")')->select('$v')->toList();
		
		$current_dirs_name = array_map('strtolower', $current_dirs);
		$dirs = from($current_dirs)->select('$v ==> array($v, $v)')->toList();
		$dirFiles = from(scandir($path_complete))->where('$v ==> !is_dir("' . $path_complete . '".$v) && ($v != ".") && ($v != "..")')->toList();
		
		
		if(isset($_REQUEST['rmdir'])):
			if(!$path)
				$_SESSION['error'] = array('O diretório \'<strong>Raiz</strong>\' não pode ser excluido!');
				
			if(count($dirFiles))
				$_SESSION['error'] = array(sprintf('Não foi possivel excluir o diretório \'<strong>%s</strong>\' porque ele não está vazio!',$path));
				
			if(!isset($_SESSION['error'])):
				rmdir($path_complete);
			endif;
			H::redirect(H::module(), H::action(), 'modal:1', 'update:1');
			die;
		elseif(isset($_POST['add_directory'])):
			$newDirName  = $_POST['add_directory']['name'];
			if(in_array(strtolower($newDirName), $current_dirs_name))
				$_SESSION['error'] = array(sprintf('Já exite um diretório com o nome \'<strong>%s</strong>\' !', $newDirName));
				
			if(!isset($_SESSION['error'])):
				@H::mkDir(H::path_combine(array($pathGalleryImg, $newDirName)));
				H::redirect(H::module(), H::action(), 'modal:1', 'update:1', 'p:' . $newDirName);
			endif;
			H::redirect(H::module(), H::action(), 'modal:1', 'update:1');
            die;
		endif;
		
		if(isset($_REQUEST['remove'])):
			$remove = H::path_combine(array($pathGalleryImg, $path), false, '/') . $_REQUEST['remove'];
			$exists = file_exists($remove);
			if($exists) unlink($remove);
			ImagePlugin::clearThumbs($remove, trim($path_thumb,'/'));
			
			echo json_encode(array('success'=>$exists));
			return;
		endif;
			
			
        if (isset($_FILES['imagens'])):
            foreach ($_FILES["imagens"]["error"] as $key => $error)
                if ($error == UPLOAD_ERR_OK):
                    if (!file_exists($path_complete)) H::mkDir($path_complete);
					move_uploaded_file($_FILES["imagens"]["tmp_name"][$key], $path_complete .  ImagePlugin::normalizeName($_FILES["imagens"]["name"][$key]));
                endif;
            echo '{}';
            die;
        endif;

        $list = array();
        
        foreach($dirFiles as $l):
			$info = pathinfo($path_url  . $l);
			if(!in_array(strtolower($info['extension']),array('jpg', 'jpeg', 'gif', 'png', 'bmp')))
				continue;
			
			$list[] = (object) array('img' => (!$path ? '' : $path . '/')  . $l,'thumb' => URL::site() . ImagePlugin::resize($path_url  . $l, trim($path_thumb,'/'), 180, 135, false), 'name' => $l, 'site' => $root_path);
		endforeach;
        
        $errors = array();
        if (isset($_SESSION['error']))
        {
            $errors = $_SESSION['error'];
            unset($_SESSION['error']);
        }
        H::vars(array(
			'lista' => $list, 
			'allow' => true, 
			'directories' => array_merge(array(array(null, 'Raiz')), $dirs), 
			'current' => str_replace('/images/', '', $path), 
			'errors' => $errors,
			'path' => $path
		));
			
        H::config('gallery.php');
		$subtitle =  !$subtitle ? 'Galerias' : 'Galerias - ' . $subtitle;
        return self::RENDER(self::$title, $subtitle . ($path ? ' - ' . $path : ''));
    }
}