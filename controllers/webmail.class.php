<?php
class webmail extends controller{
	private static $imap = null;
	
	public static function CONFIG() {
		static::$title = 'Webmail';
	}
	
	protected static function connect() {
		static::formJS();
		static::formCSS(array('email.css'));
		$mailbox = sprintf('%s:993', static::$user->domain);
		$username = static::$user->email;
		$password = Encryption::decode(static::$user->cpass);
		$encryption = 'ssl'; // or ssl or ''
		
		// open connection
		static::$imap = new Imap($mailbox, $username, $password, $encryption);
		if(static::$imap->isConnected()===false):

			echo static::$imap->getError();die;
		endif;
		
		$folders = static::$imap->getFolders();
		$folder = URL::getVar('folder');
		if(empty($folder))
			$folder = 'INBOX';
		
		
		static::$imap->selectFolder($folder);
		$folder_labels = array(
			'INBOX'=>'Caixa de Entrada',
			'INBOX.Sent'=>'E-mail Enviados',
			'INBOX.Junk'=>'Caixa de Spam',
			'INBOX.Trash'=>'Lixeira',
			'INBOX.Archive'=>'Arquivo',
			'INBOX.Drafts'=>'Rascunho',
			'Sent'=>'E-mail Enviados',
			'Junk'=>'Caixa de Spam',
			'Trash'=>'Lixeira',
			'Archive'=>'Arquivo',
			'Drafts'=>'Rascunho'
		);
		
		$folder_icons = array(
			'INBOX'=>'fa-envelope',
			'INBOX.Sent'=>'fa-send',
			'INBOX.Junk'=>'fa-ban',
			'INBOX.Trash'=>'fa-trash',
			'INBOX.Archive'=>'fa-archive',
			'INBOX.Drafts'=>'fa-file-o',
			'Sent'=>'fa-send',
			'Junk'=>'fa-ban',
			'Trash'=>'fa-trash',
			'Archive'=>'fa-archive',
			'Drafts'=>'fa-file-o'
		);
		
		$boxName = isset($folder_labels[$folder]) ? $folder_labels[$folder] : $folder;
		$comp = compact('folders', 'folder', 'folder_labels', 'folder_icons', 'boxName');
		H::vars($comp);
		return $comp ;
	}

	public static function index(){
		$comp = static::connect();
		extract($comp);
		
		H::js(array('webmail/index.js'));
		$current_page = URL::getVar('page');
		if(empty($current_page)) $current_page = 1;
		$limit = 25;
		$start = $limit * ($current_page - 1);
		
		$emails = static::$imap->getMessages($start, $limit);
		$data = array();
		foreach($emails as &$email):
			$item = new stdClass();
			foreach($email as $k=>$v)
				$item->{$k} = $v;
			
			$item->datetime = CData::format('d/m/Y H:i', $item->date);
			
			$data[] = $item;
		endforeach;
		$total = static::$imap->getTotal();
		$pagination = new Pagination($current_page, $limit, $total);
		
		H::vars(compact('pagination', 'data', 'total'));
		return static::RENDER(static::$title, $boxName);
		#H::render(H::path().H::file());
	}
	
	public static function read(){
		$comp = static::connect();
		extract($comp);
		$email = static::$imap->getMessageByUid(H::cod());
		
		H::vars($email);
		return static::RENDER(static::$title);
	}
	
	public static function body(){
		echo base64_decode($_POST['content']);
	}

	public static function flag(){
		static::connect();
		$success = static::$imap->setFlagByUid(H::cod(), "\\Flagged", URL::getVar('enable'));
		echo json_encode(array('success'=> ($success * 1)));
		return;
	}
	
	public static function compose(){
		H::js(array('tinymce/tinymce.min.js', 'bootstrap-filestyle.min.js'));
		$comp = static::connect();
		$act = URL::getVar('action');
		extract($comp);
		$uid = H::cod();
		$reply = false;
		$email = array('to'=>array(), 'from'=>'', 'date'=>null, 'subject'=>'', 'uid'=>null, 'unread'=>false, 'answered'=>false, 'body'=>'' );
			
		if(!!$uid)
			$email = static::$imap->getMessageByUid($uid);
				
		$contacts = array();
		$sub = 'Nova Mensagem';
		switch($act) {
			case 'reply':
			case 'reply-all':
				$sub = 'Resposta de e-mail';
				break;
			case 'forward':
				$sub = 'Encaminhar e-mail';
				break;
		}
		$errors = array();
		$isSuccess = false;
		if(isset($_POST['compose'])):
			$response = static::sendMessage($_POST['compose']);
			if($response !== true):
				$errors[] = $response;
			else:
				$isSuccess = true;
			endif;
		endif;
		
		H::vars(compact('reply', 'email', 'contacts', 'act', 'errors', 'isSuccess'));
		return static::RENDER(static::$title, $sub);
	}
	
	public static function move_to(){
		$comp = static::connect();
		$to = URL::getVar('to');
		$uid = H::cod();
		if(isset($_REQUEST['uid'])):
			static::$imap->moveMessages($_REQUEST['uid'], $to);
			echo json_encode(array('success'=>true, 'redirect' => H::link(H::module(), 'index', 'folder:' . $to)));
			return;
		elseif(!empty($uid)):
			static::$imap->moveMessage($uid, $to );
			H::redirect(H::module(), 'index', 'folder:'.$to);
			return;
		endif;
		echo json_encode(array('success'=>false, 'redirect' => ''));
	}
	
	public static function unseen() {
		$comp = static::connect();
		extract($comp);
		$uid = H::cod();
		if(isset($_REQUEST['uid'])):
			foreach($_REQUEST['uid'] as $uid):
				static::$imap->setUnseenMessage($uid, false);
			endforeach;
			echo json_encode(array('success'=>true, 'redirect' => H::link(H::module(), 'index', 'folder:' . $folder)));
			return;
		elseif(!empty($uid)):
			static::$imap->setUnseenMessage($uid, false);
			H::redirect(H::module(), 'index', 'folder:'.$folder); 
		endif;
		echo json_encode(array('success'=>false, 'redirect' => ''));
	}
	
	public static function delete() {
		$comp = static::connect();
		extract($comp);
		$uid = H::cod();
		if(isset($_REQUEST['uid'])):
			static::$imap->deleteMessages($_REQUEST['uid']);
			echo json_encode(array('success'=>true, 'redirect' => H::link(H::module(), 'index', 'folder:' . $folder)));
			return;
		elseif(!empty($uid)):
			static::$imap->deleteMessage(H::cod());
			H::redirect(H::module(), 'index', 'folder:'.$folder);
			return;
		endif;
		echo json_encode(array('success'=>false, 'redirect' => ''));
	}
	
	public static function attachment(){
		static::connect();
		$uid = H::cod();
		$index = URL::friend(3);
		$att = static::$imap->getAttachment($uid, $index);
		header('Pragma: public');
		header('Expires: 0');
		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename="'.$att['name'].'"');
		header('Content-Type: application/octet-stream');
		header('Content-Type: '.$att['mime']);
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: '.$att['size']);
		header('Connection: close');
		echo $att['content'];
	}
	
	private static function sendMessage($post) {
		$user = static::$user;
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPAuth = true;
		$mail->Timeout = 15;
		$mail->SMTPSecure = 'tls';
		$mail->Host = $user->domain;
		$mail->Port = '587';
		$mail->Username = $user->email;
		$mail->MessageID = sprintf('%s-%s', md5(uniqid()) ,$user->email);
		$mail->Password = Encryption::decode(static::$user->cpass);
		$mail->From = $user->email;
		$mail->FromName = $user->name;
		#$mail->SMTPDebug = 4;
		// Tipo de mensagem
		$mail->IsHTML(true);
		$mail->CharSet = 'utf-8';
		
		$to = static::transformAddress($post['to']);
		if(is_object($to)):
			$mail->addAddress($to->email, $to->name);
		else:
			return $to;
		endif;
		
		foreach($post['cc'] as $_cc):
			if(empty($cc)) continue;
			$to = static::transformAddress($cc);
			if(is_object($to)):
				$mail->addCC($to->email, $to->name);
			else:
				return $to;
			endif;
		endforeach;
			
		// Define a mensagem (Texto e Assunto)
		if(isset($_FILES['attachments'])):
			$files = $_FILES['attachments'];
			foreach($files['name'] as $k=>$name):
				if(!empty($name)):
					$mail->addAttachment($files['tmp_name'][$k], $name);
				endif;
			endforeach;
		endif;
		$mail->Subject = $post['subject']; // Assunto da mensagem
		$mail->Body = $post['content'];
		if (!$mail->Send()):
			return new Exception('Erro: ' . $mail->ErrorInfo);
		else:
			static::$imap->saveMessageInSent('Sent',$mail->getSentMIMEMessage());
		endif;
		return true;
	}
	
	private static function transformAddress($email){
		$o_email = $email;
		$email = trim($email, ' ');
		$matches = array();
		if(!!filter_var($email, FILTER_VALIDATE_EMAIL)):
			return (object)array('email'=>$email, 'name'=>'');
		endif;
		preg_match('/([\S ]+)<([\S]+)>/', $email, $matches);
		if(count($matches) > 2):
			$emailAddress = trim(array_pop($matches), ' ');
			if(!filter_var($emailAddress, FILTER_VALIDATE_EMAIL)):
				return sprintf('"%s" não é um e-mail válido.', htmlentities($o_email));
			endif;
			$name = trim(array_pop($matches), ' ');
			return (object)array('email'=>$emailAddress, 'name'=>$name);
		endif;
		$email = trim($email, '<>');
		if(!!filter_var($email, FILTER_VALIDATE_EMAIL)):
			return (object)array('email'=>$email, 'name'=>'');
		else:
			return sprintf('"%s" não é um e-mail válido.', htmlentities($o_email));
		endif;
	}
}
