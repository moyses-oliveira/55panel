<?php
class app extends controller {
	public static function busca_cep() {
		if(!defined('ACESSO'))
			return false;

		$data = correio::endereco(H::cod());
		echo json_encode($data);
		return true;
	}
	
	public static function er403() {
		header("HTTP/1.0 403 Access denied");
		header("Status: 403 Access denied");
		self::$menu_tabs = 'views/app/menu.php';
		H::path('views/app/');
        H::config('403.php');
        self::RENDER('Acesso negado','403');
		die;
		return true;
	}
	
	public static function er404() {
		header("HTTP/1.0 404 Not Found");
		header("Status: 404 Not Found");
		self::$menu_tabs = 'views/app/menu.php';
		H::path('views/app/');
        H::config('404.php');
        self::RENDER('Página não encontrada','404');
		die;
		return true;
	}
	
	public static function er500($errors = array()) {
		header("HTTP/1.0 500 Internal error");
		header("Status: 500 Internal error");
		self::formJS();
		self::formCSS();
        H::vars(array('errors'=>$errors));
		self::$menu_tabs = 'views/app/menu.php';
		H::path('views/app/');
		H::config('500.php');
        self::RENDER('Erro interno','500');
		die;
		return true;
	}
	
	public function get_external()
	{
		$moduleAction = CCrypt::enc_to_key(base64_decode(URL::friend(2)), str_replace('.', '', $_SERVER['HTTP_HOST']));
		
		$vars = explode('/', $moduleAction);
		
		$module = str_replace('-','_',$vars[0]);
		$module = str_replace('.','/',$module);
		$extract = explode('/',$module);
		if(count($extract) > 1):
			$module = (substr($module,0,5) == 'base/') ? 'software_mais/'.substr($module,5) : 'soft_'.$module;
		endif;
		$action = $vars[1];
		
		return self::loadAction($module, $action, true);
	}
}