<?php
class home extends controller {
	
	public static function CONFIG(){
	}
	
	public static function index() {
        if(!(isset($_SESSION['user']) && !empty($_SESSION['user']))):
            return self::login();
        endif;
		$vars = new stdClass();
		H::vars($vars);
		H::css(array(
			'datepicker/datepicker3.css',
			'home.css'
		));

		H::config('index.php');
		return self::RENDER('Home');
	}
	
	private static function login() {
		$table_prefix = 'usuario';
		H::vars(array('table_prefix' => $table_prefix));
		$errors = array();
		$api = new API();
		$api->addAction('base','acs_user','info');
		$info = $api->callMethod();
		
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		
		$vars = $info;
		$post = isset($_POST[$table_prefix]) ? $_POST[$table_prefix] : null;
		if($post):
			$email = $post['user_vrc_email'];
			$pass = $post['user_vrb_pass'];
			$domain = substr(strrchr($email, "@"), 1);
			$params = array('vrc_email'=>$email, 'vrc_alias' => URL_ALIAS);#, 'vrc_pass' => $post['user_vrb_pass']);
			$api->addAction('base','acs_valid_user','run', $params);
			$login = $api->callMethod();
			
			$mailbox = sprintf('mail.%s:993', $domain);
			// open connection
			
			if(!!count($login->errors)):
				$errors = $login->errors;
			else:
				$user = current($login->data);
				if(isset($user->message)):
					$msgs = array(
						'NOT_FOUND' => 'Usuário não encontrado.',
						'INVALID' => 'Senha incorreta.'
					);
					$errors = array($msgs[$user->message]);
				endif;
			endif;
			
			if(!count($errors)): 
				$imap = new Imap($mailbox, $email, $pass, 'ssl');
				if(!$imap->isConnected()):
					$errors = array('Senha incorreta.'); 
				endif;
			endif;
			if(!count($errors)): 
				static::mkLogin($user, $domain, Encryption::encode($pass));
				H::redirect('home','index');
			endif;
			
		endif;
		
		$vars->errors = $errors;
		H::js(array('form.js'));
		H::css(array( 'form.css', 'boxes.css', 'login.css' ));
		H::vars($vars);
		H::render('views/layout/login.php');
		return true;
	}
	
	public static function recovery() {
		$table_prefix = 'usuario';
		H::vars(array('table_prefix' => $table_prefix));
		$vars = new stdClass();
		$vars->model = new cad_usuario();
		$vars->errors = array();
		$vars->success = false;
		
		if(isset($_POST[$table_prefix])):
			$vars->model->user_vrc_login = $_POST[$table_prefix]['user_vrc_login'];
			$data = $vars->model->findOne();
			if(empty($data->user_int_id)):
				$vars->errors['user_vrc_login'] = 'Usuário não encontrado.';
			else:;
				$vars->success = true;
			endif;
		endif;
				
		H::js(array('form.js'));
		H::css(array( 'form.css', 'boxes.css', 'login.css' ));
		H::vars($vars);
		H::render('views/layout/recovery.php');
		return true;
	}
	
	public static function ch_empresa(){
		if(!$_SESSION['master_user']) return URL::er404();
		
		self::loadModel('software_mais/cad_empresa_model');
		
		$cod = H::cod();
		if($_SESSION['empresa_vrc_url'] != 'dev' && $cod == 'dev')
			return URL::er404();
			
		if(!empty($cod)):
			$mChEmpresa = new cad_empresa();
			$mChEmpresa->empresa_vrc_url = $cod;
			self::sessionDefine(self::$usuario->id, $mChEmpresa->findOne()->empresa_int_id);
			H::redirect('home','index');
			return false;
		else:
			$model = new cad_empresa();
			if($_SESSION['empresa_vrc_url'] != 'dev') $model->DBF()->addWhere('empresa_vrc_url != \'dev\''); 

			H::vars(array('model' => $model));
			H::config('ch_empresa.php');
			return self::RENDER('Mudar Empresa');
		endif;
	}
	
	private static function mkLogin($user, $domain, $cpass) {
		$api = new API();
		$api->addAction('base','acs_list_group','run', array('int_start'=>0, 'int_limit'=>1000, 'vrc_name'=>''));
		$params = array('int_user'=>$user->id, 'vrc_alias' => URL_ALIAS);
		$api->addAction('base','acs_list_module_user','run', $params);
		$api->addAction('cms','cms_list_weblog_profile_user','run', array());
		$res = $api->callMethodCombo();	
		$groups = array_shift($res);
		$modules = array_shift($res);
		$modules_wbl = array_shift($res);
		
		
		if(!!count($groups->errors)):
			$errors = $groups->errors;
		elseif(!!count($modules->errors)):
			$errors = $modules->errors;
		elseif(!!count($modules_wbl->errors)):
			$errors = $modules_wbl->errors;
		else:
			$user->domain = $domain;
			$user->cpass = $cpass;
			$_SESSION['user'] = json_encode($user);
			$_SESSION['acs'] = $menu = array();
			
			foreach($groups->data AS $g)
				$menu[$g->group_vrc_alias] = array(
					'grupo' => $g->group_vrc_name, 
					'icone' => $g->group_vrc_icon, 
					'color' => '', 
					'items' => array()
				);
					
			foreach($modules->data as $i=>$m):
				$m = (object)$m;
				$menu[$m->group_vrc_alias]['items'][] =  array(
					'title' => $m->modl_vrc_name,
					'url' => H::link($m->modl_vrc_alias)
				);
				$_SESSION['acs'][] = $m->modl_vrc_alias;
			endforeach;
			foreach($modules_wbl->data as $m):
				$menu['cms']['items'][] = array(
					'title' => $m->modl_vrc_name,
					'url' => H::link($m->modl_vrc_alias)
				);
				$_SESSION['acs'][] = $m->modl_vrc_alias;
			endforeach;
			
			foreach($menu as $k=>$m) if(!count($m['items'])) unset($menu[$k]);
			
			$_SESSION['acs'][] = 'home';
			$_SESSION['acs'][] = 'profile';
			$_SESSION['acs'][] = 'webmail';
			$_SESSION['acs'][] = 'diary';
			$_SESSION['acs'][] = 'storage.gallery';
			$_SESSION['menu'] = json_encode($menu);
			
		endif;
	}
	
	public static function array_pluck($toPluck, $arr) {
		return array_map(function ($item) use ($toPluck) {
			if(is_array($item)) return $item[$toPluck];
			return $item->{$toPluck};
		}, $arr); 
	}
}
