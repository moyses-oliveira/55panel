<?php
class acs_system extends controller
{
	public static function CONFIG() {
		static::$table_prefix = 'acs_system';
		static::$list_action = 'acs_list_system';
		static::$save_action = 'acs_save_system';
		static::$model_default = 'access_control/acs_system';
		static::$title = 'Sistema';
		static::$view_params = array('sys_vrc_name','sys_vrc_alias');
		static::$grid_params = array('sys_vrc_name','sys_vrc_alias');
	}
}