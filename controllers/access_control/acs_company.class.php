<?php
class acs_company extends controller{
	public static function CONFIG() {
		static::$list_action = 'acs_list_company';
		static::$save_action = 'acs_save_company';
		static::$delete_action = 'acs_delete_company';
		static::$table_prefix = 'acs_company';
		static::$title = 'Cliente';
		static::$view_params = array('company_vrc_name','company_vrc_name2');
		static::$grid_params = array('company_vrc_name','company_vrc_name2');
		
	}

	public static function products() {
		static::formJS();
		static::formCSS();
		$api = new API();
		$api->addAction(static::$base, 'acs_company_pack','info');
		$info = $api->callMethod();
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		
		foreach($info->info as $k=>$i)
			if($i->pk)
				$info->pk = $k;
			
		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		$totalRecords = 0;
		
		$api->addAction(static::$base, 'acs_list_company_pack', 'run', array('company_pack_int_company'=>H::cod(),'int_start'=>$start,'int_limit'=>$limit,'vrc_name'=>''));
		$list = $api->callMethod();
		
		if(count($list->data)) $totalRecords = current($list->data)->total_records;
		$info->pagination = new Pagination($current_page,$limit,$totalRecords);
		$info->data = $list->data;
		
		H::vars($info);
		if(!file_exists(H::path().H::file()))
			H::path('views/default/');

		return static::RENDER(static::$title);
	}
	
	public static function product_create() { static::product_form(); }
	public static function product_update() { static::product_form(H::cod()); }
	public static function product_form($id=null) {
		$vars = new stdClass();
		$vars->errors = $errors = array();
		 
		static::formJS();
		static::formCSS();
		
		$api = new API();
		$api->addAction(static::$base, 'acs_options_plan','run');
		
		$load_data = $api->callMethodCombo();
		
		$modules = array_shift($load_data)->data;
		$vars->options_plan = array(array('','Selecione'));
		foreach($modules as $o) 
			$vars->options_plan[] = array($o->plan_int_id, $o->plan_vrc_name);
		
		if($id):
			$api->addAction(static::$base, 'acs_get_company_pack', 'run', array('company_pack_int_id'=>$id));
			$vars->data = $api->callMethod()->data[0];
		else:
			$vars->data = (object)array('pack_vrc_alias'=>null, 'plan_int_id'=>null);
		endif;
		
		$vars->allow = !!$id;
		if(isset($_POST[static::$table_prefix])):
			CObject::getData($vars->data, $_POST[static::$table_prefix]);
			$data = array();
			$post = $_POST[static::$table_prefix];
			$post['int_usro_id'] = static::$user->id;
			$post['company_pack_int_id'] = $id;
			
			$api->addAction(static::$base, 'acs_save_company_pack', 'run', $post);
			$res = $api->callMethod();
			
			if(count($res->errors)):
				$vars->errors = $res->errors;
			else:
				$data = current($res->data);
				if($data->message != 'SUCCESS'):
					$msgs = array('REPEATED'=>'Este modulo j? esta cadastrado.');
					$vars->errors[] = $msgs[$data->message];
				endif;
				if(!$vars->errors):
					H::vars($vars);
					H::file('modal_success.php');
					return static::RENDER(static::$title);
				endif;
				#H::redirect(H::module(), 'index');
			endif;
		endif;
			
		H::vars($vars);
		H::file('product-form.php');
		
		return static::RENDER(static::$title);
	}
}