<?php
class acs_domain extends controller{
	
	public static function CONFIG() {
		static::$table_prefix = 'acs_domain';
		static::$list_action = 'acs_list_domain';
		static::$save_action = 'acs_save_domain';
		static::$model_default = 'access_control/acs_domain';
		static::$title = 'Modulo';
		static::$view_params = array('domain','domain');
		static::$grid_params = array('domain','domain');
	}

	public static function index(){
		$list_domains = (new hostManager())->showDNSDomains();
		asort($list_domains);
		$domains = array();
		foreach($list_domains as $domain)
			$domains[] = (object)compact('domain');

		$linq = new Linq();
		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		$totalRecords = count($domains);
		$data = from($domains)->skip($start)->take($limit);
		$pagination = new Pagination($current_page,$limit,$totalRecords);
		H::vars(compact('data', 'pagination'));
		H::file('index.php');

		return static::RENDER(static::$title);
	}

	public static function create() { static::form();}
	public static function update() { static::form(URL::friend(2));}
	private static function form($domain = null) {
		$errors = array();
		static::formJS();
		static::formCSS();

		$isSaved = !!@$_SESSION['saved'];
		if(isset($_SESSION['saved'])) 
			unset($_SESSION['saved']);
		
		$api = new API();
		$api->addAction(static::$base, static::$table_prefix, 'info');
		$info = $api->callMethod();
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;

		$config_file = '../.confs/' . $domain;
		if(!$domain || !file_exists($config_file)): 
			$vhostconf = file_get_contents('views/access_control/acs_domain/template.vhost.txt');
			$vhostconf = str_replace('{url}', $domain, $vhostconf);
		else:
			$vhostconf = file_get_contents($config_file);
		endif;

		$labels = $types = $errors = $lenght = array();
		foreach($info->info as $k=>$i):
			$labels[$k] = $i->label;
			$types[$k] = $i->type;
			$lenght[$k] = $i->lenght;
			
			if($i->pk)
				$pk = $k;
			
		endforeach;

		$params = array('domain_vrc_name'=>$domain);
		$api->addAction(static::$base,'acs_get_domain','run', $params);
		$data_res = $api->callMethod()->data;
		
		$info->data->domain_vrc_name = $domain;
		$data = !$data_res ? $info->data : $data_res[0];
		$id = empty($data->domain_int_id) ? null : $data->domain_int_id;
		$mailinabox_msg = null;

		if(isset($_POST[static::$table_prefix])):
			CObject::getData($data, $_POST[static::$table_prefix]);
			$_POST[static::$table_prefix][$pk] = $id;
			$post = $_POST[static::$table_prefix];
			$domain = $post['domain_vrc_name'];
			$post['user_int_id'] = static::$user->id;
			$is_domain = URL::is_domain($domain);
			$repository = $post['domain_vrc_repository'];
			
			if(!empty($post['vhostconf']))
				$vhostconf = $post['vhostconf'];
			
			if(!$is_domain):
				$errors = array('Não é um domínio.');
			elseif(!phpGit::validURL($repository)):
				$errors = array('Não é um repositorio.');
			endif;
			
			if(!count($errors)):
				$api->addAction(static::$base, static::$save_action, 'run', $post);
				$res = $api->callMethod(true);
			endif;
			
			if(count($errors)):
				# STOP VERIFY
			elseif(count($res->errors)):
				$errors = $res->errors;
			else: 
				$data_res = current($res->data);
				if(isset($data_res->message)):
					$msgs = array('REPEATED'=>'Este item já está cadastrado.');
					$errors[] = $msgs[$data_res->message];	
				else:
					$_SESSION['saved'] = true;
					if(!empty($post['vhostconf']) || !$id):
						file_put_contents($config_file, $vhostconf);
						$path = static::getPath($domain);
						H::mkDir($path);
						phpGit::setOrigin($path, $repository);
					endif;
					
					$mailinabox_msg = (new hostManager())->addDNSDomain($data_res->domain_vrc_name);
					H::redirect(H::module(), 'update', $data_res->domain_vrc_name);
				endif;
			endif;
		endif;
		
		$allow = !!$domain;
		$table_prefix = static::$table_prefix;
		H::vars(compact('labels', 'types', 'lenght', 'errors', 'data', 'isSaved', 'allow', 'vhostconf', 'table_prefix'));
		H::file('form.php');
		
		return static::RENDER(static::$title);
	}
	
	public static function update_version(){
		static::formJS();
		static::formCSS();
		$domain = URL::friend(2);
		$api = new API();
		$errors =array();
		
		if(!$domain)
			return URL::er500('domain not found.');
		
		$params = array('domain_vrc_name'=>$domain);
		$api->addAction(static::$base,'acs_get_domain','run', $params);
		$res = $api->callMethod();
		
		if(!isset($res->data[0]))
			return URL::er500('domain not saved.');
		
		$data = $res->data[0];
		$path = static::getPath($domain);
		$message = '';
		if(count($_POST)):
			$pull = phpGit::pull($path, $_POST['user'], $_POST['pass']);
			if($pull->success):
				$message = $pull->message;
			else:
				$errors[] = $pull->message;
			endif;
		endif;
		$allow = true;
		H::vars(compact('message', 'errors', 'data', 'allow'));
		H::file('update-version.php');
		return static::RENDER(static::$title);
	}
	
	public static function getPath($domain){
		return '../customers/' . $domain;
	}
}