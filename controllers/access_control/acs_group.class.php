<?php
class acs_group extends controller 
{
	public static function CONFIG() {
		static::$table_prefix = 'acs_group';
		static::$list_action = 'acs_list_group';
		static::$save_action = 'acs_save_group';
		static::$delete_action = 'acs_delete_group';
		static::$model_default = 'access_control/acs_group';
		static::$title = 'Grupo de Modulos';
		static::$view_params = array('group_int_order','group_vrc_name', 'group_vrc_alias');
		static::$grid_params = array('group_int_order','group_vrc_name', 'group_vrc_alias');
	}
}