<?php
class acs_profile extends controller {	

	public static function CONFIG() {
		static::$table_prefix = 'acs_profile';
		static::$list_action = 'acs_list_profile';
		static::$save_action = 'acs_save_profile';
		static::$delete_action = 'acs_delete_profile';
		static::$model_default = 'access_control/acs_profile';
		static::$title = 'Perfil de Acesso';
		static::$view_params = array('profile_vrc_name');
		static::$grid_params = array('profile_vrc_name');
	}
		
	public static function create() { static::form();}
	public static function update() { static::form(H::cod());}
	private static function form($id = null) {
		$errors = array();
		static::formJS();
		static::formCSS();
		
		$isSaved = !!@$_SESSION['saved'];
		if(isset($_SESSION['saved'])) 
			unset($_SESSION['saved']);
		
		
		$api = new API();
		$api->addAction('base',static::$table_prefix,'info');
		$info = $api->callMethod();
		
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		$labels = $types = $errors = array();
		foreach($info->info as $k=>$i):
			$labels[$k] = $i->label;
			$types[$k] = $i->type;
			if($i->pk)
				$pk = $k;
			
		endforeach;
		
		if($id):
			$params = array('pk'=>$id);
			$api->addAction('base',static::$table_prefix,'single', $params);
			$data = $api->callMethod()->data;
		else:
			$data = $info->data;
		endif;
		
		if(isset($_POST[static::$table_prefix])):
			
			CObject::getData($data, $_POST[static::$table_prefix]);
			$_POST[static::$table_prefix]['profile_int_id'] = $id;
			$post = $_POST[static::$table_prefix];
			$post['int_usro_id'] = static::$user->id;
			if(!isset($_POST['module']) || !count($_POST['module'])):
				$errors = array('Selecione um perfil de acesso.');
			else:
				foreach($_POST['module'] as $module)
					$api->addAction('base', 'acs_save_module_profile', 'run', array('smp_int_modl_plan'=>$module));
					
					
				$api->addAction('base', static::$save_action, 'run', $post);
				$responses = $api->callMethodCombo();
				$errors = array();
				$responses = (array)$responses;
				
				foreach($responses as $res)
					if(count($res->errors))
						$errors = $res->errors;
				
				if(count($errors)):
					$errors = $errors;
				else:
					$data = end($responses)->data;
					$data = current($data);
					if(isset($data->message)):
						$msgs = array('REPEATED'=>'Este perfil já esta cadastrado.');
						$errors[] = $msgs[$data->message];
					else:
						$api->addAction('cms', 'cms_clear_weblog_profile', 'run', array('profile_int_id'=>$data->profile_int_id));
						foreach($_POST['wbl'] as $wbl):
							$params = array('profile_int_id'=>$data->profile_int_id, 'weblog_int_id'=>$wbl);
							$api->addAction('cms', 'cms_save_weblog_profile', 'run', $params);
						endforeach;
						$responses = $api->callMethodCombo();
					
						$_SESSION['saved'] = true;
						H::redirect(H::module(), 'update', $data->profile_int_id);
					endif;		
				endif;
			endif;
		endif;
		
		$api->addAction('base', 'options_module_profile', 'run', array('profile_int_id'=>$id));
		$modules_data = $api->callMethod()->data;
		$api->addAction('cms', 'cms_options_weblog_profile', 'run', array('profile_int_id'=>$id));
		$wbl = $api->callMethod()->data;
		$modules = array_merge($modules_data, $wbl);
		
		$allow = !!$id;
		
		
		H::vars(compact('labels', 'types', 'lenght', 'errors', 'data', 'isSaved', 'allow', 'modules'));
		H::file('form.php');
		return static::RENDER(static::$title);
	}
	
	public static function delete() {
		$api = new API();
		$params = array('pk' => H::cod(), 'user_int_id' => static::$user->id);
		if(!$params['pk'])
			return URL::er500('Pk not found.');
		
		$api->addAction(static::$base, static::$delete_action, 'run', $params);
		$data = $api->callMethod();
		
		if(!count($data->errors)):
			$msg = current($data->data)->message;
			if($msg == 'SUCCESS'):
				H::redirect(H::module(), 'index');
			elseif($msg == 'INVALID_PK'):
				return URL::er500(array('Primary Key not found!'));
			elseif($msg == 'USED'):
				$params = array('pk'=>H::cod());
				$api->addAction('base',static::$table_prefix,'single', $params);
				$prof = $api->callMethod()->data;
				$_SESSION['fail'] = sprintf('O perfil "%s" est? em uso por um usu?rio.', $prof->profile_vrc_name);
				H::redirect(H::module(), 'index');
			endif;
		else:
			return URL::er500($data->errors);
		endif;
	}
	
}