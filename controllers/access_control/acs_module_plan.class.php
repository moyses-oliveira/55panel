<?php
class acs_module_plan extends controller {

	public static function CONFIG() {
		static::$table_prefix = 'acs_module_plan';
		static::$list_action = 'acs_list_module_plan';
		static::$save_action = 'acs_save_module_plan';
		static::$delete_action = 'acs_delete_module_plan';
		static::$model_default = 'access_control/acs_module_plan';
		static::$title = 'Modulo/Plano';
		static::$view_params = array('modl_plan_int_module','modl_plan_int_plan','modl_plan_int_group');
		static::$grid_params = array('modl_plan_int_module','modl_plan_int_plan','modl_plan_int_group');
	}

	public static function create() { static::form();}
	public static function update() { static::form(H::cod());}
	private static function form($id = null) {	
		$errors = array();
		$vars = new stdClass();
		static::formJS();
		static::formCSS();
		
		$api = new API();
		$api->addAction('base', static::$table_prefix,'info');
		$api->addAction('base', 'acs_options_module','run');
		$api->addAction('base', 'acs_options_group','run');
		
		$load_data = $api->callMethodCombo();
		$info = array_shift($load_data);
		
		$modules = array_shift($load_data)->data;
		$vars->options_modules = array(array('','Selecione'));
		foreach($modules as $o) 
			$vars->options_modules[] = array($o->modl_int_id, $o->modl_vrc_name);
			
		$groups = array_shift($load_data)->data;
		$vars->options_groups = array(array('','Selecione'));
		foreach($groups as $o) 
			$vars->options_groups[] = array($o->group_int_id, $o->group_vrc_name);

		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		$vars->labels = $vars->types = $vars->errors = array();
		foreach($info->info as $k=>$i):
			$vars->labels[$k] = $i->label;
			$vars->types[$k] = $i->type;
			if($i->pk)
				$pk = $k;
			
		endforeach;
		
		if($id):
			$params = array('pk'=>$id);
			$api->addAction('base',static::$table_prefix,'single', $params);
			$vars->data = $api->callMethod()->data;
		else:
			$vars->data = $info->data;
		endif;
		
		$vars->allow = !!$id;
		if(isset($_POST[static::$table_prefix])):
			CObject::getData($vars->data, $_POST[static::$table_prefix]);
			$_POST[static::$table_prefix][$pk] = $id;
			$data = array();
			$post = $_POST[static::$table_prefix];
			$api->addAction('base', static::$save_action, 'run', $post);
			$res = $api->callMethod();
			
			if(count($res->errors)):
				$vars->errors = $res->errors;
			else: 
				$data = current($res->data);
				if(isset($data->message)):
					$msgs = array('REPEATED'=>'Este modulo já esta cadastrado.');
					$vars->errors[] = $msgs[$data->message];
				endif;
				if(!$vars->errors):
					H::vars($vars);
					H::file('modal_success.php');
					return static::RENDER(static::$title);
				endif;
				#H::redirect(H::module(), 'index');
			endif;
		endif;
		H::vars($vars);
		H::file('form.php');
		return static::RENDER(static::$title);
	}
	
}