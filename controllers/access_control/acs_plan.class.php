<?php
class acs_plan extends controller {

	public static function CONFIG() {
		static::$table_prefix = 'acs_plan';
		static::$list_action = 'acs_list_plan';
		static::$save_action = 'acs_save_plan';
		static::$model_default = 'access_control/acs_plan';
		static::$title = 'Plano';
		static::$grid_params = array('sys_vrc_name','plan_vrc_name','plan_vrc_alias');
		static::$view_params = array();
	}
	
	public static function index() {
		$api = new API();
		$api->addAction('base', 'vwe_acs_plan', 'info');
		$info = $api->callMethod();
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		
		$info->pk = 'plan_int_id';
			
		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		$totalRecords = 0;
		
		$api->addAction('base', static::$list_action, 'run', array('int_start'=>$start,'int_limit'=>$limit,'vrc_name'=>''));
		$list = $api->callMethod();
		if(count($list->data)) $totalRecords = current($list->data)->total_records;
		$info->pagination = new Pagination($current_page, $limit, $totalRecords);
		$info->data = $list->data;
		$info->info->sys_vrc_name->label = 'Sistema';
		H::vars($info);
		if(!file_exists(H::path().H::file()))
			H::path('views/default/');

		return static::RENDER(static::$title);
	}
		
	public static function create() { static::form();}
	public static function update() { static::form(H::cod());}
	private static function form($id = null) {	
		$errors = array();
		static::formJS();
		static::formCSS();
		
		$isSaved = !!@$_SESSION['saved'];
		if(isset($_SESSION['saved'])) 
			unset($_SESSION['saved']);
		
		$api = new API();
		$api->addAction('base', static::$table_prefix,'info');
		$api->addAction('base', 'acs_options_system','run');
		if($id)
			$api->addAction('base', 'acs_list_module_plan','run', array('plan_int_id'=>$id));
		
		$load_data = $api->callMethodCombo();
		$info = array_shift($load_data);
		
		$systems = array_shift($load_data)->data;
		$options = array(array('','Selecione'));
		foreach($systems as $o) 
			$options[] = array($o->sys_int_id, $o->sys_vrc_name);
		
		$modules = count($load_data) ? array_shift($load_data)->data : null; 
		
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		$labels = $types = $errors = array();
		foreach($info->info as $k=>$i):
			$labels[$k] = $i->label;
			$types[$k] = $i->type;
			if($i->pk)
				$pk = $k;
			
		endforeach;
		
		if($id):
			$params = array('pk'=>$id);
			$api->addAction('base',static::$table_prefix,'single', $params);
			$data = $api->callMethod()->data;
		else:
			$data = $info->data;
		endif;
		
		
		if(isset($_POST[static::$table_prefix])):
			CObject::getData($data, $_POST[static::$table_prefix]);
			$_POST[static::$table_prefix][$pk] = $id;
			$data = array();
			$post = $_POST[static::$table_prefix];
			$api->addAction('base', static::$save_action, 'run', $post);
			$res = $api->callMethod();
			
			if(count($res->errors)):
				$errors = $res->errors;
			else: 
				$data = current($res->data);
				if(isset($data->message)):
					$msgs = array('REPEATED'=>'Este modulo já esta cadastrado.');
					$errors[] = $msgs[$data->message];
				else:
					$_SESSION['saved'] = true;
					H::redirect(H::module(), 'update', $data->{$pk});
				endif;
			endif;
		endif;
		$allow = !!$id;
		
		H::vars(compact('labels', 'types', 'lenght', 'errors', 'data', 'isSaved', 'allow', 'options', 'modules'));
		H::file('form.php');
		return static::RENDER(static::$title);
	}
	
	
	public static function create_rel() {
		require 'acs_module_plan.class.php';
		H::path('views/access_control/acs_module_plan/');
		acs_module_plan::CONFIG();
		return acs_module_plan::create();
	}	
	
	public static function update_rel() {
		require 'acs_module_plan.class.php';
		H::path('views/access_control/acs_module_plan/');
		acs_module_plan::CONFIG();
		return acs_module_plan::update();
	}
	
	
	public static function delete_rel() {
		$params = array('pk' => H::cod());
		if(!$params['pk'])
			return URL::er500('Pk not found.');
		
		$api = new API();
		$api->addAction('base', 'acs_delete_module_plan', 'run', $params);
		$data = $api->callMethod();
		
		if(!count($data->errors)):
			if(current($data->data)->message == 'SUCCESS'):
				H::redirect(H::module(), 'update', URL::getVar('plan'));
			else:
				return URL::er500('Dados não encontrados');
			endif;
		else:
			return URL::er500($data->errors);
		endif;
	}
}