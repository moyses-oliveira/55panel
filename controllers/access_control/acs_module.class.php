<?php
class acs_module extends controller 
{
	public static function CONFIG() {
		static::$table_prefix = 'acs_module';
		static::$list_action = 'acs_list_module';
		static::$save_action = 'acs_save_module';
		static::$model_default = 'access_control/acs_module';
		static::$title = 'Modulo';
		static::$view_params = array('modl_vrc_name','modl_vrc_alias');
		static::$grid_params = array('modl_vrc_name','modl_vrc_alias');
	}
}