<?php
class acs_user extends controller{
	public static function CONFIG() {
		static::$list_action = 'acs_list_user';
		static::$save_action = 'acs_save_user';
		static::$delete_action = 'acs_delete_user';
		static::$table_prefix = 'acs_user';
		static::$title = 'Usuário';
		static::$view_params = array('user_vrc_name','user_vrc_email');
		static::$grid_params = array('user_vrc_name','user_vrc_email');
		
	}
	
	public static function create() { static::form();}
	public static function update() { static::form(H::cod());}
	private static function form($id = null) {	
		$errors = array();
		static::formJS();
		static::formCSS();
		$currentMails =  (new hostManager())->showEmails();
		$isSaved = !!@$_SESSION['saved'];
		if(isset($_SESSION['saved'])) 
			unset($_SESSION['saved']);
		
		$api = new API();
		$api->addAction('base',static::$table_prefix,'info');
		$info = $api->callMethod();
		
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		$labels = $types = $errors = array();
		foreach($info->info as $k=>$i):
			$labels[$k] = $i->label;
			$types[$k] = $i->type;
			if($i->pk)
				$pk = $k;
			
		endforeach;
		
		if($id):
			$params = array('pk'=>$id);
			$api->addAction('base',static::$table_prefix,'single', $params);
			$data = $api->callMethod()->data;
		else:
			$data = $info->data;
		endif;
		
		
		if(isset($_POST[static::$table_prefix])):
			CObject::getData($data, $_POST[static::$table_prefix]);
			$_POST[static::$table_prefix][$pk] = $id;
			$post = $_POST[static::$table_prefix];
			
			$post['int_usro_id'] = static::$user->id;
			#if(!isset($_POST['access']) || !count($_POST['access'])):
			#	$errors = array('Selecione um perfil de acesso.');
			#else:
			#endif;
			if(isset($_POST['access']))
				foreach($_POST['access'] as $access)
					$api->addAction('base', 'acs_save_user_profile', 'run', array('uspr_int_profile'=>$access));
			
			$email = sprintf('%s@%s', $post['alias'], $post['domain']); 
			
			$post['user_vrc_email'] = $email;
			$api->addAction('base', static::$save_action, 'run', $post);
			$responses = $api->callMethodCombo();
			$errors = array();
			$responses = (array)$responses;
			
			foreach($responses as $res)
				if(count($res->errors))
					$errors = $res->errors;

			if(!count($errors)):
				$data_res = end($responses)->data[0];
				if(isset($data_res->message)):
					$msgs = array(
						'REPEATED'=>'Este e-mail já esta cadastrado.',
						'NOT_FOUND'=>'Usuário não encontrado.'
					);
					$errors[] = $msgs[$data_res->message];
				else:
					if(!in_array($email, $currentMails)):
						(new hostManager())->addEmail($email, $post['user_vrb_pass']);
					endif;
					if(!empty($post['user_vrb_pass']) && !!$id):
						(new hostManager())->chPassword($email, $post['user_vrb_pass']);
					endif;
					$_SESSION['saved'] = true;
					H::redirect(H::module(), 'update', $data_res->user_int_id);
				endif;
			endif;
		endif;
		$domains = (new hostManager())->showDNSDomains();
		
		$api->addAction('base', 'options_user_profile', 'run', array('user_int_id'=>$id));
		$profiles = $api->callMethod()->data;
		$allow = !!$id;
		
		H::vars(compact('domains', 'labels', 'types', 'lenght', 'errors', 'data', 'isSaved', 'allow', 'profiles'));
		H::file('form.php');
		return static::RENDER(static::$title);
	}
	
	public static function delete() {
		if(!H::cod())
			return URL::er500('Pk not found.');
		
		$api = new API();
		
		$params = array('pk'=>H::cod());
		$api->addAction('base',static::$table_prefix,'single', $params);
		$data = $api->callMethod()->data;
		$user_vrc_email = $data->user_vrc_email;
		
		$params = array('pk' => H::cod(), 'user_int_id' => static::$user->id);
		$api->addAction(static::$base, static::$delete_action, 'run', $params);
		$data = $api->callMethod();
		
		$msgs = array('INVALID_PK'=>'Item não encontrado');
		
		if(!count($data->errors)):
			$msg = current($data->data)->message;
			$params = array('user_vrc_email'=>$user_vrc_email);
			$api->addAction('base','acs_count_user_email','run', $params);
			$data = $api->callMethod()->data[0];
			
			if($data->total < 1):
				(new hostManager())->removeEmail($user_vrc_email);
			endif;
			
			echo $msg == 'SUCCESS' ? $msg : $msgs[$msg];
		else:
			echo '500';
		endif;
	}
}