<?php
class bootstrap extends controller {
	
	public static function INIT(){
		Encryption::setKey('rcne');
		if(isset($_SESSION['user']) && !empty($_SESSION['user'])):
			static::$user = json_decode($_SESSION['user']);
			H::vars(array('user' => static::$user));
			define('PLAN',static::$user->int_plan);
		else:
			define('PLAN',0);
		endif;
		list($module, $action, $cod) = array(URL::friend(0), URL::friend(1), URL::friend(2));
		H::module(empty($module) ? 'home' : strtolower($module));
		$action = empty($action) ? 'index' : str_replace('-','_',strtolower($action));
		H::action($action);
		H::file($action.'.php');
		H::cod($cod);
		H::root(URL::root());
		H::site(URL::site());
		
		if(H::module() == 'home' && H::action() == 'logout'):
			session_destroy();
			H::redirect('home','index');
		endif;
		# JS GERAL DO MODULO
		H::js(array('jquery-1.9.1.min.js', 'jquery-ui-1.9.2.custom.min.js', 'bootstrap.min.js', 'jquery.magnific-popup.min.js', 'jquery/jquery.tooltipster.min.js', 'modal.js', 'app.js'));
		
		# CSS GERAL DO MODULO
		H::css(array('fonts.css', 'font-awesome.min.css', 'jquery-ui-1.9.2.custom.css', 'bootstrap.min.css',  'tooltipster.css', 'magnific-popup.css', 'App.css'));
		H::path('views/');
		$allow = true;
		
		$offline_pages = array('login', 'home', 'recovery', 'not_found', 'denied');
		if(empty(static::$user)): 
			if(H::module() != 'home'):
				H::module('home');
				H::action('index');
			elseif(!in_array(H::action(), $offline_pages)):
				H::action('index');
			endif;
		endif;
					
		if(substr_count($module, 'dcms.') > 0):
			$module = $class =  'dcms';
		else:
			$module = str_replace('-','_',H::module());
			$module = str_replace('.','/',$module);
			$extract = explode('/',$module);
			if(count($extract) == 1): 
				$module = $module;
				$extract[0] = $module;
			endif;
			$class = end($extract);
		endif;
		
		$ctrlFile = 'controllers/'.$module.'.class.php';
		if(file_exists($ctrlFile)):
			require $ctrlFile;
		else:
			H::module('home');
			H::action('error');
		endif;
		
		$path = str_replace('-','_',H::module());
		$path = str_replace('.','/',$path);
		H::path('views/' . $path . '/');
		
		if(method_exists( $class, 'CONFIG'))
			call_user_func( $class . '::CONFIG');
			
		if(method_exists( $class, 'baseConfig'))
			call_user_func( $class . '::baseConfig');
			
		
		$acs = isset($_SESSION['acs']) ? $_SESSION['acs'] : array();
		$denied = false;
		if(!empty($module) && !in_array($module,$acs) && !in_array($module,$offline_pages)) 
			$denied = true;
		
		if(method_exists( $class, H::action()) && $allow):
			if($denied && empty(static::$user)):
				return H::redirect('home','index');
			else:
				return call_user_func( $class . '::' . H::action());
			endif;
		endif;
		return URL::er404();		
	}
}
