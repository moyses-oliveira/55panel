<?php
class profile extends controller {
	
	public static function CONFIG() {
		static::$table_prefix = 'acs_user';
		static::$list_action = 'acs_list_user';
		static::$save_action = 'acs_save_user';
		static::$delete_action = 'acs_delete_user';
		static::$model_default = 'access_control/acs_user';
		static::$title = 'Meu Perfil';
		static::$view_params = array('user_vrc_name', 'user_vrc_email');
		static::$grid_params = array('user_vrc_name', 'user_vrc_email');
	}
	
	
	
	public static function index(){
		$errors = array();
		$vars = new stdClass();
		static::formJS();
		static::formCSS();
		
		$api = new API();
		$api->addAction('base',static::$table_prefix,'info');
		$info = $api->callMethod();
		$vars->isUpdated = false;
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		$vars->labels = $vars->types = $vars->errors = array();
		foreach($info->info as $k=>$i):
			$vars->labels[$k] = $i->label;
			$vars->types[$k] = $i->type;
			if($i->pk)
				$pk = $k;
			
		endforeach;
		
		$params = array('pk'=>static::$user->id);
		$api->addAction('base',static::$table_prefix,'single', $params);
		$vars->data = $api->callMethod()->data;
		
		if(isset($_POST[static::$table_prefix])):
			$_POST[static::$table_prefix][$pk] = static::$user->id;
			$data = array();
			$post = $_POST[static::$table_prefix];
			$post['user_vrb_pass'] = null;
			$post['int_usro_id'] = static::$user->id;
			$api->addAction('base', 'acs_save_user', 'run', $post);
			$res = $api->callMethod();
			$errors = array();
			
			if(count($res->errors))
				$errors = $res->errors;

			if(count($errors)):
				$vars->errors = $errors;
			else:
				$data = $res->data;
				$data = current($data);
				if(isset($data->message)):
					$msgs = array(
						'REPEATED'=>'Este e-mail já esta cadastrado.',
						'NOT_FOUND'=>'Usuário não encontrado.'
					);
					$vars->errors[] = $msgs[$data->message];
				else:
					$_SESSION['updated'] = true;
					H::redirect(H::module(), H::action());
				endif;	
			endif;
		endif;
		
		$vars->isUpdated = isset($_SESSION['updated']) ? $_SESSION['updated'] : false;
		$_SESSION['updated'] = false;
		
		
		H::vars($vars);
		H::file('index.php');
		return static::RENDER('Alterar Senha');
	}
	
	public static function change_password(){
		$vars = new stdClass();
		$vars->errors = array();
		static::formJS();
		static::formCSS();

		if(isset($_POST[static::$table_prefix])):
			extract($_POST[static::$table_prefix]);
			$pass = Encryption::decode(static::$user->cpass);
			if($old_pass != $pass):
				$vars->errors[] = 'A senha atual não convere.';
			elseif($new_pass != $rnew_pass):
				$vars->errors[] = 'A repetição de senha não confere.';
			else:
				(new hostManager())->chPassword(static::$user->email, $new_pass);
				$_SESSION['updated'] = true;
				H::redirect(H::module(), H::action());
				exit;
			endif;
			/*
			$_POST[static::$table_prefix][$pk] = static::$user->id;
			$data = array();
			$post = $_POST[static::$table_prefix];
			$post['user_int_id'] = static::$user->id;
			$api->addAction(static::$base, 'acs_change_password', 'run', $post);
			$res = $api->callMethod();
			
			if(count($res->errors)):
				$vars->errors = $res->errors;
			else: 
				$data = current($res->data);
				if($data->message == 'SUCCESS'):
					$_SESSION['updated'] = true;
					H::redirect(H::module(), H::action());
					exit;
				endif;
				$msgs = array(
					'INVALID_PASS'=>'Senha atual invalida.',
					'INVALID_LENGTH'=>'A nova senha precisa conter no minimo 6 letras.',
					'RPASS_FAIL'=>'A repetição da nova senha não confere.'
				);
				$vars->errors[] = $msgs[$data->message];	
			endif;
			*/
		endif;
		$vars->isUpdated = isset($_SESSION['updated']) ? $_SESSION['updated'] : false;
		$_SESSION['updated'] = false;
		
		H::vars($vars);
		H::file('change-password.php');
		return static::RENDER('Alterar Senha');
	}
}