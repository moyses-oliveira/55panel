<?php
class dcms extends controller {
	
	private static $settings = null;
	private static $mod = null;
	
	public static function CONFIG(){
		$api = new API();
		$module = H::module();
		$mod = str_replace('dcms.','',$module);
		$params = array('wbl_vrc_alias'=>$mod);
		static::$mod = $mod;
		$api->addAction('cms', 'cms_get_weblog_by_alias', 'run', $params);
		
		static::$settings = $api->callMethod()->data[0];
		$s = static::$settings;
		define('WBL_INT_ID', $s->wbl_int_id);
		define('SINGLE_PAGE', $s->wbl_tny_single > 0);
		define('CATEGORIES', $s->wbl_tny_enable_category > 0);
		define('SUMMARY', $s->wbl_tny_enable_summary > 0);
		define('AUTHOR', $s->wbl_tny_enable_author > 0);
		define('FILTER', $s->wbl_tny_enable_filter > 0);
		define('IMAGE', $s->wbl_enm_images);
		define('DATE_FORMAT', $s->wbl_enm_publish_date);
		define('TITLE', $s->wbl_vrc_description);
		static::$base = 'cms';
		static::$table_prefix = 'post';
		static::$list_action = 'cms_list_weblog_post';
		static::$delete_action = 'cms_delete_weblog_post';
		static::$title = TITLE;
		H::path('views/dcms/');
	}


	public static function index(){
		if(SINGLE_PAGE):
			$api = new API();
			$params = array('postd_int_post'=>null, 'postd_int_wbl'=>WBL_INT_ID, 'postd_chr_language'=>'pt_br');
			$api->addAction(static::$base, 'cms_get_weblog_post_description','run', $params);
			$post = $api->callMethod();
			static::form( isset($post->data[0]) ? current($post->data)->post_int_id : null);
		else:
			static::findAll();
		endif;
	}
	
	private static function findAll(){
		$api = new API();
		
		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		$totalRecords = 0;
		
		$api->addAction(static::$base, static::$list_action, 'run', array('int_start'=>$start,'int_limit'=>$limit,'vrc_name'=>'','chr_language'=>'pt_br', 'vrc_alias'=>static::$mod));
		$list = $api->callMethod();
		$list->data = (array)$list->data;
		
		if(count($list->data)) 
			$totalRecords = current($list->data)->total_records;
		
		$pagination = new Pagination($current_page,$limit,$totalRecords);
		$data = $list->data;
		
		H::vars(compact('pagination', 'data'));
		return static::RENDER(static::$title);
	}
	
		
	public static function create() { static::form();}
	public static function update() { static::form(H::cod());}
	private static function form($id = null) {	
		$errors = array();
		static::formJS(array('tinymce/tinymce.min.js', 'gallery.js', 'fileinput/fileinput.js', 'fileinput/fileinput_locale_pt-BR.js', 'weblog/post.js'));
		static::formCSS(array('gallery.css','fileinput.min.css','weblog/post.css'));
	
		
		$isSaved = !!@$_SESSION['saved'];
		if(isset($_SESSION['saved'])) 
			unset($_SESSION['saved']);
		
		$api = new API();
		$api->addAction(static::$base, 'cms_weblog_post', 'info');
		$api->addAction(static::$base, 'cms_weblog_post_description', 'info');
		$api->addAction(static::$base, 'cms_options_weblog_category', 'run', array('chr_language'=>'pt_br'));
		$responses = (array)$api->callMethodCombo();
		foreach($responses as $response)
			if(!!count($response->errors)):
				echo $response->errors[0];
				die;
			endif;
			
		$pinfo  = array_shift($responses);
		$dinfo  = array_shift($responses);
		$labels = $types = $errors = $lenght = array();
		$info = (object)array_merge((array)$pinfo->info, (array)$dinfo->info);
		$data = (object)array_merge((array)$pinfo->data, (array)$dinfo->data);
		$imgs = array();
		foreach($info as $k=>$i):
			$labels[$k] = $i->label;
			$types[$k] = $i->type;
			$lenght[$k] = $i->lenght;
			if($i->pk) $pk = $k;			
		endforeach;
		
		$cat  = array_shift($responses);
		$category_options = array(array('','Selecione a categoria'));
		foreach($cat->data as $v):
			$category_options[] = array($v->id, $v->name);
		endforeach;
		#$category_options[] = array('#','-- Criar nova categoria --');
		
		if($id):
			$api->addAction(static::$base, 'cms_weblog_post','single', array('pk'=>$id));
			$params = array('postd_int_post'=>$id, 'postd_int_wbl'=>WBL_INT_ID, 'postd_chr_language'=>'pt_br');
			$api->addAction(static::$base, 'cms_get_weblog_post_description','run', $params);
			$api->addAction(static::$base, 'cms_list_weblog_post_image','run', array('int_post'=>$id));
			$dataRes = (array)$api->callMethodCombo();
			$dataPost = array_shift($dataRes)->data;
			$dataDesc = array_shift($dataRes)->data[0];
			$imgs = array_shift($dataRes)->data;
			$data = (object)array_merge((array)$dataPost, (array)$dataDesc);
		endif;
		
		if(count($_POST)):
			$data = array();
			$int_item_num = 0;
			if(isset($_POST['desc'])):
				foreach($_POST['desc'] as $post_desc):
					$api->addAction(static::$base, 'cms_save_weblog_postd', 'run', $post_desc);
					$int_item_num++;
				endforeach;
			endif;
			
			if(isset($_POST['post_img'])):
				foreach($_POST['post_img'] as $i=>$post_img):
					$post_img['post_img_vrc_description'] = '';
					$api->addAction(static::$base, 'cms_save_weblog_post_image', 'run', $post_img);
				endforeach;
			endif;
			$params = $_POST[static::$table_prefix];
			$params['post_int_id'] = $id;
			$params['post_int_wbl'] = WBL_INT_ID;
			
			if(strlen($params['post_dtt_posted']) == 10):
				$params['post_dtt_posted'] = CData::setDateBr($params['post_dtt_posted']);
			else:
				$params['post_dtt_posted'] = CData::setDateTimeBr($params['post_dtt_posted']);
			endif;
			$params['int_user_id'] = static::$user->id;
			$api->addAction(static::$base,'cms_save_weblog_post', 'run',$params);
			$responses = (array)$api->callMethodCombo();
			
			if(!count($errors)):
				$rows = end($responses);
				$_SESSION['isUpdated'] = true;
				if(SINGLE_PAGE)
					H::redirect(H::module(), 'index');
				else
					H::redirect(H::module(), 'update', end($rows->data)->post_int_id);
					
			endif;
		endif;
		
		$allow = !!$id;
		H::vars(compact('imgs', 'allow', 'data', 'labels', 'types', 'lenght', 'errors', 'isSaved', 'category_options'));
		H::file('form.php');
		return static::RENDER(static::$title);
	}

    public static function gallery()
    {
        return static::loadAction('storage/gallery', 'modal',true, array(URL_ALIAS));
    }
	
	public static function move() {
		$to = $_GET['to'];
		$redirect = $_GET['redirect'];
		$params = array();
		$params['post_int_id'] = H::cod();
		$params['post_int_wbl'] = WBL_INT_ID;
		$params['enm_to'] = $_GET['to'];
		$api = new API();
		$api->addAction(static::$base,'cms_move_weblog_post', 'run',$params);
		$response = $api->callMethod();
		H::redirect($_GET['redirect']);
		
	}
	
	private static function array_pluck($toPluck, $arr) {
		return array_map(function ($item) use ($toPluck) {
			if(is_array($item)) return $item[$toPluck];
			return $item->{$toPluck};
		}, $arr); 
	}
	
	
	public static function categories(){
		static::formJS(array(
            'jquery/jquery.migrate.1.2.1.js',
            'jquery/jquery.metadata.js',
            'jquery/jquery-ui.1.11.min.js',
            'jquery/jquery.rowsorter.min.js',
			'fancytree/jquery.fancytree-all.js',
			'ecommerce/cookie.jquery.js',
			'dcms/weblog_category.js'
		));
		static::formCss(array('fancytree/ui.fancytree.min.css'));

		$vars = new stdClass();

        H::vars($vars);
        H::config('categories.php');
        return static::RENDER(static::$title, 'Lista');
    
	}
	
	public static function category_create(){
		return static::formCategory();
	}
	
	public static function category_update(){
		return static::formCategory(H::cod());
	}
	
	public static function formCategory($id = null) {
		$api = new API();
		$isSaved = !!@$_SESSION['isSaved'];
		$_SESSION['isSaved'] = false;
		$api->addAction(static::$base, 'cms_weblog_category', 'info');
		$api->addAction(static::$base, 'cms_weblog_category_description', 'info');
		$responses = (array)$api->callMethodCombo();
		foreach($responses as $response)
			if(!!count($response->errors)):
				echo $response->errors[0];
				die;
			endif;
		
		$cinfo  = array_shift($responses);
		$dinfo  = array_shift($responses);
		$labels = $types = $errors = $lenght = array();
		if(!!$id):
			$params = array('wblcat_int_id'=>$id, 'wblcat_int_wbl'=>WBL_INT_ID);
			$api->addAction(static::$base, 'cms_get_weblog_category','run', $params);
			$cdata = $api->callMethod();

			if(!$cdata->data)
				return URL::er404();
			
			$params = array('wblcatd_int_wblcat'=>$id, 'wblcatd_int_wbl'=>WBL_INT_ID , 'wblcatd_chr_language'=>'pt_br');
			$api->addAction(static::$base, 'cms_get_weblog_category_description','run', $params);
			$ddata = $api->callMethod();
			
			if(!$ddata->data)
				return URL::er404();
			
			$cinfo->data = $cdata->data[0];
			$dinfo->data = $ddata->data[0];
		endif;	
		
		$info = (object)array_merge((array)$cinfo->info, (array)$dinfo->info);
		$data = (object)array_merge((array)$cinfo->data, (array)$dinfo->data);
		
		if(count($_POST)):
			$data = array();
			$int_item_num = 0;
			
			foreach($_POST['desc'] as $i=>$post_desc):
				
				foreach($post_desc as $j=>$v) $data[$i]->{$j} = $v;
				
				$api->addAction('cms','cms_save_weblog_category_description','run',$post_desc);
				$int_item_num++;
			endforeach;
			
			$category_params = array(
					'wblcat_int_id' => $id,
					'wblcat_int_wbl'=>WBL_INT_ID,
					'wblcat_int_parent'=>null,
					'int_user_id'=>static::$user->id
				);
			
			$api->addAction('cms','cms_save_weblog_category','run', $category_params);
			$responses = $api->callMethodCombo();
			
			$rdata = array_pop($responses)->data;

			if(isset($rdata[0]->message)):
				$errors[] = $rdata[0]->message;
			endif;
			/*
			foreach($responses as $res):
				if(count($errors)) continue;
				if(count($res->errors)):
					$errors = $res->errors;
				endif;
			endforeach;
			*/
			if(!count($errors)):
				$_SESSION['isSaved'] = true;
				return H::redirect(H::module(), 'category_update', $rdata[0]->wblcat_int_id);
			endif;
		endif;
		
		H::vars(compact('data', 'labels', 'info', 'types', 'lenght', 'errors', 'isSaved'));

		H::render(H::path().'category-form.php');
		return true;
	}
	
    public static function get_json_categories() {
		$api = new API();
		$params = array('wbl_int_id'=>WBL_INT_ID,  'chr_language'=>'pt_br');
		$api->addAction('cms', 'cms_list_weblog_category_tree','run', $params);

		$data = $api->callMethod()->data;
		$json = array();
		$parent = null;
		foreach($data as $k=>$d):
			if(empty($d->pid)):
				if(!empty($parent))
					$json[] = $parent;
				
				$parent = $d;
				$parent->children = array();
			else:
				$parent->children[] = $d;
			endif;
		endforeach;
		if(!empty($parent))
			$json[] = $parent;
		
		
		echo json_encode((array)$json);
		return;
	}
	
	public static function delete() {
		$params = array('pk' => H::cod(), 'wbl_int_id'=>WBL_INT_ID, 'user_int_id' => static::$user->id);
		if(!$params['pk'])
			return URL::er500('Pk not found.');
		
		$api = new API();
		$api->addAction(static::$base, static::$delete_action, 'run', $params);
		$data = $api->callMethod();
		
		$msgs = array('INVALID_PK'=>'Item não encontrado');
		
		if(!count($data->errors)):
			$msg = current($data->data)->message;
			echo $msg == 'SUCCESS' ? $msg : $msgs[$msg];
		else:
			echo '500';
		endif;
	}
	
    public static function category_delete() {
		$id = H::cod();
		if(!$id)
			return URL::er404();
		
		$api = new API();
		$params = array('pk'=>$id, 'wbl_int_id'=>WBL_INT_ID, 'user_int_id' => static::$user->id);
		$api->addAction('cms','cms_delete_weblog_category', 'run',$params);
		$res = $api->callMethod();
		
		$msgs = array(
			'INVALID_PK'=>'Categoria não encontrada',
			'HAS_POST'=>'Esta categoria não pode ser excluida porque existem items cadastrados nela.',
			'HAS_CHILD'=>'Esta categoria não pode ser excluida porque possui sub-categorias.'
		);
		if(count($res->errors)):
			var_dump($res);
		else:
			$msg = current($res->data)->message;
			echo $msg == 'SUCCESS' ? $msg : $msgs[$msg];
		endif;
	}
	
    public static function category_organize() {
		$api = new API();
		
		$list = isset($_POST['list']) ? $_POST['list'] : array();
		foreach($list as $k=>$params):
			$params['wblcat_int_position'] = $k;
			$params['wblcat_int_wbl'] = WBL_INT_ID;
			$api->addAction('cms','cms_organize_weblog_category_tree','run', $params);
		endforeach;
		if(count($list)) $api->callMethodCombo();
	
		return 'ok';
	}
	
	public static function refresh_category_options(){
		$api = new API();
		$api->addAction(static::$base, 'cms_options_weblog_category', 'run', array('chr_language'=>'pt_br'));
		$cat = $api->callMethod();
		$category_options = array(array('','Selecione a categoria'));
		foreach($cat->data as $v):
			$category_options[] = array($v->id, $v->name);
		endforeach;
		#$category_options[] = array('#','-- Criar nova categoria --');
		
		foreach($category_options as $c)
			printf('<option value="%s">%s</option>', $c[0], $c[1]);
			
		return true;
	}
	
}