<?php
class cms_people extends controller 
{
	public static function CONFIG() {
		static::$base = 'cms';
		static::$table_prefix = 'cms_people';
		static::$list_action = 'cms_list_people';
		static::$save_action = 'cms_save_people';
		static::$delete_action = 'cms_delete_people';
		static::$model_default = '';
		static::$title = 'Depoimentos';
		static::$view_params = array('people_vrc_name');
		static::$grid_params = array('people_vrc_name');
	}
	
	public static function create() { static::form();}
	public static function update() { static::form(H::cod());}
	private static function form($id = null) {	
		$errors = array();
		$vars = new stdClass();
		static::formJS();
		static::formCSS();
		
		$api = new API();
		$api->addAction(static::$base,static::$table_prefix,'info');
		$info = $api->callMethod();
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		$vars->labels = $vars->types = $vars->errors = $vars->lenght = array();
		foreach($info->info as $k=>$i):
			$vars->labels[$k] = $i->label;
			$vars->types[$k] = $i->type;
			$vars->lenght[$k] = $i->lenght;
			
			if($i->pk)
				$pk = $k;
			
		endforeach;
		
		if($id):
			$params = array('pk'=>$id);
			$api->addAction(static::$base,static::$table_prefix,'single', $params);
			$vars->data = $api->callMethod()->data;
		else:
			$vars->data = $info->data;
		endif;
		
		$pathImgs = 'files/personal/'. URL_ALIAS .'/people/';
		if(isset($_POST[static::$table_prefix])):
			CObject::getData($vars->data, $_POST[static::$table_prefix]);
			$_POST[static::$table_prefix][$pk] = $id;
			$data = array();
			$post = $_POST[static::$table_prefix];
			$post['user_int_id'] = static::$user->id;
			$post['people_vrc_image'] = $vars->data->people_vrc_image;
			$api->addAction(static::$base, static::$save_action, 'run', $post);
			$res = $api->callMethod();
			$data = current($res->data);
			
			$f = $_FILES['people_vrc_image'];
			if(strlen($f['tmp_name']) > 0):
				$post = array_merge($post,(array)$data);
				$ext = pathinfo($f['name'], PATHINFO_EXTENSION);
				$img = sprintf('%s.%s', md5($data->people_int_id), $ext);
				move_uploaded_file($f['tmp_name'], $pathImgs . $img);
				$post['people_vrc_image'] = $img;
				
				$api->addAction(static::$base, static::$save_action, 'run', $post);
				$res = $api->callMethod();
				$data = current($res->data);
			endif;
			
			if(count($res->errors)):
				$vars->errors = $res->errors;
			else: 
				$data = current($res->data);
				if(isset($data->message)):
					$msgs = array('REPEATED'=>'Este item já está cadastrado.');
					$vars->errors[] = $msgs[$data->message];	
				else:
					$_SESSION['updated'] = true;
					H::redirect(H::module(), 'update', $data->people_int_id);
				endif;
			endif;
		endif;
		$vars->isUpdated = isset($_SESSION['updated']) ? $_SESSION['updated'] : false;
		$_SESSION['updated'] = false;
		$vars->allow = !!$id;
		$vars->pathImgs = $pathImgs;
		H::vars($vars);
		H::file('form.php');
		return static::RENDER(static::$title);
	}
}