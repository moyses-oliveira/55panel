<?php
class cms_page extends controller 
{
	public static function CONFIG() {
		static::$base = 'cms';
		static::$table_prefix = 'cms_page';
		static::$list_action = 'cms_list_page';
		static::$save_action = 'cms_save_page';
		static::$delete_action = 'cms_delete_page';
		static::$model_default = '';
		static::$title = 'Página Dinâmica';
		static::$view_params = array('page_vrc_alias', 'page_vrc_title');
		static::$grid_params = array('page_vrc_alias', 'page_vrc_title');
	}
	
	
	public static function create() { static::form();}
	public static function update() { static::form(H::cod());}
	private static function form($id = null) {	
		$errors = array();
		$vars = new stdClass();
		static::formJS();
		static::formCSS();
		
		$api = new API();
		$api->addAction(static::$base,static::$table_prefix,'info');
		$info = $api->callMethod();
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		$vars->labels = $vars->types = $vars->errors = $vars->lenght = array();
		foreach($info->info as $k=>$i):
			$vars->labels[$k] = $i->label;
			$vars->types[$k] = $i->type;
			$vars->lenght[$k] = $i->lenght;
			
			if($i->pk)
				$pk = $k;
			
		endforeach;
		
		if($id):
			$params = array('pk'=>$id);
			$api->addAction(static::$base,static::$table_prefix,'single', $params);
			
			$params = array('int_page'=>$id );
			$api->addAction('cms','cms_list_page_image','run', $params);
			$responses = $api->callMethodCombo();
			
			$vars->data = array_shift($responses)->data;
			$vars->img_data_list = array_shift($responses)->data;
		else:
			$vars->data = $info->data;
			$vars->img_data_list = array();
		endif;
		
		
		if(isset($_POST[static::$table_prefix])):
			CObject::getData($vars->data, $_POST[static::$table_prefix]);
			$_POST[static::$table_prefix][$pk] = $id;
			$data = array();
			if(isset($_POST['page_img'])):
				foreach($_POST['page_img'] as $i=>$post_img):
					$post_img['page_img_vrc_description'] = '';
					$api->addAction('cms', 'cms_save_page_image', 'run', $post_img);
				endforeach;
			endif;
			
			$post = $_POST[static::$table_prefix];
			$post['user_int_id'] = static::$user->id;
			$api->addAction(static::$base, static::$save_action, 'run', $post);
			$response = (array)$api->callMethodCombo();
			$res = end($response);
			if(count($res->errors)):
				$vars->errors = $res->errors;
			else: 
				$data = current($res->data);
				if(isset($data->message)):
					$msgs = array('REPEATED'=>'Este cms jรก esta cadastrado.');
					$vars->errors[] = $msgs[$data->message];
				else:
					H::redirect(H::module(), 'index');
				endif;				
			endif;
		endif;
		$vars->allow = !!$id;
		H::vars($vars);
		H::file('form.php');
		return static::RENDER(static::$title);
	}
	
	public static function gallery()
    {
		require 'controllers/storage/gallery.class.php';
        $linq = new Linq();
		gallery::modal(URL_ALIAS, 'Galeria');
        #return static::loadAction('storage/gallery', 'modal', true, array(URL_ALIAS));
    }
	
	public static function formJS($_js = array()) {
		$js = array(
			'form.js',
			'jquery/jquery.migrate.1.2.1.js',
			'jquery/jquery.metadata.js',
			'jquery/jquery.ui.core.js',
			'jquery/jquery.ui.widget.js',
			'jquery/jquery.ui.datepicker.js',
			'jquery/jquery.ui.datepicker-pt-BR.js',
			'jquery/jquery.maskMoney.js',
			'fileinput/fileinput.js', 
			'fileinput/fileinput_locale_pt-BR.js',
			'gallery.js',
			'cms/cms_page.js'
		);
		$js = array_merge($js, $_js);
		H::js($js);
	}
	
	public static function formCSS($_css = array()) {
		$css = array('form.css','boxes.css', 'smoothness/jquery-ui.css', 'fileinput.min.css', 'gallery.css', 'cms/cms_page.css');
		$css = array_merge($css, $_css);
		H::css($css);
	}
}