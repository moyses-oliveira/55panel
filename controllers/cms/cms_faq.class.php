<?php
class cms_faq extends controller 
{
	public static function CONFIG() {
		static::$base = 'cms';
		static::$table_prefix = 'cms_faq';
		static::$list_action = 'cms_list_faq';
		static::$save_action = 'cms_save_faq';
		static::$delete_action = 'cms_delete_faq';
		static::$model_default = '';
		static::$title = 'FAQ';
		static::$view_params = array('faq_vrc_question');
		static::$grid_params = array('faq_vrc_question');
	}
}