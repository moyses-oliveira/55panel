<?php
class cms_weblog extends controller 
{
	public static function CONFIG() {
		static::$base = 'cms';
		static::$list_action = 'acs_list_company_pack';
		static::$save_action = 'cms_save_weblog';
		static::$delete_action = 'cms_delete_weblog';
		static::$table_prefix = 'cms_weblog';
		static::$title = 'Páginas Dinâmicas';
		static::$view_params = array('company_vrc_name','company_vrc_name2');
		static::$grid_params = array('company_vrc_name','company_vrc_name2');
	}	
	
	public static function index() {
		$api = new API();
		$api->addAction('base','acs_company','info');
		$info = $api->callMethod();
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		
		foreach($info->info as $k=>$i)
			if($i->pk)
				$info->pk = $k;
			
		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		$totalRecords = 0;
		
		$api->addAction('base', static::$list_action, 'run', 
			array('int_start'=>$start,'int_limit'=>$limit,'vrc_name'=>'', 'company_pack_int_company'=>null)
		);
		$list = $api->callMethod();
		if(count($list->data)) $totalRecords = current($list->data)->total_records;
		$info->pagination = new Pagination($current_page,$limit,$totalRecords);
		$info->data = $list->data;
		
		H::vars($info);
		if(!file_exists(H::path().H::file()))
			H::path('views/default/');

		return static::RENDER(static::$title);
	}
	
	public static function modules() {
		$api = new API();
			
		$int_access = URL::getVar('access');
		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		$totalRecords = 0;
		
		$api->addAction(static::$base, 'cms_list_weblog', 'run', 
			array('int_start'=>$start,'int_limit'=>$limit,'vrc_name'=>'', 'int_access'=>$int_access));
		
		$list = $api->callMethod();
		if(count($list->data)) $totalRecords = current($list->data)->total_records;
		$pagination = new Pagination($current_page, $limit, $totalRecords);
		$data = $list->data;
		
		H::vars(compact('data','pagination'));
		if(!file_exists(H::path().H::file()))
			H::path('views/default/');

		return static::RENDER(static::$title, 'Modulos');
	}
		

	public static function create() { static::form();}
	public static function update() { static::form(H::cod());}
	private static function form($id = null) {	
		$errors = array();
		$vars = new stdClass();
		static::formJS();
		static::formCSS();
		
		$isSaved = !!@$_SESSION['saved'];
		if(isset($_SESSION['saved'])) 
			unset($_SESSION['saved']);
		
		$api = new API();
		$int_access = URL::getVar('access');
		$api->addAction(static::$base,static::$table_prefix,'info');
		$info = $api->callMethod();
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		$vars->labels = $vars->types = $vars->errors = $vars->lenght = $vars->ctypes = array();
		foreach($info->info as $k=>$i):
			$vars->labels[$k] = $i->label;
			$vars->types[$k] = $i->type;
			$vars->lenght[$k] = $i->lenght;
			$vars->ctypes[$k] = $i->ctype;
			
			if($i->pk)
				$pk = $k;
			
		endforeach;
		
		if($id):
			$params = array('wbl_int_id'=>$id, 'int_access'=>$int_access);
			$api->addAction(static::$base, 'cms_get_weblog', 'run', $params);
			$vars->data = $api->callMethod()->data[0];
		else:
			$vars->data = $info->data;
		endif;
		
		
		if(isset($_POST[static::$table_prefix])):
			$_POST[static::$table_prefix][$pk] = $id;
			$data = array();
			
			$post = $_POST[static::$table_prefix];
			$post['int_user_id'] = static::$user->id;
			$post['wbl_int_access'] = $int_access;
			$api->addAction(static::$base, static::$save_action, 'run', $post);
			$response = (array)$api->callMethodCombo();
			$res = end($response);
			if(count($res->errors)):
				$vars->errors = $res->errors;
			else: 
				$data = current($res->data);
				if(isset($data->message)):
					$msgs = array('REPEATED'=>'Este item já esta cadastrado.');
					$vars->errors[] = $msgs[$data->message];
				else:
					$_SESSION['saved'] = true;
					H::redirect(H::module(), 'update', $data->wbl_int_id, 'access:' . $int_access);
				endif;				
			endif;
		endif;
		$vars->allow = !!$id;
		$vars->isSaved = $isSaved;
		H::vars($vars);
		H::file('form.php');
		return static::RENDER(static::$title);
	}
	
	public static function gallery()
    {
		require 'controllers/storage/gallery.class.php';
        $linq = new Linq();
		gallery::modal(URL_ALIAS, 'Galeria');
        #return static::loadAction('storage/gallery', 'modal', true, array(URL_ALIAS));
    }
	
	public static function formJS($_js = array()) {
		$js = array(
			'form.js',
			'jquery/jquery.migrate.1.2.1.js',
			'jquery/jquery.metadata.js',
			'jquery/jquery.ui.core.js',
			'jquery/jquery.ui.widget.js',
			'jquery/jquery.ui.datepicker.js',
			'jquery/jquery.ui.datepicker-pt-BR.js',
			'jquery/jquery.maskMoney.js',
			'fileinput/fileinput.js', 
			'fileinput/fileinput_locale_pt-BR.js',
			'gallery.js',
			'cms/cms_weblog.js'
		);
		$js = array_merge($js, $_js);
		H::js($js);
	}
	
	public static function formCSS($_css = array()) {
		$css = array('form.css','boxes.css', 'smoothness/jquery-ui.css', 'fileinput.min.css', 'gallery.css', 'cms/cms_weblog.css');
		$css = array_merge($css, $_css);
		H::css($css);
	}
}