<?php
class controller {
	public static $table_prefix = 'form';
	public static $base = 'base';
	public static $list_action = '';
	public static $save_action = '';
	public static $delete_action = '';
	public static $title = 'Title';
	public static $model_default = '';
	public static $menu_tabs = 'views/default/menu.php';
	public static $view_params = array();
	public static $grid_params = array();
	protected static $user = null;
	
	public static function baseConfig(){
		if(file_exists(H::path().'menu.php'))
			static::$menu_tabs = H::path().'menu.php';
		
		H::vars(array(
			'table_prefix' => &static::$table_prefix,
			'menu_tabs' => &static::$menu_tabs,
			'view_params' => &static::$view_params,
			'grid_params' => &static::$grid_params
		));
	}
	
	public static function index() {
		$api = new API();
		$api->addAction(static::$base,static::$table_prefix,'info');
		$info = $api->callMethod();
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		
		foreach($info->info as $k=>$i)
			if($i->pk)
				$info->pk = $k;
			
		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		$totalRecords = 0;
		
		$api->addAction(static::$base, static::$list_action, 'run', array('int_start'=>$start,'int_limit'=>$limit,'vrc_name'=>''));
		$list = $api->callMethod();
		if(count($list->data)) $totalRecords = current($list->data)->total_records;
		$info->pagination = new Pagination($current_page,$limit,$totalRecords);
		$info->data = $list->data;
		
		H::vars($info);
		if(!file_exists(H::path().H::file()))
			H::path('views/default/');

		return static::RENDER(static::$title);
	}
	
	public static function create() { static::form();}
	public static function update() { static::form(H::cod());}
	private static function form($id = null) {	
		$errors = array();
		static::formJS();
		static::formCSS();
		
		$isSaved = !!@$_SESSION['saved'];
		if(isset($_SESSION['saved'])) 
			unset($_SESSION['saved']);
		
		$api = new API();
		$api->addAction(static::$base,static::$table_prefix,'info');
		$info = $api->callMethod();
		if(!!count($info->errors)):
			echo $info->errors[0];
			die;
		endif;
		$labels = $types = $errors = $lenght = array();
		foreach($info->info as $k=>$i):
			$labels[$k] = $i->label;
			$types[$k] = $i->type;
			$lenght[$k] = $i->lenght;
			
			if($i->pk)
				$pk = $k;
			
		endforeach;
		
		if($id):
			$params = array('pk'=>$id);
			$api->addAction(static::$base,static::$table_prefix,'single', $params);
			$data = $api->callMethod()->data;
		else:
			$data = $info->data;
		endif;
		
		
		if(isset($_POST[static::$table_prefix])):
			CObject::getData($data, $_POST[static::$table_prefix]);
			$_POST[static::$table_prefix][$pk] = $id;
			$data = array();
			$post = $_POST[static::$table_prefix];
			$post['user_int_id'] = static::$user->id;
			$api->addAction(static::$base, static::$save_action, 'run', $post);
			$res = $api->callMethod();
			
			if(count($res->errors)):
				$errors = $res->errors;
			else: 
				$data = current($res->data);
				if(isset($data->message)):
					$msgs = array('REPEATED'=>'Este item já está cadastrado.');
					$errors[] = $msgs[$data->message];	
				else:
					$_SESSION['saved'] = true;
					H::redirect(H::module(), 'update', $data->{$pk});
				endif;
			endif;
		endif;
		$allow = !!$id;
		
		H::vars(compact('labels', 'types', 'lenght', 'errors', 'data', 'isSaved', 'allow'));
		H::file('form.php');
		return static::RENDER(static::$title);
	}
	
	public static function delete() {
		$params = array('pk' => H::cod(), 'user_int_id' => static::$user->id);
		if(!$params['pk'])
			return URL::er500('Pk not found.');
		
		$api = new API();
		$api->addAction(static::$base, static::$delete_action, 'run', $params);
		$data = $api->callMethod();
		
		
		$msgs = array('INVALID_PK'=>'Item não encontrado');
		
		if(!count($data->errors)):
			$msg = current($data->data)->message;
			echo $msg == 'SUCCESS' ? $msg : $msgs[$msg];
		else:
			echo '500';
		endif;
	}
	
	public static function RENDER($title_module = 'NaN', $title_action = '') {
		$modal = URL::getVar('modal');
		if(empty($modal)):
			$title = (object)array('module'=>$title_module,'action'=>$title_action);
			H::vars(array('title' => $title));
			H::render('views/layout-novo/index.php');
		else:
			H::render(H::path().H::file());
		endif;
		return true;
	}
	

		
	public static function formJS($_js = array()) {
		$js = array(
			'jquery/jquery.migrate.1.2.1.js',
			'moment-with-locales.js',
			'moment-pt-br.js',
			'bootstrap-datetimepicker.js',
			'jquery.icheck.min.js',
			'select2.js', 
			'formModal.js',
			'form.js'
		);
		$js = array_merge($js, $_js);
		H::js($js);
	}
	
	
	public static function formCSS($_css = array()) {
		$css = array('bootstrap-fileupload.css','bootstrap-datetimepicker.css','jquery.simplecolorpicker.css', 'blue.css', 'select2.css', 'boxes.css', 'form.css');
		$css = array_merge($css, $_css);
		H::css($css);
	}
	
	protected static function loadAction($module, $action, $allow = true, $params = array()) {
		$ctrlFile = 'controllers/'.$module.'.class.php';
		if(file_exists($ctrlFile)):
			if(!$allow): return URL::er404();
			else: require_once $ctrlFile;
			endif;
		else:
			return URL::er404();
		endif;
		$class = strpos($module, '/') ? substr(strrchr($module, '/'), 1) : $module;
		
		H::path('views/' . $module . '/');
		if(method_exists( $class, 'CONFIG'))
			call_user_func( $class . '::CONFIG');
		
		if(method_exists( $class, $action))
			return call_user_func_array($class . '::' . $action, $params);
			
		return false;
	}
	
	/**
	* Para ajax request temos esse metodo de auxilio
	* As requisições ajax devem seguir este padrão
	* Requisições antigas devem ser modificadas (em momento oportuno)
	*
	*/
	public static function jsonData($errors, $res = '', $type=null){
		if($type === null && count($errors) > 0):
			$type = 'error';
		elseif($type === null):
			$type = 'success';
		endif;
		
		return CHTML::json(array(
			'errors'=>$errors,
			'res'=>$res,
			'type'=>$type
		));	
	}
	
	
	public static function sendNotification($mails, $subject, $msg) {
		return FormMail::send($mails, $subject, $msg, 'contato@55digital.com.br', 'PAINEL DE CONTROLE 55 DIGITAL', true);
	}
	

}