<?php
class API {
	private $data = null;
	
	public function __construct(){ 
		$this->data = array(); 
	}
	
	public function addAction($schema, $structure, $action, $data = array()) { 
		$this->data[] =
			array(
				'schema' => $schema,
				'structure' => $structure,
				'action' => $action,
				'data'=>$data
			);
	}
	
	public function clearActions() { $this->data = array(); }


	public function callMethod($token = null) {
		return $this->call(false, $token);
	}
	
	public function callMethodCombo() {
		return $this->call(true);
	}
	
	private function call($build_list = false) {
		if(!isset($_SESSION['token'])):
			$model = new DBModel('.confs/db.json');
			$dbmap = json_decode(file_get_contents('.confs/db_map.json'));
			$pl = 'prc_acs_get_token';
			$sql = sprintf('CALL `%s`.`%s`(?);', $dbmap->base, $pl);
			$res = $model->fetch($sql, array(URL_ALIAS));
			if(!$res):
				echo 'Invalid alias';die;
			else:
				$_SESSION['token'] = $token = $res->token;
				
				#var_dump($token);die;
			endif;
		else:
			$token = $_SESSION['token']; 
		endif;
		$post = array('data'=>json_encode($this->data));
		$this->clearActions();
		$ch = curl_init();
		//set the url, number of POST vars, POST data
		$get = !$build_list ? false : array('build_list'=>$build_list);
		$get = !$get ? '' : '?'. http_build_query($get);
		$url = API_URL . urlencode($token) . $get;
		curl_setopt($ch, CURLOPT_URL, $url);
		$fields_string = !$post ? null : http_build_query($post);
		$total_fields = !$fields_string ? 0 : substr_count($fields_string,'&') + 1;
		curl_setopt($ch, CURLOPT_POST, $total_fields);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);

		//execute post
		$json = curl_exec($ch);
		//close connection
		curl_close($ch);
		$response = json_decode($json);
		
		if(!is_object($response) && !is_array($response)) { 
			$response = (object)array('errors'=>array($json));
			return $response; 
		}
		
		if(!is_array($response)):
			foreach(array('errors') as $k)
				$response->{$k} = (array)$response->{$k};
		endif;
		return $response;
	}
}