<?php
class DBModel {
	public $PDO = null;
	public function __construct($conf_file) 
	{
		$conf = file_get_contents($conf_file);
		$dbconf = (object)json_decode($conf);
		$dns = sprintf('%s:dbname=%s;host=%s;port=%s', $dbconf->engine, $dbconf->database, $dbconf->host, $dbconf->port);
		$this->PDO =  new PDO($dns, $dbconf->user, $dbconf->pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		$this->PDO->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		$this->dbconf = $dbconf;
	}
	
	public function validType($type, $db, $tb) {
		
		$call = $this->callSchemaPl('validType', $db, $tb, '');
		$data = count($call) ? current($call) : null;
		if($data) return $data->type == strtoupper($type);
		return false;
	}
	
	public function tbInfo($db, $tb) {
		return $this->callSchemaPl('tbInfo', $db, $tb, '');
	}
	
	public function getParams($db, $pl) {
		$data = $this->callSchemaPl('getParams', $db, $pl, '');
		$params = array();
		foreach($data as $p) $params[] = $p->Col;
		return $params;
	}
	
	public function getPk($db, $tb) {
		$call = $this->callSchemaPl('getPk', $db, $tb, '');
		return count($call) ? current($call)->Column : null;
	}
	
	public function getFk($db, $tb, $col) {
		return $this->callSchemaPl('getFk', $db, $tb, $col);
	}
	
	protected function callSchemaPl($funcao, $table_schema, $table_name, $column_name){
		$dbmap =  json_decode(file_get_contents('db_map.json'));
		$sql = sprintf('CALL `%s`.`prc_information_schema`(?,?,?,?);', $dbmap->base);
		return $this->fetchAll($sql, array($table_schema, $table_name, $column_name, $funcao));
	}
	
	
	public function fetchAll($sql, $array_exec = array()) {
		$pdo = $this->PDO->prepare($sql);
		$pdo->execute($array_exec);
		return $pdo->fetchAll(PDO::FETCH_OBJ);
	}
	
	public function fetch($sql, $array_exec = array()) {
		$pdo = $this->PDO->prepare($sql);
		$pdo->execute($array_exec);
		return $pdo->fetch(PDO::FETCH_OBJ);
	}
	
	public function lastID(){
		$data = $this->fetch('SELECT IF(ISNULL(@LAST_ID), LAST_INSERT_ID(), @LAST_ID) as PK;');
		return !!$data ? $data->PK : null;
	}
	
	public function quote($value) {
		return $this->PDO->quote($value);
	}
	
	public function escape($value) {
		return trim($this->PDO->quote($value),'\'');
	}
}