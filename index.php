<?php 
date_default_timezone_set("America/Sao_Paulo"); ## TIMEZONE
$conf = (object)json_decode(file_get_contents('.confs/conf.json'));
define('MODEL_PATH',$conf->paths->model);
define('FILE_PATH',$conf->paths->file);
define('FAVICON',$conf->paths->favicon);
define('API_URL',$conf->api->url);
define('COMPANY',$conf->company);
#define('DATABASE_ACCESS',file_get_contents($conf->db_access_file));


#(1=Show, 2=Hide) Debug message
define('DEBUG',1);
$GLOBALS['AUTOLOAD'] = $conf->autoload;
include($conf->paths->framework . 'autoload.php');	## Auto-load classes

if(substr_count($_SERVER['SERVER_NAME'], $conf->base) > 0):
	define('URL_ALIAS', str_replace($conf->base,'',$_SERVER['SERVER_NAME']));
else:
	define('URL_ALIAS', URL::baseDomain());
endif;

H::title('',$conf->title,'-');
require_once $conf->paths->framework . 'required/BaseModel.class.php';
require_once $conf->paths->framework . 'required/PDOFramework.class.php';
require_once 'tools/DBModel.class.php';
require_once 'tools/API.class.php';
require_once 'controllers/controller.class.php';
require_once 'controllers/bootstrap.class.php';
#require_once '../models/Model.class.php';

session_name(md5(URL::site()));
session_start();

define('FILE_URL',URL::site());

# Smart Software bootstrap
bootstrap::INIT();
