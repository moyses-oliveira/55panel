<?php
header('Content-Type: text/html; charset=UTF-8');
$debug = array();
$debug[] = "";
$debug[] = "php_value error_reporting 7" . PHP_EOL . "php_flag display_errors on";
$debug[] = "php_value error_reporting 30719" . PHP_EOL . "php_flag display_errors on";
$download = !isset($_GET['download']) ? '' : $_GET['download'];
$required = array('upload','framework', 'model');
foreach($required as $path):
	if(!isset($_GET[$path])): 
		printf('Paramêtro \'%s\' é obrigatório.',$path); 
		die;
	else:
		${$path} = $_GET[$path];
	endif;	
endforeach;
$type = substr($upload, 0, 5) == 'http:' || substr($upload, 0, 6) == 'https:' ? '[R=301,L]' : '[L]';

$htaccess = sprintf('%s

<IfModule mod_rewrite.c>
	RewriteEngine on
	
	%s
	
	RewriteRule !\.(gif|jpg|jpeg|png|woff|ttf|css|js|xml|pdf|swf|php)$ index.php
	RewriteRule sitemap.xml sitemap.xml
</IfModule>',
!isset($_GET['debug']) ? '' : $debug[$_GET['debug']],
(empty($download) ? '' : sprintf('RewriteRule download/(.*)$ %s$1 %s',$download,$type))
);
$conf = new stdClass();
$conf->paths = new stdClass();
$conf->paths->framework = $framework;
$conf->paths->model = $model;
$conf->db_access_file = $model . 'access.json';
$conf->autoload = array();
$conf->autoload[] = array('path'=>'../oliveira/tools/', 'sufix' => '.class.php');
echo '<pre>';
echo htmlentities($htaccess), PHP_EOL . json_encode($conf);
