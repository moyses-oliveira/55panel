<?php
echo '<fieldset id="images">';
printf('<legend>Imagens</legend>');

$gallery_tpl = file_get_contents('views/dcms/gallery-item.tpl');

$root_path = URL_ALIAS;
$gallery_images = '';$img_data_list = array();
foreach($imgs as $ki=>$img):
	
	$img->name = basename($img->img);
	$path = substr($img->img, 0,-1 * strlen(basename($img->img)));
	$img_path = H::path_combine(array('files', 'personal', $root_path, 'gallery', 'images'), false, '/');
	$thumb_path = H::path_combine(array('files', 'personal', $root_path, 'gallery', 'thumbs'), false, '/') . $path;
	$img->thumb = H::root() . ImagePlugin::resize($img_path . $img->img, trim($thumb_path,'/'), 180, 135, false);
	$img->input_key = $ki;
	$params = array_keys((array)$img); 
	foreach($params as &$v) $v = '{' . $v . '}';
	$values = array_values((array)$img);
	$gallery_images .= str_replace($params, $values, $gallery_tpl);
	$gallery_images .= PHP_EOL . PHP_EOL;
endforeach;

$single_img = IMAGE == 'SINGLE';
printf('<div class="gallery-list post-gallery">%s</div>', $gallery_images);
printf('<a class="col-xs-12 btn btn-primary add_img_from_gallery" href="%s" _title="Galeria" target="#images .gallery-list" input_name="post_img" template="post_img" single="%s"><i class="fa fa-image"></i>&nbsp;%s</a>', H::link(H::module(),'gallery','modal:1'), $single_img * 1, $single_img ? 'Selecionar Imagem' : 'Adicionar Imagem');
echo '</fieldset>';