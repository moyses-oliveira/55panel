<?php 
echo '<div class="box-body table-responsive" style="padding: 10px;">' . PHP_EOL;
$grid = new gridView();
$grid->addColumn(new gridViewColumn('post_int_position', 'Pos', array('width'=>'50px')));

$grid_labels = array('postd_vrc_title'=>'Título', 'category'=>'Categoria');
foreach($grid_labels as $col=>$label)
	$grid->addColumn(new gridViewColumn($col, $label));

	
$urlUp = H::link(H::module(), 'move', '{post_int_id}', sprintf('?to=UP&redirect=%s',urlencode(URL::atual())));
$urlDown = H::link(H::module(), 'move', '{post_int_id}', sprintf('?to=DOWN&redirect=%s',urlencode(URL::atual())));
$btns = array(
	array('action' => '', 'class' => 'fa-chevron-up', 'title' => 'Mover acima', 'url'=>$urlUp ),
	array('action' => '', 'class' => 'fa-chevron-down', 'title' => 'Mover abaixo', 'url'=>$urlDown ),
	array('action' => 'update', 'class' => 'fa-pencil', 'title' => 'Editar'),
	array('action' => 'delete', 'class' => 'fa-close', 'title' => 'Remover')
);
$grid->addActionColumn(H::module(), 'post_int_id', '130px', $btns, false);
echo $grid->renderTable($data);
printf(
	'<div class="row">
		<div class="col-md-6">%s</div>
		<div class="col-md-6">%s</div>
	</div>
	',
	$pagination->getPaginationInfo(), 
	$pagination->getPaginationBar()
);
echo '</div>';
