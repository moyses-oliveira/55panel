<?php
$form = new form();

echo $form->sucessBox(@$isSaved, 'max-800 center');
if(@$isSaved):
	echo '<script type="text/javascript"> reloadTree(); </script>';
endif;
echo $form->warningBox($errors,'max-800 center');

$category_prefix = 'category';
$desc_prefix = 'desc';
echo $form->open($category_prefix,URL::atual(),'form-'.$category_prefix, array('class'=>'default-form max-800 center update_target'));
$tpl = '<div class="%s">%s</div>';


	echo '<div id="desc-br">';
	$k = 0;
	echo $form->hidden(array($desc_prefix,$k,'wblcatd_chr_language'),'pt_br');
	
	$key = 'wblcatd_vrc_title';$inf = $info->{$key};$attr = array('maxlength'=>$inf->lenght);
	printf($tpl,'col-md-12', $form->text(array($desc_prefix,$k, $key),$inf->label, $data->{$key},$inf->type, $attr));

	$key = 'wblcatd_vrc_description';$inf = $info->{$key};$attr = array('maxlength'=>$inf->lenght);
	printf($tpl,'col-md-12', $form->textarea(array($desc_prefix,$k, $key),$inf->label, $data->{$key},$inf->type, $attr));

	echo '</div>';
	
printf('<div class="col-md-12">%s<div class="clear"></div></div><br/>', $form->submit('Salvar','btn btn-primary button-right'));
echo $form->close();
?>
<script type="text/javascript">
	var modal_id = '#modal_' + msgBoxStatic.modal.uid + ' .modal-body';
	$(modal_id + ' form').attr('target', modal_id);
	globalFormConfig(modal_id);
</script>