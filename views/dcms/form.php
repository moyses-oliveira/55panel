<style type="text/css">
a.cat-edit { background-color: #23527C; vertical-align: top;}
a.cat-rm { background-color: #E5412D; vertical-align: top;}
a.cat-edit, a.cat-rm {
	font-size: 1.5em;
	padding: 4px 8px 5px 8px;
	margin-top: 1px;
	display: inline-block;
	border-radius: 3px;
	color: #fff;
}
a.cat-edit:hover, a.cat-rm:hover {
	opacity: 0.7;
}
</style>
<?php
$form = new form();

echo $form->sucessBox($isSaved, 'max-800 center');
echo $form->warningBox($errors,'max-800 center');
$desc_prefix = 'desc';
echo $form->open('post-form',URL::atual(),'form-post', array('class'=>'default-form max-800 center'));
$tpl = '<div class="%s">%s</div>';

$prefix = 'post';
$key = 'post_int_wblcat'; 
$attr = array('maxlength'=>$lenght[$key]);
$lattr = array('style'=>'display: block;');

if(!SINGLE_PAGE && CATEGORIES):
	$cl = $types[$key] . ' field-category';
	$field = $form->select(array($prefix,$key),$labels[$key], $data->{$key}, $category_options, $cl, $attr, $lattr);
	/*
	$field .= '
		<a href="#" class="cat-edit tltp" title="Editar"><i class="fa fa-pencil"></i></a>
		<a href="#" class="cat-rm tltp" title="Remover"><i class="fa fa-times"></i></a>
	';*/
	printf($tpl,'col-md-6', $field);
else:
	echo $form->hidden(array($prefix, $key), $data->{$key});
endif;

$key = 'post_dtt_posted'; 
$attr = array('maxlength'=>$lenght[$key]);
$label = DATE_FORMAT == 'DATETIME' ? 'Data e Hora' : 'Data';
$cls = DATE_FORMAT == 'DATETIME' ? 'datetime' : 'date';
$data->{$key} = CData::format('d/m/Y H:i', empty($data->{$key}) ? date('Y-m-d H:i') : $data->{$key});

if(DATE_FORMAT != 'NONE')
	printf($tpl,'col-md-6', $form->text(array($prefix,$key),$labels[$key], $data->{$key}, $cls , $attr));
else
	echo $form->hidden(array($prefix, $key), $data->{$key});

$prefix = 'desc'; $lang = 'pt_br';
echo $form->hidden(array($prefix, $lang, 'postd_chr_language'), $lang);

$key = 'postd_vrc_title'; $attr = array('maxlength'=>$lenght[$key]);
printf($tpl,'col-md-12', $form->text(array($prefix, $lang, $key),$labels[$key], htmlentities($data->{$key}), $types[$key], $attr));

$key = 'postd_vrc_summary'; $attr = array('maxlength'=>$lenght[$key],'style'=>'height: 55px;resize: none;');
if(SUMMARY)
	printf($tpl,'col-md-12', $form->textarea(array($prefix, $lang, $key),$labels[$key], $data->{$key}, $types[$key], $attr));
else
	echo $form->hidden(array($prefix, $lang, $key), $data->{$key});

$key = 'postd_txt_description'; $attr = array('maxlength'=>$lenght[$key],'style'=>'height: 210px;resize: none;');
printf($tpl,'col-md-12', $form->textarea(array($prefix, $lang, $key),$labels[$key], $data->{$key}, $types[$key] . ' html_textfield', $attr));

if(IMAGE != 'NONE'):
	include('images.php');
endif;

printf('<div class="col-md-12">%s<div class="clear"></div></div><br/>', $form->submit('Salvar','btn btn-primary pull-right'));

echo $form->close();
?>
<script type="text/javascript">
function refreshCat() {
	$('select.field-category').html('<option value="">#atualizando...</option>');
	$.get(ROOT + '<?php echo H::module();?>/refresh_category_options').done(
		function(content){
			$('select.field-category').html(content);
		});
}

$(document).ready(function(){
	staticGalleryPlugin.templates["post_img"] = "<?php echo str_replace(array('"',"\n","\r","\r\n",'{pk}','{position}'), array('\\"',"\\n","\\r","\\r\\n",'',''), $gallery_tpl);?>";
	$('#images .post-gallery').on("afterDelete", function(){
		lsPostImg.forcePositions();
	});
	$('#images .post-gallery').on("afterInsert", function(){
		lsPostImg.forcePositions();
	});
	
	$('a.cat-edit').click(function(){ return false; });
	$('a.cat-rm').click(function(){ return false; });
	
	
	
	
});
</script>