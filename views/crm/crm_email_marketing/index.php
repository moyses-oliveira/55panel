<?php 
echo '<div class="box-body table-responsive" style="padding: 10px;">' . PHP_EOL;
$grid = new gridView();
foreach($grid_params as $col)
	$grid->addColumn(new gridViewColumn($col, $info->{$col}->label));

$url = H::link(H::module(), 'send', '{tpl_int_id}');
$btns = array(
	array('action' => 'modules', 'class' => 'fa-share-alt-square', 'title' => 'Enviar pacote', 'url'=>$url)
);
	
$grid->addActionColumn(H::module(), 'int_access', '80px', $btns, false);
	
echo $grid->renderTable($data);
printf(
	'<div class="row">
		<div class="col-md-6">%s</div>
		<div class="col-md-6">%s</div>
	</div>
	',
	$pagination->getPaginationInfo(), 
	$pagination->getPaginationBar()
);
echo '</div>';
