<?php 
$form = new form();

echo $form->sucessBox($isSaved, 'max-800 center');
echo $form->warningBox($errors,'max-800 center');

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));
$tpl = '<div class="%s">%s</div>';

$attr = array('maxlength'=>255);
$key = 'email_vrc_name';
printf($tpl,'col-md-12', $form->text(array($table_prefix,$key),$labels[$key],$data->{$key},$types[$key], $attr));

$key = 'email_vrc_email';
printf($tpl,'col-md-12', $form->text(array($table_prefix,$key),$labels[$key],$data->{$key},$types[$key], $attr));

$key = 'email_vrc_extra';$attr = array('maxlength'=>3000, 'style'=>'height: 125px;');
printf($tpl,'col-md-12', $form->textarea(array($table_prefix,$key),$labels[$key],$data->{$key},$types[$key], $attr));

echo '<input type="hidden" value="1" class="count_groups" />';

echo '<fieldset style="border: 1px solid #aaa;padding: 10px 20px;"><legend>Grupo</legend>';

foreach($groups as $g):
	$chk = !empty($g->has_group) ? 'checked="checked"' : '';
	printf('<label style="display: inline-block;margin: 5px 10px;"><input type="checkbox" value="%s" %s name="group[]" /> &nbsp;%s</label>', 
		$g->group_int_id, $chk, $g->group_vrc_name);
		
endforeach;
echo '</fieldset>';

printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));

printf($form->close());
?>
<script type="text/javascript">
$(document).ready(function(){
	validator.warnings.count_groups = 'Selecione pelo menos 1 grupo.';
	validator.vTypes.push('count_groups');
	validator.test.count_groups = function(v){
		return $('input[name="group[]"]:checked').length > 0;
	}
});
</script>


