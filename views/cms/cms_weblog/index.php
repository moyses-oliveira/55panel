<?php 
echo '<div class="box-body table-responsive" style="padding: 10px;">' . PHP_EOL;
$grid = new gridView();

$cols = array(
	'company_vrc_name'=>'Nome',
	'company_vrc_name2'=>'Nome Fantasia',
	'plan_vrc_name'=>'Plano',
	'access_vrc_alias'=>'Alias',
);
foreach($cols as $col=>$label)
	$grid->addColumn(new gridViewColumn($col, $label));

$url = H::link(H::module(), 'modules', 'access:{int_access}');
$urln = H::link(H::module(), 'create', 'access:{int_access}');
$btns = array(
	array('action' => 'modules', 'class' => 'fa-share-alt-square', 'title' => 'Listar Módulos', 'url'=>$url),
	array('action' => 'modules', 'class' => 'fa-file', 'title' => 'Novo Módulo', 'url'=>$urln)
);
	
$grid->addActionColumn(H::module(), 'int_access', '80px', $btns, false);
echo $grid->renderTable($data);
printf(
	'<div class="row">
		<div class="col-md-6">%s</div>
		<div class="col-md-6">%s</div>
	</div>
	',
	$pagination->getPaginationInfo(), 
	$pagination->getPaginationBar()
);
echo '</div>';
