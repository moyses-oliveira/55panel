<?php 
$form = new form();

echo $form->sucessBox($isSaved, 'max-800 center');
echo $form->warningBox($errors,'max-800 center');

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));
$tpl = '<div class="%s">%s</div>';

$t = array(
	'NONE'=>'Não exibir',
	'SINGLE'=>'Uma imagem',
	'MULTIPLE'=>'Varias imagens',
	'DATE'=>'Data',
	'DATETIME'=>'Data e Hora',
	'POSITION'=>'Posição',
	'LASTEST'=>'Recentes'
);


$fields = array('wbl_vrc_alias', 'wbl_vrc_description', 'wbl_tny_single', 'wbl_tny_enable_category', 'wbl_tny_enable_summary', 'wbl_enm_publish_date', 'wbl_enm_order', 'wbl_enm_images', 'wbl_tny_enable_author', 'wbl_tny_enable_filter');
 
foreach($fields as $key):
	$attr = array('maxlength'=>$lenght[$key]);
	if(substr($key,0,8) == 'wbl_enm_'):
		$options = array();
		eval(str_replace(array('enum','ENUM'), '$opt = array', $ctypes[$key]) . ';');
		foreach($opt as $o)
			$options[] = array($o,$t[$o]);
		
		
	elseif(substr($key,0,8) == 'wbl_tny_'):
		$options = array(array(1,'Sim'), array(0, 'Não'));
	endif;
	$fieldType = $lenght[$key] > 1000 ? 'textarea' : 'text';
	
	if(isset($options)):
		printf($tpl,'col-md-12', $form->select(array('cms_weblog', $key), $labels[$key], $data->{$key}, $options, $types[$key], $attr));
		unset($options);
		continue;
	endif;
	
	if(H::action() == 'update' && $key == 'page_vrc_alias'):
		echo $form->hidden(array('cms_weblog', $key), $data->{$key}) . PHP_EOL;
		continue;
	endif;
	
	printf($tpl,'col-md-12', $form->$fieldType(array('cms_weblog', $key),$labels[$key], $data->{$key},$types[$key], $attr));
endforeach;

printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));
printf($form->close());