<?php 
echo '<div class="box-body table-responsive" style="padding: 10px;">' . PHP_EOL;
$grid = new gridView();

$cols = array(
	'wbl_vrc_description'=>'Descrição',
	'wbl_vrc_alias'=>'Alias'
);
foreach($cols as $col=>$label)
	$grid->addColumn(new gridViewColumn($col, $label));
	
$url = H::link(H::module(), 'update', '{wbl_int_id}', 'access:{wbl_int_access}');
$urln = H::link(H::module(), 'delete', '{wbl_int_id}', 'access:{wbl_int_access}');
	
$btn = array(	
	array('action' => 'update', 'class' => 'fa-pencil', 'title' => 'Editar', 'url'=> $url),
	array('action' => 'delete', 'class' => 'fa-close', 'title' => 'Remover', 'url'=> $url)
);
$grid->addActionColumn(H::module(), 'wbl_int_id', '80px', $btn , false);
echo $grid->renderTable($data);
printf(
	'<div class="row">
		<div class="col-md-6">%s</div>
		<div class="col-md-6">%s</div>
	</div>
	',
	$pagination->getPaginationInfo(), 
	$pagination->getPaginationBar()
);
echo '</div>';
