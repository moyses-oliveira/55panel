<?php 
$form = new form();
$msg = ''; 
if(count($errors) > 0): 
	$msg = '<ul>';
	foreach($errors as $k=>$v) $msg .= sprintf('<li>%s</li>',$v);
	$msg .= '</ul>';
endif;
printf('<div class="warnings">%s</div>',H::warningBox($msg, 'max-800 center','Atenção', count($errors) < 1));

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));
$tpl = '<div class="%s">%s</div>';


$fields = array('page_vrc_alias', 'page_vrc_title', 'page_txt_content', 'page_vrc_meta_keywords', 'page_vrc_meta_description');
 
foreach($fields as $key):
	$attr = array('maxlength'=>$lenght[$key]);
	if($key == 'page_txt_content'): $attr['style']='height: 225px;resize: none;';
	elseif($key == 'page_vrc_meta_description'):  $attr['style']='height: 110px;resize: none;';
	endif;
	$fieldType = $lenght[$key] > 1000 ? 'textarea' : 'text';
	
	if(H::action() == 'update' && $key == 'page_vrc_alias'):
		echo $form->hidden(array('cms_page', $key), $data->{$key}) . PHP_EOL;
		continue;
	endif;
	
	printf($tpl,'col-md-12', $form->$fieldType(array('cms_page', $key),$labels[$key], $data->{$key},$types[$key], $attr));
endforeach;
	
echo '<fieldset id="tab-images" class="col-md-12">';
echo '<legend>Imagens</legend>';

$gallery_tpl = file_get_contents('views/cms/cms_page/gallery-item.tpl');
$gallery_images = '';
$pathGalleryImg = 'files/personal/'. URL_ALIAS .'/gallery/images';
$pathGalleryThumb = 'files/personal/'. URL_ALIAS .'/gallery/thumbs';
foreach($img_data_list as $ki=>$img):
	$img->name = basename($img->img);
	$path = substr($img->img, 0,-1 * strlen(basename($img->img)));
	$img_path = H::path_combine(array($pathGalleryImg), false, '/');
	$thumb_path = H::path_combine(array($pathGalleryThumb), false, '/') . $path;
	$img->thumb = H::root() . ImagePlugin::resize($img_path . $img->img, trim($thumb_path,'/'), 180, 135, false);
	
	$img->input_key = $ki;
	$params = array_keys((array)$img); 
	foreach($params as &$v) $v = '{' . $v . '}';
	$values = array_values((array)$img);
	$gallery_images .= str_replace($params, $values, $gallery_tpl);
	$gallery_images .= PHP_EOL . PHP_EOL;
endforeach;

printf('<div class="gallery-list page-gallery">%s</div>', $gallery_images);
printf('<a class="col-xs-12 btn btn-primary add_img_from_gallery" href="%s" _title="Galeria" single="0" target="#tab-images .gallery-list" input_name="page_img" template="page_img"><i class="fa fa-plus"></i>&nbsp;Adicionar Imagem</a>', H::link(H::module(),'gallery','modal:1'));
echo '</fieldset>';

printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));

printf($form->close());

printf('
<script type="text/javascript">
	$(document).ready(function(){
		staticGalleryPlugin.templates["page_img"] = "%s";
		lsPageImg.forcePositions();
		$(\'#tab-images .page-gallery\').on("afterDelete", function(){
			lsPageImg.forcePositions();
		});
		$(\'#tab-images .page-gallery\').on("afterInsert", function(){
			lsPageImg.forcePositions();
		});
	});
</script>
',
	str_replace(array('"',"\n","\r","\r\n",'{pk}','{position}'), array('\\"',"\\n","\\r","\\r\\n",'',''), $gallery_tpl)
);


