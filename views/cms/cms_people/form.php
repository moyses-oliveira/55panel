<script type="text/javascript">
	
	$(document).on('change', '.btn-file :file', function () {
		var input = $(this), numFiles = input.get(0).files ? input.get(0).files.length : 1, label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [
			numFiles,
			label
		]);
	});
	$(document).ready(function () {
		$('.btn-file :file').on('fileselect', function (event, numFiles, label) {
			var input = $(this).parents('.input-group').find(':text'), log = numFiles > 1 ? numFiles + ' files selected' : label;
			if (input.length) {
				input.val(log);
			} else if (log) {
				
					alert(log);
			}
		});
	});
    
</script>
<style type="text/css">
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
</style>
<?php 
$form = new form();

echo $form->sucessBox($isUpdated, 'max-800 center');
echo $form->warningBox($errors,'max-800 center');

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));
$tpl = '<div class="%s">%s</div>';

$fields = array('people_vrc_name', 'people_vrc_email', 'people_vrc_text');

foreach($fields as $key):
	$attr = array('maxlength'=>$lenght[$key]);
	if($key == 'people_vrc_text'):
		$attr['style'] = 'height: 115px;resize: none;';
		printf($tpl,'col-md-12', $form->textarea(array('cms_people', $key),$labels[$key], $data->{$key}, $types[$key], $attr));
	else:
		printf($tpl,'col-md-12', $form->text(array('cms_people', $key),$labels[$key], $data->{$key}, $types[$key], $attr));
	endif;
endforeach;

$key = 'people_tny_enabled';
$options = array(array(0,'Não'), array(1,'Sim'));
printf($tpl,'col-md-12', $form->select(array('cms_people', $key),$labels[$key], $data->{$key}, $options, $types[$key]));

echo '
<div class="col-md-12">
	<div class="input-group">
		<span class="input-group-btn">
			<span class="btn btn-primary btn-file">
				Browse… <input type="file" name="people_vrc_image" accept="image/x-png, image/gif, image/jpeg">
			</span>
		</span>
		<input type="text" class="form-control" readonly="">
	</div>
</div>';
if(!empty($data->people_vrc_image)):
	$thumb = ImagePlugin::resize($pathImgs  . $data->people_vrc_image, trim($pathImgs . 'thumbs','/'), 320, 240, false);
	printf($tpl,'col-md-12 text-center', tag::img($thumb, '', ''));
endif;

printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));

printf($form->close());


