<?php 
$form = new form();

echo $form->sucessBox($isSaved, 'max-800 center');
echo $form->warningBox($errors,'max-800 center');

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));
$tpl = '<div class="%s">%s</div>';

$fields = array('faq_vrc_question', 'faq_vrc_answer');

foreach($fields as $key):
	$attr = array('maxlength'=>$lenght[$key]);
	$attr['style'] = $key == 'faq_vrc_question' ? 'height: 125px;resize: none;' : 'height: 225px;resize: none;';
	printf($tpl,'col-md-12', $form->textarea(array('cms_faq', $key),$labels[$key], $data->{$key},$types[$key], $attr));
endforeach;
	
printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));

printf($form->close());


