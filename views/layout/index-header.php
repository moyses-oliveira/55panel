<header class="header">
	<!--a class="logo" href="<?php echo H::root();?>">
		&nbsp;
	</a-->
	<!-- Header Navbar: style can be found in header.less -->
	<nav role="navigation" class="navbar navbar-static-top">
		<!-- Sidebar toggle button-->
		<a role="button" data-toggle="offcanvas" class="navbar-btn sidebar-toggle" id="btn-menu-principal" href="#">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="navbar-right">
			<ul class="nav navbar-nav">
				
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a data-toggle="dropdown" class="dropdown-toggle" href="#">
						<i class="glyphicon glyphicon-user"></i>
						
						<span><?php printf('%s',$user->name);?> <i class="caret"></i></span>
					</a>
					<ul class="dropdown-menu">
					
						<?php
							$ddm_tpl = '<li><a href="%s" title="%s" class="%s"><i class="%s"></i> %s</a></li>';
							printf($ddm_tpl,H::link('profile','index'),'Perfil','lime', 'glyphicon glyphicon-user','Perfil');
							printf($ddm_tpl,H::link('profile','change-password'),'Alterar Senha','orange', 'fa fa-key','Alterar Senha');
							
							printf($ddm_tpl,H::link('home','logout'),'Logout','red','fa fa-power-off','Logout');
						?>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>