<aside class="left-side sidebar-offcanvas collapse-left">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="menu-principal">
            <div class="clear"></div>
            <li style="width: 100%">
                <input type='text' name='menu-principal-busca' value='' id='field-menu-principal-busca' placeholder='Localizar' />
                <button class="btn btn-danger button-left fechar-menu" data-widget="remove" style="margin-left: 10px;"><i class="fa fa-times"></i></button>
                <div class="clear"></div>
            </li>
            <div class="clear"></div>
            <?php

            // Para evitar conflito com outras variaveis
            function _block_make_menu()
            {
				$menu = json_decode($_SESSION['menu']);
                $menu_agenda = (object) array('grupo' => 'Agenda', 'icone' => 'calendar', 'color' => 'blue', 'items' => array());
                $menu_agenda->items[] = (object) array('title' => 'Agenda', 'url' => H::link('base.age-agenda'));
                #$menu[] = $menu_agenda;
                foreach ($menu as $k => $m):
                    $active = false;
                    $item_template = '
				<li item_of="%3$s" >
					<a href="%1$s" class="bg-%2$s item_menu" tabindex="0">
						<span class="inner"><i class="fa fa-%3$s"></i>%4$s</span>
						<span class="clear"></span>
					</a>
				</li>
				';
                    $items = '';
                    $root = H::root();
                    $module = H::module();
                    foreach ($m->{'items'} as $c => $item):
                        $color = empty($m->{'color'}) ? 'red' : $m->{'color'};
                        $items .= sprintf($item_template, $item->{'url'}, $color, $m->{'icone'}, $item->{'title'}) . PHP_EOL;
                        if ($item->{'url'} == $root . $module)
                            $active = true;

                    endforeach;
                    printf('<div class="clear"></div><h3 group="%s">%s</h3>%s' . PHP_EOL, $m->{'icone'}, $m->{'grupo'}, $items
                    );
                endforeach;
            }

            _block_make_menu();
            ?>
            <div class="clear"></div>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
