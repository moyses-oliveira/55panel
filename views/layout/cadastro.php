<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<?php include("head.php"); ?>
</head>
<body class="bg-black">

	<div class="form-box" id="login-box">
	<div class="header">Recuperar Senha</div>
<?php
	$form = new form();
	echo $form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form body bg-gray'));
	$msg = '';
	if(count($errors) > 0):
		$msg = '<ul>';
		foreach($errors as $k=>$v):
			$msg .= sprintf('<li>%s</li>',$v);
		endforeach;
		$msg .= '</ul>';
	endif;
	
if(!$success):
	printf('<div class="warnings">%s</div>', H::warningBox($msg, 'center','Atenção', count($errors) < 1));
	
	$key = 'user_vrc_name'; $cls = $model->getType($key);
	echo $form->text(array($table_prefix,$key),$model->getLabel($key),$model->{$key},$cls);
	echo '<div style="margin-top: 10px;">';
	$key = 'user_vrc_email'; $cls = $model->getType($key);
	echo $form->text(array($table_prefix,$key),$model->getLabel($key),$model->{$key},$cls);
	echo '</div>';
		
	printf('<div style="margin-top: 10px;">%s<div class="clear"></div></div>', $form->submit('Cadastrar','btn bg-olive btn-block'));
	
	printf(
		'<div class="footer" style="text-align: right;">%s</div>',
		tag::a(H::link('home','login'),'Voltar para a tela de login.',
		'')
	);
else:
	echo "<h4 style='text-align: center;margin: 10px 15px;'>Uma senha foi enviada para seu e-mail.<br/> Efetue o login e modifique a senha.</h4>";
	printf(
		'<div class="footer" style="text-align: right;font-size: 1.4em;">%s</div>',
		tag::a(H::link('home','login'),'Voltar para a tela de login.','')
	);
endif;
	printf($form->close());
	
?>

	</div>
</body>
</html>