<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<?php include("head.php"); ?>
<style type="text/css">
.btn-yes  { background-color: #DFF0D8; border: 1px solid #AAA;} 
a.btn-no  { background-color: #F2DEDE; color: #000; border: 1px solid #AAA;}

</style>
</head>
<body class="bg-black">

<div class="form-box" id="login-box">
<div class="header">Login</div>
<?php
$form = new form();
$msg = '';
if(count($errors) > 0):
	$msg = '<ul>';
	foreach($errors as $k=>$v):
		$msg .= sprintf('<li>%s</li>',$v);
	endforeach;
	$msg .= '</ul>';
endif;
printf('<div class="warnings"  style="background-color: #fff; border: 1px solid #fff;margin: 0;padding: 10px 10px 0 10px;">%s</div>',H::warningBox($msg, 'center','Atenção', count($errors) < 1));
echo $form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form body'));

//$user = current($data);

$key = 'user_vrc_email';
$_POST[$table_prefix][$key] = isset($_POST[$table_prefix][$key]) ? $_POST[$table_prefix][$key] : '';
printf('<div>%s</div>', $form->text(array($table_prefix,$key),$info->{$key}->label, $_POST[$table_prefix][$key], $info->{$key}->type));

$tpl = '<div style="margin-top: 10px;">%s</div>';
$key = 'user_vrb_pass';
printf($tpl, $form->pass(array($table_prefix,$key),$info->{$key}->label,'',$info->{$key}->type));

printf($tpl, $form->submit('Login','btn bg-blue btn-block'));

printf($form->close());
?>
<div class="footer" style='text-align: right;'>         
	<!-- a href="<?php echo H::link('home','recovery');?>">Esqueceu a senha?</a -->
</div>

</div>
</body>
</html>