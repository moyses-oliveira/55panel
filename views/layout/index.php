<!DOCTYPE html>
<html>
<head>
<?php include("head.php"); ?>
</head>
<body class="skin-blue">
	<?php include("index-header.php"); ?>
	
	<div class="wrapper row-offcanvas row-offcanvas-left" style="min-height: 392px;">
		<!-- Left side column. contains the logo and sidebar -->
		<?php include("index-menu.php"); ?>

		<!-- Right side column. Contains the navbar and content of the page -->
		<aside class="right-side">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					<?php echo $title->module; ?>
					<small><?php echo $title->action; ?></small>
				</h1>
			</section>
			
			<!-- Main content -->
			<section class="content">
				<?php
				if(file_exists($menu_tabs)):
					include($menu_tabs);
				endif;
				?>
				<div>
				<?php include(H::path().H::file()); //pagina onde fica o conteudo ?>
				</div>
			</section><!-- /.content -->
		</aside><!-- /.right-side -->
	</div>
</body>
</html>