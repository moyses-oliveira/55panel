<?php 
$form = new form();

if($message):
	printf('<div class="%s" style="padding-top: 20px;" >%s</div>',
			'max-800 center', H::msgBox('<pre>'.$message.'</pre>', true, H::SUCCESS)
		);
endif;
echo $form->warningBox($errors,'max-800 center');

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));

$tpl = '<div class="%s">%s</div>';

$key = 'user';
$attr = array('maxlength'=>'255');
printf($tpl,'col-md-12', $form->text(array($key), 'Usuário', @$_POST['user'],'not_null', $attr));

$key = 'pass';
$attr = array('maxlength'=>'255');
printf($tpl,'col-md-12', $form->pass(array($key), 'Senha', '', 'not_null', $attr));

printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));

printf($form->close());