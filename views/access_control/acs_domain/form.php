<?php 
$form = new form();

echo $form->sucessBox(@$isSaved, 'max-800 center');
echo $form->warningBox($errors,'max-800 center');

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));

$tpl = '<div class="%s">%s</div>';

$key = 'domain_vrc_name';
$attr = array('maxlength'=>$lenght[$key]);
printf($tpl,'col-md-12', $form->text(array($table_prefix,$key), $labels[$key], $data->{$key},'not_null', $attr));

$key = 'domain_vrc_repository';
$attr = array('maxlength'=>$lenght[$key]);
printf($tpl,'col-md-12', $form->text(array($table_prefix,$key), $labels[$key], $data->{$key},'not_null', $attr));

$key = 'vhostconf';
if(!$data->domain_int_id):
	echo $form->hidden(array($table_prefix,$key), '');
else:
	printf($tpl,'col-md-12', $form->textarea(array($table_prefix,$key), 'Virtual Host Config', $$key,'not_null', array('style'=>'height: 400px;')));
endif;

printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));

printf($form->close());