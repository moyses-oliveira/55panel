<?php 
echo '<div class="box-body table-responsive" style="padding: 10px;">' . PHP_EOL;
$grid = new gridView();

$grid->addColumn(new gridViewColumn('domain', 'Domínio'));
	
$grid->addActionColumn(H::module(), 'domain', '80px', array(
                array('action' => 'update', 'class' => 'fa-pencil', 'title' => 'Editar'),
                array('action' => 'update-version', 'class' => 'fa-refresh', 'title' => 'Atualizar Versão')
                #array('action' => 'delete', 'class' => 'fa-close', 'title' => 'Remover')
            ), false);

echo $grid->renderTable($data);

printf(
	'<div class="row">
		<div class="col-md-6">%s</div>
		<div class="col-md-6">%s</div>
	</div>
	',
	$pagination->getPaginationInfo(), 
	$pagination->getPaginationBar()
);

echo '</div>';
