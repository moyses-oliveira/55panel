<VirtualHost *:80>
    ServerAdmin postmaster@55digital.com.br
    ServerName {url}
    ServerAlias {url}
    DocumentRoot /var/www/customers/{url}/
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

<VirtualHost *:80>
    ServerAdmin postmaster@55digital.com.br
    ServerName {url}
    ServerAlias www.{url}
    DocumentRoot /var/www/customers/{url}/
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

<VirtualHost *:80>
    ServerAdmin postmaster@55digital.com.br
    ServerName {url}.55websites.com.br
    DocumentRoot /var/www/customers/{url}/
    ServerAlias {url}
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
