<?php 
$form = new form();
$msg = ''; 

echo $form->sucessBox($isSaved, 'max-800 center');
echo $form->warningBox($errors,'max-800 center');

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center','autocomplete'=>'off')));
$tpl = '<div class="%s">%s</div>';

$key = 'user_vrc_name';
printf($tpl,'col-md-12', $form->text(array($table_prefix,$key),$labels[$key],$data->{$key},$types[$key]));

$key = 'user_vrc_email';
$val = $data->{$key};
if(isset($_POST[$table_prefix]['alias'])):
	$alias = $_POST[$table_prefix]['alias'];
	$domain = $_POST[$table_prefix]['domain'];
else:	
	list($alias, $domain) = !empty($val) ? explode('@', $val) : array('',null);
endif;
$options = array();
if(URL::is_domain(URL_ALIAS)):
	$options[] = array(URL_ALIAS, '@'.URL_ALIAS);
else:
	foreach($domains as $d):
		$options[] = array($d,'@'.$d);
	endforeach;
endif;
printf($tpl,'col-md-6', $form->text(array($table_prefix,'alias'),'E-mail',$alias, 'email_alias not_null', array('placeholder'=>'seu.nome', 'autocomplete'=>'off')));
printf($tpl,'col-md-6', $form->select(array($table_prefix,'domain'),'&nbsp;',$domain, $options, ''));

$key = 'user_vrb_pass';
$cls = H::action() == 'create' ? 'not_null password' : 'password';
printf($tpl,'col-md-12', $form->pass(array($table_prefix,$key),$labels[$key],'', $cls, array('placeholder'=>'******', 'autocomplete'=>'off')));

$cols = array(array('profile_vrc_name','Perfil'));
$grid = new gridView();

$col = new gridViewColumn('','&nbsp;',array('width'=>'50px'),'action-column');
$col->setTemplate('<input name="access[]" type="checkbox" value="{profile_int_id}" class="icheckbox_minimal" {has_user}/>');
$grid->addColumn($col);

foreach($cols as $i) 
  $grid->addColumn(new gridViewColumn($i[0],$i[1]));
 
#$grid->addActionColumn(H::module(),'smp_int_id', '110px');
echo '<div class="clear"></div><h3 class="text-center">Perfis de Acesso</h3>';
printf('<div class="col-md-12" style="overflow: auto;max-height: 200px;">%s</div>',$grid->renderTable($profiles));

printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));

echo $form->close();


