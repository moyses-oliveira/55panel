<?php 
$form = new form();

echo $form->sucessBox($isSaved, 'max-800 center');
echo $form->warningBox($errors,'max-800 center');

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));
$tpl = '<div class="%s">%s</div>';


$key = 'plan_int_system';
//$options = API::callMethod('access_control/acs_system','getOptions');
//array_unshift($options,array('','Selecione'));
//$options = array(array('','Selecione'));
printf($tpl,'col-md-12',$form->select(array($table_prefix,$key),$labels[$key],$data->{$key},$options,$types[$key]));

$key = 'plan_vrc_name';
printf($tpl,'col-md-12', $form->text(array($table_prefix,$key),$labels[$key],$data->{$key},$types[$key]));

$key = 'plan_vrc_alias';
printf($tpl,'col-md-12', $form->text(array($table_prefix,$key),$labels[$key],$data->{$key},$types[$key]));


if(H::action() == 'update'):
	$prdt = H::cod();
	
	$grid = new gridView();
	$labels = array('Nome', 'Alias', 'Grupo');
	foreach(array('modl_vrc_name','modl_vrc_alias','group_vrc_name') as $k=>$key) 
	  $grid->addColumn(new gridViewColumn($key,$labels[$k]));

	$tpl = tag::a(H::link(H::module(),'update_rel','{modl_plan_int_id}','plan:'.$prdt,'modal:1','?url='.urlencode(URL::atual())),'<i class="fa fa-pencil"></i>','Editar',' class="update load_modal_url" _title="Editar"');
	$tpl .= tag::a(H::link(H::module(),'delete_rel','{modl_plan_int_id}','plan:'.$prdt,'modal:1'),'<i class="fa fa-close"></i>','Remover',' class="delete"');
	$col = new gridViewColumn('','&nbsp;',array('width'=>'70px'),'action-column');
	$col->setTemplate($tpl);

	$grid->addColumn($col);

	if(URL::getVar('modal'))
		$grid->renderTable($modules);
	
	$content = '<i class="fa fa-plus-square" style="margin-right: 5px;"></i> Inserir Modulo';
	$link = H::link(H::module(),'create_rel','plan:'.H::cod(),'modal:1','?url='.urlencode(URL::atual()));
	$inserir = tag::a($link,$content,'Inserir Modulo','class="update load_modal_url lime btn btn-primary col-xs-12"');
	printf('
	<div>
		<h4 class="text-center">Modulos deste plano</h4>
		%s
		<div style="clear: both;"></div>
		%s
	</div>
	',
	$inserir,
	$grid->renderTable($modules)
	);
endif;


printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));

printf($form->close());


