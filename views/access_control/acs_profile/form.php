<?php 
$form = new form();

echo $form->sucessBox($isSaved, 'max-800 center');
echo $form->warningBox($errors,'max-800 center');

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));
$tpl = '<div class="%s">%s</div>';

$key = 'profile_vrc_name';
printf($tpl,'col-md-12', $form->text(array($table_prefix,$key),$labels[$key],$data->{$key},$types[$key]));

echo '<div class="clear"></div>';

$cols = array(array('group_vrc_name','Grupo'), array('name','Modulo'), array('alias','Alias Modulo'));
$grid = new gridView();

$col = new gridViewColumn('','&nbsp;',array('width'=>'50px'),'action-column');
$col->setTemplate('<input name="{prefix}[]" type="checkbox" value="{id}" class="icheckbox_minimal" {checked}/>');
$grid->addColumn($col);

foreach($cols as $i) 
  $grid->addColumn(new gridViewColumn($i[0],$i[1]));
 
printf('<fieldset class="col-md-12" style="overflow: auto;max-height: 200px;">
	<legend>Modulos</legend>
	%s
</fieldset>',$grid->renderTable($modules));
/*
if(count($wbl)):
	$cols = array(array('wbl_vrc_description','Modulo'), array('wbl_vrc_alias','Alias Modulo'));
	$grid = new gridView();

	$col = new gridViewColumn('','&nbsp;',array('width'=>'50px'),'action-column');
	$col->setTemplate('<input name="wblp[]" type="checkbox" value="{wbl_int_id}" class="icheckbox_minimal" {has_weblog}/>');
	$grid->addColumn($col);

	foreach($cols as $i) 
	  $grid->addColumn(new gridViewColumn($i[0],$i[1]));
	  
	printf('<fieldset class="col-md-12" style="overflow: auto;max-height: 200px;">
		<legend>Modulos</legend>
		%s
	</fieldset>',$grid->renderTable($wbl));
endif;
*/
echo '<div class="clear"></div>';
printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));
echo '<div class="clear"></div>';
printf($form->close());


