<div class='max-800 center view'>
  <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
    <thead>
      <tr class='legenda'>
        <th>Label</th>
        <th style='width: 80%;'>Valor</th>
      </tr>
    </thead>
    <tbody role="alert" aria-live="polite" aria-relevant="all">
      <?php 
	$params = CObject::toArray($data);
	foreach($data::getFieldsShow() as $K):
		if(!is_array($params[$K]) && !is_object($params[$K])):
		echo "<tr class='grid'> 
			<td>".$data->getLabel($K)."</td>
			<td>".H::limit($params[$K],200)."</td>
		</tr>";
		endif;
	endforeach;
	$content = '';
	foreach($acessos as $a)
		if(!empty($a->checked))
			$content .= sprintf('<li>%s - %s</li>', $a->group_vrc_name, $a->modl_vrc_name);
			
	printf('<tr><td>Acessos</td><td><ul>%s</ul></td></tr>',$content);
?>
    </tbody>
  </table>
</div>