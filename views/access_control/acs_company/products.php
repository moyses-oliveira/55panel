<?php 
echo '<div class="box-body table-responsive" style="padding: 10px;">' . PHP_EOL;

$grid = new gridView();

$cols = array(
	'plan_vrc_name'=>'Plano',
	'access_vrc_alias'=>'Alias'
);

foreach($cols as $col=>$label)
	$grid->addColumn(new gridViewColumn($col, $label));

$tpl = tag::a(H::link(H::module(),'product-update','{company_pack_int_id}','company:'.H::cod(),'modal:1','?url='.urlencode(URL::atual())),'<i class="fa fa-pencil"></i>','Editar',' class="update load_modal_url" _title="Editar"');
$tpl .= tag::a(H::link(H::module(),'product-delete','{company_pack_int_id}','company:'.H::cod(),'modal:1'),'<i class="fa fa-close"></i>','Remover',' class="delete"');
$col = new gridViewColumn('','&nbsp;',array('width'=>'70px'),'action-column');
$col->setTemplate($tpl);

$grid->addColumn($col);


$content = '<i class="fa fa-plus-square" style="margin-right: 5px;"></i> Inserir Modulo';
$link = H::link(H::module(),'product-create','company:'.H::cod(),'modal:1','?url='.urlencode(URL::atual()));
echo tag::a($link,$content,'Inserir Modulo','class="update load_modal_url lime btn btn-primary btn-block"');
echo $grid->renderTable($data);
printf(
	'<div class="row">
		<div class="col-md-6">%s</div>
		<div class="col-md-6">%s</div>
	</div>
	',
	$pagination->getPaginationInfo(), 
	$pagination->getPaginationBar()
);
echo '</div>';
