<?php 
$form = new form();
$msg = ''; 
 if(count($errors) > 0): 
	$msg = '<ul>';
	foreach($errors as $k=>$v) $msg .= sprintf('<li>%s</li>',$v);
	$msg .= '</ul>';
endif;
printf('<div class="warnings">%s</div>',H::warningBox($msg, 'max-800 center','Atenção', count($errors) < 1));

echo $form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'update_target default-form max-800 center'));
$tpl = '<div class="%s">%s</div>';

$key = 'company_int_id';
echo $form->hidden(array($table_prefix,$key),URL::getVar('company'));

$key = 'pack_vrc_alias';
printf($tpl,'col-md-12',$form->text(array($table_prefix,$key), 'Alias', $data->{$key}, 'not_null'));

$key = 'plan_int_id';
printf($tpl,'col-md-12',$form->select(array($table_prefix,$key), 'Plano', $data->{$key}, $options_plan, 'not_null'));


printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));

printf($form->close());

printf('
	<script type="text/javascript">
		var update_target = \'#modal_\' + msgBoxStatic.modal.uid + \' .modal-body\';
		$(\'#form-%s\').attr(\'target\',update_target);
		globalFormConfig(update_target);
		DivMsgWidget(update_target);
	</script>
	',
	$table_prefix,
	$table_prefix
);

