<?php 
echo '<div class="box-body table-responsive" style="padding: 10px;">' . PHP_EOL;
$grid = new gridView();
foreach($grid_params as $col)
	$grid->addColumn(new gridViewColumn($col, $info->{$col}->label));

$btns = array(
	array('action' => 'products', 'class' => 'fa-cube', 'title' => 'Produtos'),
	array('action' => 'update', 'class' => 'fa-pencil', 'title' => 'Editar'),
	array('action' => 'delete', 'class' => 'fa-close', 'title' => 'Remover')
);
$grid->addActionColumn(H::module(), $pk, '110px',$btns, false);
echo $grid->renderTable($data);
printf(
	'<div class="row">
		<div class="col-md-6">%s</div>
		<div class="col-md-6">%s</div>
	</div>
	',
	$pagination->getPaginationInfo(), 
	$pagination->getPaginationBar()
);
echo '</div>';
