<?php 
$form = new form();
$msg = ''; 
 if(count($errors) > 0): 
	$msg = '<ul>';
	foreach($errors as $k=>$v) $msg .= sprintf('<li>%s</li>',$v);
	$msg .= '</ul>';
endif;
printf('<div class="warnings">%s</div>',H::warningBox($msg, 'max-800 center','Atenção', count($errors) < 1));

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));
$tpl = '<div class="%s">%s</div>';

$key = 'modl_plan_int_module';
$cls = $model->getType($key);
$model_combo = new mdl_acs_module();
$combo_lst = array(array('','Selecione'));
foreach($model_combo->findAll() as $m) $combo_lst[] = array($m->modl_int_id, $m->modl_vrc_name);
printf($tpl,'col-md-12',$form->select(array($table_prefix,$key),$model->getLabel($key),$model->{$key},$combo_lst,$cls));

$key = 'modl_plan_int_plan';
$cls = $model->getType($key);
$model_combo = new mdl_acs_plan();
$combo_lst = array(array('','Selecione'));
foreach($model_combo->findAll() as $m) $combo_lst[] = array($m->plan_int_id, $m->plan_vrc_name);
printf($tpl,'col-md-12',$form->select(array($table_prefix,$key),$model->getLabel($key),$model->{$key},$combo_lst,$cls));

$key = 'modl_plan_int_group';
$cls = $model->getType($key);
$model_combo = new mdl_acs_group();
$combo_lst = array(array('','Selecione'));
foreach($model_combo->findAll() as $m) $combo_lst[] = array($m->group_int_id, $m->group_vrc_name);
printf($tpl,'col-md-12',$form->select(array($table_prefix,$key),$model->getLabel($key),$model->{$key},$combo_lst,$cls));

printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));

printf($form->close());


