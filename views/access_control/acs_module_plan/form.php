<?php 
$form = new form();

echo $form->sucessBox($isSaved, 'max-800 center');
echo $form->warningBox($errors,'max-800 center');

echo $form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'update_target default-form max-800 center'));
$tpl = '<div class="%s">%s</div>';

$key = 'modl_plan_int_plan';
echo $form->hidden(array($table_prefix,$key),URL::getVar('plan'));

$key = 'modl_plan_int_module';
printf($tpl,'col-md-12',$form->select(array($table_prefix,$key),$labels[$key],$data->{$key},$options_modules,$types[$key]));

$key = 'modl_plan_int_group';
printf($tpl,'col-md-12',$form->select(array($table_prefix,$key),$labels[$key],$data->{$key},$options_groups,$types[$key]));

printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));

printf($form->close());

printf('
	<script type="text/javascript">
		var update_target = \'#modal_\' + msgBoxStatic.modal.uid + \' .modal-body\';
		$(\'#form-%s\').attr(\'target\',update_target);
		globalFormConfig(update_target);
		DivMsgWidget(update_target);
	</script>
	',
	$table_prefix,
	$table_prefix
);

