<nav id="sidebar">		
	<ul id="main-nav" class="open-active">
	<?php

	// Para evitar conflito com outras variaveis
	function _block_make_menu()
	{
		$menu = json_decode($_SESSION['menu']);
		#$menu_agenda = (object) array('grupo' => 'Agenda', 'icone' => 'calendar', 'color' => 'blue', 'items' => array());
		#$menu_agenda->items[] = (object) array('title' => 'Agenda', 'url' => H::link('base.age-agenda'));
		#$menu[] = $menu_agenda;
		
		$item_template = '
			<li item_of="%3$s" class="%2$s">
				<a href="%1$s"><i class="fa fa-%5$s"></i>%4$s</a>
			</li>';
		foreach ($menu as $k => $m):
			$active = false;
			$items = '';
			$root = H::root();
			$module = H::module();
			foreach ($m->{'items'} as $c => $item):
				$item_active = false;
				if ($item->{'url'} == $root . $module)
					$item_active = $active = true;
				
				$items .= sprintf($item_template, $item->{'url'}, $item_active ? 'active' : '', $m->{'icone'}, $item->{'title'}, 'chevron-right') . PHP_EOL;
			endforeach;
			printf('
			<li class="dropdown %s">
				<a href="javascript:;">
					<i class="fa fa-%s"></i>%s<span class="caret"></span>
				</a>
				<ul class="sub-nav">
				%s
				</ul>
			</li>',
				$active ? 'active opened' : '',
				$m->{'icone'},
				$m->{'grupo'},
				$items
			);
			#printf('<div class="clear"></div><h3 group="%s">%s</h3>%s' . PHP_EOL, $m->{'icone'}, $m->{'grupo'}, $items);
		endforeach;
		printf($item_template, H::link('webmail'), H::module() == 'webmail' ? 'active_single' : '', '','Webmail', 'envelope');
		printf($item_template, H::link('diary'), H::module() == 'diary' ? 'active_single' : '', '','Agenda', 'calendar');
	}

	_block_make_menu();
	?>		

	</ul>
</nav> <!-- #sidebar -->