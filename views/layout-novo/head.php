<?php
printf('
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<script type="text/javascript">
	var BASE_URL = \'%s\';
	var ROOT = \'%s\';
	var URI = \'%s\';
	var newDate = new Date;
	var URLInfo = { module: function() { return \'%s\' }, action: function() { return \'%s\'}, cod: function() { return \'%s\'} };
</script>
', 
	H::site(),
	H::root(),
	$_SERVER['REQUEST_URI'],
	H::module(),
	H::action(),
	H::cod()
);
$js = H::js();
$css = H::css();

foreach($js as $v)
	printf('<script src="%s" type="text/javascript"></script>' . PHP_EOL, 
		(substr_count($v, '://') > 0 ?  $v : H::root() . FILE_PATH . 'js/'.$v)
	);
	
foreach($css as $v)
	printf('<link href="%s" rel="stylesheet" type="text/css">' . PHP_EOL, 
		(substr_count($v, '://') > 0 ?  $v : H::root() . FILE_PATH . 'css/'.$v)
	);

printf('
<link rel="shortcut icon" href="%1$s" type="image/x-icon"/>
<link rel="icon" href="%1$s" type="image/gif" />
<title>%2$s</title>',
	H::root() . FILE_PATH . FAVICON,
	H::title()
);
	