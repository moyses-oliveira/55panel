<!DOCTYPE html>
<!-- saved from url=(0061)?page=index -->
<html class=""><!--<![endif]-->
<head>
<?php include("head.php"); ?>
</head>
<body cz-shortcut-listen="true">

<div id="wrapper">
	<h1 id="site-logo">
		<a href="<?php echo URL::root();?>">
			<img src="<?php echo URL::root();?>files/img/logo.png" alt="Site Logo">
		</a>
	</h1>

	<header id="header">

			

		<a href="javascript:;" data-toggle="collapse" data-target=".top-bar-collapse" id="top-bar-toggle" class="navbar-toggle collapsed">
			<i class="fa fa-cog"></i>
		</a>

		<a href="javascript:;" data-toggle="collapse" data-target=".sidebar-collapse" id="sidebar-toggle" class="navbar-toggle collapsed">
			<i class="fa fa-reorder"></i>
		</a>

	</header> <!-- header -->


	<nav id="top-bar" class="collapse top-bar-collapse">

		<ul class="nav navbar-nav pull-left">
			<li class=""><a href="<?php echo URL::root();?>"><i class="fa fa-home"></i> Home</a></li>
			<li class=""><a href="<?php echo H::link('webmail','index');?>"><i class="fa fa-envelope"></i> Webmail</a></li>

			<!--li class="dropdown">
		    	<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
		        	Dropdown <span class="caret"></span>
		    	</a>

		    	<ul class="dropdown-menu" role="menu">
			        <li><a href="javascript:;"><i class="fa fa-user"></i>&nbsp;&nbsp;Example #1</a></li>
			        <li><a href="javascript:;"><i class="fa fa-calendar"></i>&nbsp;&nbsp;Example #2</a></li>
			        <li class="divider"></li>
			        <li><a href="javascript:;"><i class="fa fa-tasks"></i>&nbsp;&nbsp;Example #3</a></li>
		    	</ul>
		    </li-->
		    
		</ul>

		<ul class="nav navbar-nav pull-right">
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
					<i class="fa fa-user"></i>
		        	<?php echo $user->name;?>
		        	<span class="caret"></span>
		    	</a>

		    	<ul class="dropdown-menu" role="menu">
			        <li><a href="<?php echo H::link('profile','index');?>"><i class="fa fa-user"></i>&nbsp;&nbsp;Meu perfil</a></li>
			        
			        <li><a href="<?php echo H::link('profile','change-password');?>"><i class="fa fa-key"></i>&nbsp;&nbsp;Alterar Senha</a></li>
					<!--
					<li><a href="?page=page-calendar"><i class="fa fa-calendar"></i>&nbsp;&nbsp;Minha Agenda</a></li>
			        <li><a href="?page=page-settings"><i class="fa fa-cogs"></i>&nbsp;&nbsp;Settings</a></li>
					-->
			        <li class="divider"></li>
			        <li><a href="<?php echo H::link('home','logout');?>"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;Logout</a>
			        </li>
		    	</ul>
		    </li>
		</ul>

	</nav> <!-- /#top-bar -->


	<div id="sidebar-wrapper" class="collapse sidebar-collapse">
		<!--div id="search">
			<form>
				<input class="form-control input-sm" type="text" name="search" placeholder="Search...">
				<button type="submit" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
			</form>		
		</div> <!-- #search -->
		<?php include('menu.php'); ?>
	</div> <!-- /#sidebar-wrapper -->
	
	<div id="content">		
		
		<div id="content-header">
			<h1><?php printf('%s <small>%s</small>', $title->module, $title->action);?></h1>
		</div> <!-- #content-header -->
		
		<?php
		if(file_exists($menu_tabs)):
			echo '<div class="top-container-tabs">';
			include($menu_tabs);
			echo '</div>';
		endif;
		?>
		<div id="content-container">
		
		<?php include(H::path().H::file()); //pagina onde fica o conteudo ?>
		
		</div> <!-- /#content-container -->
		

	</div> <!-- #content -->
</div> <!-- #wrapper -->

<footer id="footer">
	<ul class="nav pull-right">
		<li>
			55 websites - 55 41 9737-7925 - atendimento@55websites.com.br
		</li>
	</ul>
</footer>

<div id="basicModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title">Basic Modal</h3>
      </div>
      <div class="modal-body">
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.</p>

        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="styledModal" class="modal modal-styled fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title">Styled Modal</h3>
      </div>
      <div class="modal-body">
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.</p>

        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-tertiary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<a id="back-to-top" href="?page=index#top" style="display: none;"><i class="fa fa-chevron-up"></i></a>

</body>
</html>