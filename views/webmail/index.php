<?php 
echo '<div class="dev-email">';

	include('navigation.php');

	echo '<div class="dev-email-messages col-md-9 col-sm-8">' . PHP_EOL;
	if(count($data)):
		$pagination_info = $pagination->getPaginationInfo();
		$pagination_info = str_replace('registro', 'E-mail', $pagination_info);
		echo '<div>';
		echo '';

		$btnsTpl = '<li><a class="%s" href="%s"><i class="fa %s"></i> &nbsp; %s</a></li>' . PHP_EOL;
		$moveto = '';
		foreach($folders as $f):
			$link = H::link(H::module(), 'index', 'folder:'.$f);
			if(in_array($f,array($folder, 'INBOX.Sent', 'INBOX.Drafts'))) continue;
			$icon = H::arOption($folder_icons, $f, 'fa-chevron-right');
			$label = 'Mover para: '. H::arOption($folder_labels, $f);
			$moveto .= sprintf($btnsTpl, 'move-sel-to', H::link(H::module(), 'move-to', 'folder:' . $folder, 'to:' . $f), $icon, $label);
		endforeach;
		
		
		printf('
		<div class="col-xs-4"><div>&nbsp;</div>
			<ul class="nav nav-pills" style="margin-bottom: 3px;">
				<li class="dropdown">
					<a class="dropdown-toggle btn btn-primary btn-sm" data-toggle="dropdown" href="#">
						<i class="fa fa-cog"></i> &nbsp;Opções &nbsp;<i class="fa fa-chevron-down"></i>
					</a>	
					
				<ul class="dropdown-menu">
					<li><a class="check-all" href="#" onclick="return checkAll(true);"><i class="fa fa-check-square"></i> &nbsp; Selecionar tudo</a></li>
					<li><a class="uncheck-all" href="#" onclick="return checkAll(false);"><i class="fa fa-square"></i> &nbsp; Deselecionar tudo</a></li>
					%s
					<li><a class="move-sel-to" href="%s"><i class="fa fa-history"></i> &nbsp; Marcar como não lida</a></li>
					<li><a class="delete-permanently" href="%s"><i class="fa fa-times"></i> &nbsp; Excluir Definitivamente</a></li>
				</ul>
				</li>
			</ul>
		</div>',
			$moveto,
			H::link(H::module(), 'unseen', 'folder:' . $folder),
			H::link(H::module(), 'delete', 'folder:' . $folder)
		);
		printf('<div class="text-right col-xs-8"><div>%s</div><div>%s</div></div>',
			$pagination_info, 
			$pagination->getPaginationBar()
		);
		
		echo '<div class="clear"></div></div>';
		
		echo '<div class="dev-email-messages-list">' . PHP_EOL;
		foreach($data as $info):
			printf('
			<div class="dev-email-messages-list-item %s noselect">
				<input type="hidden" value="%s" class="mail-uid" />
				<div class="dev-email-messages-list-item-toolbar">
					<div class="text-center"><input type="checkbox" name="uid[]" value="%s" /></div>
					<div class="star">
						<i class="fa %s"></i>
					</div>
				</div>
				<div class="dev-email-messages-list-item-info">
					<div class="message">%s</div>
					<div class="name">%s</div>
					<div class="time">%s</div>
				</div>
			</div>',
				!$info->seen ? 'unseen' : '',
				$info->uid . '/folder:' . $folder,
				$info->uid,
				!$info->flagged ? 'fa-star-o' : 'fa-star',
				$info->subject,
				in_array($folder,array('INBOX.Sent', 'Sent')) ? $info->to : $info->from,
				$info->datetime
			);
		endforeach;
		echo '<div class="clear"></div></div>';
		printf('<div class="text-right"><div>%s</div><div>%s</div></div>', 
			$pagination->getPaginationBar(),
			$pagination_info
		);
	else:
		echo '<div class="dev-email-messages-list">' . PHP_EOL;
		echo H::msgBox('<h3 class="text-center" style="margin-top: 15px;"><i class="fa fa-danger"></i>&nbsp; Nenhum e-mail encontrado.</h3>', false);
		echo '<div class="clear"></div></div>';
	endif;
echo '</div>';
echo '<div class="clear"></div></div>';
?>
