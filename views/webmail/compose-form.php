<?php

$form = new form();
#echo $form->sucessBox($isSaved, 'max-800 center');
echo $form->warningBox($errors,'max-800 center');

echo $form->open('post-form',URL::atual(),'form-post', array('class'=>'default-form max-800 center'));
$tpl = '<div class="%s">%s</div>';

$prefix = 'compose';
$post = isset($_POST[$prefix]) ? $_POST[$prefix] : array();

$fwPre = !$act ? '' : ($act == 'forward' ? 'Fw: ' : 'Re: ');
if($folder == 'INBOX.Sent') $fwPre = '';

$subject = empty($email['from']) ? '' : $fwPre . str_replace('"', '&quot;', $email['subject']);
$key = 'subject'; $attr = array('maxlength'=>255);
if(isset($post[$key])) $subject = $post[$key];
printf($tpl,'col-md-12', $form->text(array($prefix, $key), 'Assunto', $subject, 'not_null', $attr));

$key = 'to'; $attr = array('maxlength'=>255); $to = $act != 'forward' ? $email['from'] : '';
if(isset($post[$key])) $to = $post[$key];
printf($tpl,'col-md-12', $form->text(array($prefix, $key), 'Para', $to, 'not_null', $attr));


$key = 'cc'; $attr = array('maxlength'=>255); $lattr = array('style'=>'display: none;');
$btnRemove = '<div class="col-xs-1"><button class="btn btn-primary btn-block"><i class="fa fa-times"></i></button></div>';
$ccField = $form->text(array($prefix, $key, ''), 'Cc.', '%s', '', $attr, $lattr);
$ccTpl = sprintf($tpl,'cc-item', '<div class="col-xs-11">' . $ccField  . '</div>' . $btnRemove. '<div class="clear"></div>' );
echo '<div>';

$ccList = $act == 'reply-all' ? array_unique(array_merge($email['cc'], $email['to'])) : array();
if(!$ccList) $ccList = array('');
if(isset($post[$key])) $ccList = $post[$key];
foreach($ccList as $ccData):
	$matches = array();
	preg_match('/[\S ]+<([\S]+)>/', $ccData, $matches);
	$ccEmail = end($matches);
	if($user->email != $ccEmail)
		printf($ccTpl, $ccData);

endforeach;
echo '</div>';
printf($tpl,'col-md-12', '<button type="button" class="btn btn-block btn-default btn-sm" id="add-cc"><i class="fa fa-cc"></i> Adicionar CC</button>');

if(count($_FILES) > 0 && !!count($errors)):
	echo H::msgBox('<span class="h4">Selecione novamente os arquivos.</span>', true, H::DANGER);
endif;
printf($tpl,'col-md-12', '<div class="file_manager">
	<input id="file-input" name="attachments[]" type="file" multiple class="col-md-12 file-loading" />
</div>');

$key = 'content'; $attr = array('style'=>'height: 300px;resize: none;');
$val =  '';
if(!empty($email['body'])):
	$val = '<p></p>' . PHP_EOL . '<blockquote style="border-left: 2px solid #7ff;">';
	
	$val .= '<div style="border-top: 2px solid gray;border-bottom: 2px solid gray;padding-left: 20px;">';
	$val .= sprintf('<p>From: %s</p>', htmlentities($email['from']));
	$to = implode(';',$email['to']);
	$val .= sprintf('<p>To: %s</p>', htmlentities($to));
	$val .= sprintf('<p>Subject: %s</p>', htmlentities($email['subject']));
	foreach($email['cc'] as $cc)
		$val .= sprintf('<p>CC: %s</p>', htmlentities($cc));
		
	$val .= '</div>';
	$val .= $email['html'] ? CHTML::html4inline($email['body'], 0) : '<pre>' . $email['body'] . '</pre>';
	
	$val .= '</blockquote>';
endif;

if(isset($post[$key])) $val = $post[$key];
printf($tpl,'col-md-12', $form->textarea(array($prefix, $key), 'E-mail', $val, 'not_null html_textfield', $attr));


printf('<div class="col-md-12">%s<div class="clear"></div></div><br/>', $form->submit('<i class="fa fa-send"></i> &nbsp;Enviar e-mail','btn btn-primary pull-right'));
?>
<script type='text/javascript'>

function organizeCC(count){
	$('.cc-item input').each(function() {
		$(this).attr('id','field-compose-cc-' + count);
		count++;
	});
}
function setRemoveCC(){
	$('.cc-item button').each(function(){
		$(this).unbind( "click" );
		$(this).click(function(){
			if($('.cc-item').length > 1)
				$(this).parent().parent().remove();
			
			return false;
		});
	});
}
$(document).ready(function(){
	setRemoveCC();
	$('#add-cc').click(function(){
		$('.cc-item').first().parent().append(
			$('.cc-item').first().eq(0).clone()
		);
		$('.cc-item input').last().val('');
		var count = 0;
		setRemoveCC();
		organizeCC(count);
		return false;
	});
	$("#file-input").filestyle({buttonText: '&nbsp; Adicionar Anexos', iconName: 'glyphicon glyphicon-paperclip'});
});
</script>