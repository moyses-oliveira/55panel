<?php
$options = '';
array_unshift($folders, 'INBOX');
foreach($folders as $k=>$f):
	if($f == 'INBOX.Drafts') continue;
	if($k > 0 && $f == 'INBOX') continue;
	
	$cls = $folder == $f ? 'active' : '';
	$link = H::link(H::module(), 'index', 'folder:'.$f);
	$active = '';
	if(H::action() == 'index' && $f == $folder)
		$active = 'active';
	$options .= sprintf('<li class="%s"><a class="%s" href="%s"><i class="fa %s"></i> &nbsp;%s</a></li>', 
			$cls, $active, $link , H::arOption($folder_icons, $f, 'fa-chevron-right'), H::arOption($folder_labels, $f));
endforeach;
printf('
	<div class="dev-email-navigation col-md-3 col-sm-4" >
		<div class="dev-email-navigation-compose">
			<a href="%s" class="btn btn-success btn-block"><i class="fa fa-envelope"></i> Nova Mensagem</a>
		</div>
		<ul>
			%s
		</ul>
	</div>',
	H::link(H::module(), 'compose'),
	$options
);