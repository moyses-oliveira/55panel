
<?php

echo '<div class="dev-email">';
include('navigation.php');
echo '<div class="dev-email-messages col-md-9 col-sm-8">';
	

	$att = '';
	foreach($attachments as $k=>$a):
		$atlink = H::link(H::module(), 'attachment', H::cod(), $k);
		$att .= sprintf('<a class="btn btn-primary" target="_blank" href="%s"><i class="fa fa-paperclip"></i> &nbsp; %s</a>&nbsp;', $atlink, $a['name']);
	endforeach;
	if(!empty($att)) 
		$att = sprintf('<div><strong>Anexos</strong>:<br/>%s</div>', $att);

	echo '<div style="max-width: 800px;" class="center portlet">';
	
	$btnsTpl = '<li><a class="btn btn-%s btn-sm" href="%s"><i class="fa fa-%s"></i> &nbsp; %s</a></li>';
	$btnsTpl2 = '<li><a class="%s" href="%s"><i class="fa %s"></i> &nbsp; %s</a></li>';
	$folderVar = 'folder:'.URL::getVar('folder');
	$sent = $folder == 'INBOX.Sent';
	
	echo '<ul class="nav nav-pills pull-right" style="margin-bottom: 3px;">';
	
	if(!$sent):
		printf($btnsTpl, 'tertiary', H::link(H::module(), 'compose', H::cod(), $folderVar,'action:reply'), 'reply', 'Responder');
		printf($btnsTpl, 'tertiary', H::link(H::module(), 'compose', H::cod(), $folderVar,'action:reply-all'), 'reply-all', 'Responder a Todos');
	endif;
	printf($btnsTpl, 'tertiary', H::link(H::module(), 'compose', H::cod(), $folderVar,'action:forward'), 'share', 'Encaminhar');
	
	if(!$sent):
		echo '<li class="dropdown">
			<a class="dropdown-toggle btn btn-secondary btn-sm" data-toggle="dropdown" href="#">
				<i class="fa fa-arrows"></i> &nbsp;Mover para &nbsp;<i class="fa fa-chevron-down"></i>
			</a>';
			echo '<ul class="dropdown-menu">';
			
			foreach($folders as $f):
				$link = H::link(H::module(), 'index', 'folder:'.$f);
				if(in_array($f,array($folder, 'INBOX.Sent', 'INBOX.Drafts'))) continue;
				$icon = H::arOption($folder_icons, $f, 'fa-chevron-right');
				$label = H::arOption($folder_labels, $f);
				printf($btnsTpl2, '', H::link(H::module(), 'move-to', H::cod(), 'folder:' . $folder, 'to:' . $f), $icon, $label);
			endforeach;
			echo '</ul>';
		echo '</li>';
	endif;

	echo '<li class="dropdown">
		<a class="dropdown-toggle btn btn-secondary btn-sm" data-toggle="dropdown" href="#">
			<i class="fa fa-cog"></i> &nbsp;Outras ações &nbsp;<i class="fa fa-chevron-down"></i>
		</a>';
		echo '<ul class="dropdown-menu">';
		printf($btnsTpl2, 'delete-permanently', H::link(H::module(), 'delete', H::cod(), 'folder:' . $folder), 'fa-chevron-right', 'Excluir Definitivamente');
		printf($btnsTpl2, '', H::link(H::module(), 'unseen', H::cod(), 'folder:' . $folder), 'fa-chevron-right', 'Marcar como não lida');
		echo '</ul>';
	echo '</li>';
	
	echo '</ul><div class="clear"></div>';
	$ccText = '';
	if(!!count($cc))
		$ccText = sprintf('<p>CC: %s</p>', htmlentities(implode(';',$cc)));

	$contentBody = $html ? CHTML::html4inline($body, 0) : '<pre>' . $body . '</pre>';
	printf('
			<div style="background-color: #E9E9E9;padding: 10px 20px;border-top-right-radius: 10px;border-top-left-radius: 10px;">
				<div><strong>De</strong>: %s</div>
				<div><strong>Para</strong>: %s</div>
				%s
				<div><strong>Assunto</strong>: %s</div>
			</div>
			<div class="portlet-content">
				%s
			</div>
			<div class="portlet-content">
				%s
			</div>
		',
		htmlentities($from),
		htmlentities(implode(';',$to)),
		$ccText,
		$subject,
		$att,
		$contentBody
	);
	echo '</div>';
echo '</div>';
echo '<div class="clear"></div></div>';
?>
<script type="text/javascript">
$('.delete-permanently').click(function(){
	msgBoxStatic.confirm.redirect('Atenção','<div class="text-center h3">Você tem certeza que deseja excluir este e-mail definitivamente?</div>',$(this).attr('href'));
	return false;
});
</script>