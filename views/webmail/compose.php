<?php
echo '<div class="dev-email">';
include('navigation.php');
echo '<div class="dev-email-messages col-md-9 col-sm-8">';
if($isSuccess):
	echo H::msgBox('<span class="h4">E-mail enviado com sucesso.</span>', false, H::SUCCESS);
else:
	include 'compose-form.php';
endif;
echo '</div>';
echo '<div class="clear"></div></div>';
?>