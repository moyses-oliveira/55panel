<?php
$form = new form();

echo $form->warningBox($errors,'max-800 center');

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));
$tpl = '<div class="%s">%s</div>';

	if($success): 
		echo'
		<div class="alert alert-success alert-dismissable">
		<i class="fa fa-check"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h3 class="text-center">Suas configurações foram salvas com sucesso.</h3>
		</div><div class="clear"></div>';
	endif;


	if(isset($data->pagseguro_email)):
		echo '<div style="font-size: 1.2em;">';
		if($payment_method->enabled * 1 < 1):
			echo '<a href="?chstat=on" class="h2 btn btn-default btn-block"><i class="fa fa-toggle-off"></i> &nbsp; Ativar Pag Seguro</a>';
		else:
			echo '<a href="?chstat=off" class="h2 btn btn-default btn-block"><i class="fa fa-toggle-on"></i> &nbsp; Desativar Pag Seguro</a>';
		endif;
		echo '</div>';
	endif;

	include('form.php');

printf($form->close());
