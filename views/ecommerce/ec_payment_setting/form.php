<?php
foreach($fields as $f):	
	extract($f);
	$value = isset($data->$alias) ? $data->$alias : (isset($default) ? $default : null);
	
	unset($default);
	
	$name = array($table_prefix,$alias);
	$cls .= ' not_null';
	$attr = array('maxlength'=>$length);
	if(isset($f['readonly']))
			$attr['readonly'] = 'readonly';
		
	switch($type) {
		case 'BUTTON';
			$field = sprintf('<button class="btn btn-success button-right %s" url="%s" >%s</button>', $cls, $url, $label);
			break;
		case 'TEXT';
			$field = $form->text($name,$label,$value,$cls,$attr);
			break;
		case 'PASSWORD';
			$field = $form->pass($name,$label,$value,$cls,$attr);
			break;
		case 'TEXTAREA':
			$attr = array('style'=>'height: 165px;resize: none;');
			$field = $form->textarea($name, $label, $value,$cls, $attr);
			break;
		case 'SELECT':
			$field = $form->select($name,$label,$value,$options,$cls . 'first_default');
			break;
		case 'RADIO':
			$brk_options = explode(',',$opcoes);
			$options = '';
			foreach($brk_options as $o) :
				$o = trim($o);
				$checked = $o == $value ? ' checked="checked" ' : '';
				$options .= sprintf('<label class="radio_label"><input type="radio" name="%s[%s]" value="%s" %s/><span>%s</span></label>',
								$table_prefix, $alias, $o, $checked, $o);
			endforeach;
			$field = sprintf(
				'<label style="display: block;" class="required">%s</label>
				<div style="border: 1px solid #cccccc;border-radius: 4px;padding: 5px 20px;">%s<div class="clear"></div></div>',
				$label,
				$options
			);
				
			#$field = $form->select($name,$f->label,$f->value,$options,'not_null');
			break;
	}
	printf($tpl,'col-md-12', $field);
endforeach;
echo '<div class="clear"></div>';

printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));
