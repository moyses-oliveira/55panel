<?php
echo '<br/>';
$form = new form();

echo $form->warningBox($errors,'max-800 center');

printf($form->open($table_prefix, '#','form-'.$table_prefix, array('class'=>'default-form max-800 center')));
$tpl = '<div class="%s">%s</div>';
	
if($success): 
echo'
	<div class="alert alert-success alert-dismissable">
	<i class="fa fa-check"></i>
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h3 class="text-center">Suas configurações foram salvas com sucesso.</h3>
	</div><div class="clear"></div>';
endif;
foreach($fields as $f):
	extract($f);
	$value = isset($data->$alias) ? $data->$alias : (isset($default) ? $default : null);
	
	unset($default);
	
	$name = array($table_prefix,$alias);
	$cls .= ' not_null';
	$attr = array('maxlength'=>$length);
	if(isset($f['readonly']))
			$attr['readonly'] = 'readonly';

	$field = $form->text($name,$label,$value,$cls,$attr);
	printf($tpl,'col-md-12', $field);
endforeach;
echo '<div class="clear"></div>';
echo '<fieldset><legend>Formas de Pagamento Ativas</legend><br/>';
foreach($payment_methods as $s):
	$enabled = !$s->enabled ? '' : 'checked="checked"';
	$conf = sprintf('<a href="%s" class="text-primary pull-right h4"><i class="fa fa-cog"></i></a>', H::link(H::module(), $s->alias));
	
	if($s->alias == 'pay_on_delivery'):
		$s->label .= '<br/><small>(Esse item é exibido automáticamente quando a forma de entrega selecionada é "buscar no local".)</small>';
		$conf = null;
	endif;
	
	printf('<div class="col-xs-12" style="border-bottom: 1px dotted #ccc;margin-bottom: 20px;"><label class="h4"><input name="payment[]"  onclick="return false" type="checkbox" value="%s" %s /> &nbsp;%s</label>%s</div>', $s->payment_int_id, $enabled, $s->label, $conf); 
endforeach;
echo '<div class="clear"></div>';
echo '</fieldset>';

echo '<div class="clear"></div>';
#printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));
printf($form->close());