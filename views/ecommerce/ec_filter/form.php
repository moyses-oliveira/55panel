<?php
$form = new form();

echo $form->warningBox($errors,'max-800 center');

$label_prefix = 'label';
$val_prefix = 'value';
echo $form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center'));
$tpl = '<div class="%s">%s</div>';

echo '<fieldset>';
echo '<legend>Label</legend>';
foreach($label_list as $k=>$label_val):
	echo '<div>';
	$key = 'filter_label_chr_language';
	echo $form->hidden(array($label_prefix,$k, $key), $label_val->{$key});
	$key = 'filter_label_vrc_label';$inf = $info_label->{$key};
	$img = sprintf('%sfiles/img/flags/%s',H::root(), $flags[$label_val->filter_label_chr_language]);
	$style = sprintf('background-image: url(%s);',$img);
	$inf->type .= ' field_label';
	$attr = array('maxlength'=>$inf->lenght, 'placeholder'=>'Label', 'style'=> $style);
	$attr_label = array('style'=>'display: none;');
	printf($tpl,'col-md-6', $form->text(array($label_prefix,$k, $key), '', $label_val->{$key}, $inf->type, $attr, $attr_label));

	echo '</div>';
endforeach;
	$key = 'filter_tny_single';
	$options= array(array('0','Multiplo'), array('1', 'Único'));$attr_label = array('style'=>'display: none;');
	printf($tpl, 'col-md-6', $form->select(array('filter', 'filter_tny_single'), $info->{$key}->label, $data->{$key}, $options, $info->{$key}->type, array(), $attr_label));
echo '</fieldset>';


$tpl_input = sprintf('
	<div class="fields">
		<label>Opção</label>
		<input type="hidden" name="value[{lang}_{k}][filter_value_chr_filter_item]" value="{item_id}" />
		<input type="hidden" name="value[{lang}_{k}][filter_value_chr_language]" value="{lang}" />
		<input type="text" name="value[{lang}_{k}][filter_value_vrc_value]" value="{value}" class="varchar not_null field_value" maxlength="100" placeholder="{language}" style="background-image: url(%sfiles/img/flags/{flag})">
	</div>
',H::root());
$langs = array();
$langs[] = (object)array('lang'=>'pt_br','flag'=>'br.png','language'=>'Português (BR)');
$action_row = '<div class="action"><span>&nbsp;</span>Opção <a href="#" class="remove"><i class="fa fa-trash"></i></a></div>';
$tpl_item = '<input type="hidden" name="item[]" value="{item_id}" />';
$tpl_add_option_js = PHP_EOL . $action_row . PHP_EOL . str_replace('{item_id}', '{k}', $tpl_item);
foreach($langs as $l):
	$find = array('{lang}','{flag}', '{language}','{value}', '{item_id}');
	$replace = array($l->lang, $l->flag, $l->language, '','{k}');
	$tpl_add_option_js .= str_replace($find, $replace, $tpl_input);
endforeach;
$tpl_add_option_js = sprintf('<div class="option col-md-6">%s</div>', $tpl_add_option_js);
$tpl_add_option_js = str_replace(array('"',"\n","\r","\r\n"), array('\\"',"\\n","\\r","\\r\\n"), $tpl_add_option_js);

echo '<fieldset style="padding: 5px 20px 20px 20px;">';
echo '<legend>Opções</legend>';
$cur_item_id = null;
$indent = PHP_EOL . "\t\t";

foreach($item_list as $k=>$item):
	if($cur_item_id != $item->item_id):
		if(!!$cur_item_id) echo $indent . '<br/></div>';
		$cur_item_id = $item->item_id;
	endif;
	echo $indent . '<div class="option col-md-6">' . $indent . $action_row;
	echo $indent . str_replace('{item_id}', $item->item_id, $tpl_item);
	$find = array('{k}','{item_id}','{value}','{lang}','{flag}','{language}');
	$replace = array($k, $item->item_id, $item->value, $item->lang, $item->flag, $item->language);
	echo str_replace($find, $replace, $tpl_input);
endforeach;
echo $indent . '<br/></div>';

echo '<a href="#" class="add-option col-md-6"><i class="fa fa-plus"></i> Nova Opção</a>';

echo PHP_EOL . '<div class="clear"></div></fieldset>' . PHP_EOL;

printf('<div class="col-md-12">%s<div class="clear"></div></div><br/>', $form->submit('Salvar','btn btn-primary button-right'));
echo PHP_EOL . $form->close();
printf(PHP_EOL . '<script type="text/javascript"> var filter_item_tpl = \'%s\'; </script>', $tpl_add_option_js);
?>
<style type="text/css">
div.option { border: 1px dotted gray; margin: -1px 0 0 -1px;padding: 0; }
a.add-option { border: 1px dotted gray; margin: -1px 0 0 -1px;padding: 15px; display: block;}
div.option input.field_value, input.field_label { background: url() 0 -7px no-repeat;background-size: 45px auto;padding-left: 50px !important; margin-top: 10px;}
div.option .fields {margin: 5px 15px;}
a.add-option { font-size: 1.5em; }
.default-form div.action { font-size: 1.5em; margin: 5px 20px 0 20px; border-bottom: 1px solid #ddd; color: #555; }
.action a.remove { color: rgba(245, 105, 84, 0.5); display: block; float: right; }
.action a.remove:hover{ color: rgba(245, 105, 84, 1); }
div.option label { display: none;}
</style>
<script type="text/javascript">
function refreshOptionActions(){
	$('#form-ec_filter div.option a.remove').each(function(){
		$(this).unbind('click');
		$(this).click(function(){
			var boxOption = $(this).closest('div.option');
			boxOption.animate({ opacity: 0.0, height: 0 }, 1000, function() { 
				boxOption.remove();
			});
			return false;
		});
	});
}

$(document).ready(function(){
	var optionHeight = $('#form-ec_filter div.option').first().outerHeight(true);
	$('#form-ec_filter div.option').css('height', Math.floor(optionHeight));
	$('#form-ec_filter div.option').first().find('div.action .remove').remove();
	
	$('#form-ec_filter a.add-option').css('height', Math.floor(optionHeight));
	$('#form-ec_filter a.add-option').css('padding-top', Math.floor(optionHeight*0.40));
	$('#form-ec_filter a.add-option').css('text-align','center');
	refreshOptionActions();
	
	$('#form-ec_filter a.add-option').click(function(){
		var count_options = $('#form-ec_filter div.option').length;
		var currentTime = (new Date()).getTime();
		var uniqid = (currentTime + count_options).toString(36) + (currentTime*10).toString(36);
		
		var tpl = ('' + filter_item_tpl).replace(/\{k\}/g, uniqid);
		$(tpl).insertBefore($(this));
		$('#form-ec_filter div.option').last().css('height', Math.floor(optionHeight));
		refreshOptionActions();
		return false;
	});
});
</script>