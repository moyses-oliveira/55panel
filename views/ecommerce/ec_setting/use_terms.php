<?php
$form = new form();

echo $form->warningBox($errors,'max-800 center');

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));
$tpl = '<div class="%s">%s</div>';
	
if($saved): 
	echo'
	<div class="alert alert-success alert-dismissable">
	<i class="fa fa-check"></i>
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h3 class="text-center">Suas configurações foram salvas com sucesso.</h3>
	</div><div class="clear"></div>';
endif;


$name = array($table_prefix,'use_terms_txt_terms');
$attr = array('style'=>'height: 300px;resize: none;');
printf($tpl,'col-md-12', $form->textarea($name, 'Termos de uso do site', $terms, ' not_null html_textfield', $attr));
echo '<div class="clear"></div>';
printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));
printf($form->close());

?>
<script type="text/javascript">
$(document).ready(function(){

	tinymce.init({
		selector: "textarea.html_textfield",
		language: 'pt_BR',
		theme: 'modern',
		menu: {
			edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
			insert: {title: 'Insert', items: 'template hr'},
			view: {title: 'View', items: 'visualaid preview'},
			format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
			table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
		},
		plugins: [
			"advlist autolink lists link image charmap preview anchor textcolor",
			"searchreplace visualblocks code fullscreen",
			"media table contextmenu paste"
		],
		toolbar: [
			" styleselect | fontselect | fontsizeselect | print preview | image charmap | table | forecolor ",
			"insertfile | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | searchreplace | code "
		],
		setup: function (editor) {
			editor.on('keyup', function () {
				$(editor).find("textarea").first().html(editor.getContent());
			});
			editor.on('change', function () {
				$(editor).find("textarea").first().html(editor.getContent());
			});
			editor.on('keydown', function (e) {
				if (e.keyCode == 9) {
					editor.execCommand(e.shiftKey ? 'Outdent' : 'Indent');
					e.preventDefault();
					return false;
				}
			});
		}
	});
});
</script>