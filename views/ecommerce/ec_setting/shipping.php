<?php
$form = new form();

echo $form->warningBox($errors,'max-800 center');

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));
$tpl = '<div class="%s">%s</div>';
	
if($success): 
echo'
	<div class="alert alert-success alert-dismissable">
	<i class="fa fa-check"></i>
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h3 class="text-center">Suas configurações foram salvas com sucesso.</h3>
	</div><div class="clear"></div>';
endif;
foreach($fields as $f):	
	extract($f);
	$value = isset($data->$alias) ? $data->$alias : (isset($default) ? $default : null);
	
	unset($default);
	
	$name = array($table_prefix,$alias);
	$cls .= ' not_null';
	$attr = array('maxlength'=>$length);
	if(isset($f['readonly']))
			$attr['readonly'] = 'readonly';

	$field = $form->text($name,$label,$value,$cls,$attr);
	printf($tpl,'col-md-12', $field);
endforeach;
echo '<div class="clear"></div>';
echo '<fieldset><legend>Formas de Entrega Ativas</legend><br/>';
foreach($shipping_methods as $s):
	$enabled = !$s->enabled ? '' : 'checked="checked"';
	printf('<div class="col-xs-12"><label class="h4"><input name="shipping[]" type="checkbox" value="%s" %s /> &nbsp;%s</label></div>', $s->shipping_int_id, $enabled, $s->label); 
endforeach;
echo '<div class="clear"></div>';
echo '</fieldset>';

echo '<div class="clear"></div>';
printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));
printf($form->close());