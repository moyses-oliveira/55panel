<?php
$form = new form();

echo $form->warningBox($errors,'max-800 center');

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));
$tpl = '<div class="%s">%s</div>';
	
if($success): 
echo'
	<div class="alert alert-success alert-dismissable">
	<i class="fa fa-check"></i>
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h3 class="text-center">Suas configurações foram salvas com sucesso.</h3>
	</div><div class="clear"></div>';
endif;
foreach($fields as $f):	
	extract($f);
	$value = isset($data->$alias) ? $data->$alias : (isset($default) ? $default : null);
	
	unset($default);
	
	$name = array($table_prefix,$alias);
	$cls .= ' not_null';
	$attr = array('maxlength'=>$length);
	if(isset($f['readonly']))
			$attr['readonly'] = 'readonly';
		
	switch($type) {
		case 'BUTTON';
			$field = sprintf('<button class="btn btn-success button-right %s" url="%s" >%s</button>', $cls, $url, $label);
			break;
		case 'TEXT';
			$field = $form->text($name,$label,$value,$cls,$attr);
			break;
		case 'PASSWORD';
			$field = $form->pass($name,$label,$value,$cls,$attr);
			break;
		case 'TEXTAREA':
			$attr = array('style'=>'height: 165px;resize: none;');
			$field = $form->textarea($name, $label, $value,$cls, $attr);
			break;
		case 'SELECT':
			$field = $form->select($name,$label,$value,$options,$cls . 'first_default');
			break;
		case 'RADIO':
			$brk_options = explode(',',$opcoes);
			$options = '';
			foreach($brk_options as $o) :
				$o = trim($o);
				$checked = $o == $value ? ' checked="checked" ' : '';
				$options .= sprintf('<label class="radio_label"><input type="radio" name="%s[%s]" value="%s" %s/><span>%s</span></label>',
								$table_prefix, $alias, $o, $checked, $o);
			endforeach;
			$field = sprintf(
				'<label style="display: block;" class="required">%s</label>
				<div style="border: 1px solid #cccccc;border-radius: 4px;padding: 5px 20px;">%s<div class="clear"></div></div>',
				$label,
				$options
			);
				
			#$field = $form->select($name,$f->label,$f->value,$options,'not_null');
			break;
	}
	printf($tpl,'col-md-12', $field);
endforeach;
echo '<div class="clear"></div>';
printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));
printf($form->close());
?>
<!--script>
$('button.verify').each(function(){
	$(this).click(function(){
		var btnVerify = $(this);
		var btnText = $(this).html();
		$(this).html(btnText);
		$(this).html("<span></span>").attr("disabled", "disabled");
		$(this).find("span").addClass("fa fa-spinner rotating");
		
		var post_data = { setting: {} };
		$(this).closest('fieldset').find('input[type=text], input[type=password], select').each(function(){
			var fieldName = $(this).attr('name');
			post_data.setting[fieldName.substring(8,fieldName.length - 1)] = $(this).val();
		});
		
		var tmp_options = {type: 'POST', url: $(this).attr('url'), dataType: 'json', data: post_data };
		tmp_options.success = function (json) {
			alert(json.res);
			btnVerify.html(btnText).removeAttr("disabled");;
		};
		$.ajax(tmp_options);
		return false;
	});
});

</script -->