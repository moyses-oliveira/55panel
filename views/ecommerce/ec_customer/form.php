<?php
if($update):
	printf('<div class="max-800 center" style="padding-top: 20px;" >%1$s%2$s%1$s</div>',
		PHP_EOL, H::msgBox('Os dados foram salvos com sucesso!', true, H::SUCCESS)
	);
	echo '<div class="clear"></div>';
endif;


$form = new form();

$msg = ''; 
 if(count($errors) > 0): 
	foreach($errors as $k=>$v) $msg .= sprintf('<li>%s</li>',$v);
	$msg = '<ul>' . $msg . '</ul>';
endif;
printf('<div class="warnings">%s</div>',$form->warningBox($msg, 'max-800 center','Atenção', count($errors) < 1));

echo $form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center'));

$tpl = '<div class="%s">%s</div>';
$key = 'customer_vrc_name'; $inf = $info->{$key}; $attr = array('maxlength'=>$inf->lenght);
printf($tpl,'col-md-12', $form->text(array($table_prefix,$key), $inf->label, $data->{$key},'not_null', $attr));

$key = 'customer_vrc_email'; $inf = $info->{$key}; $attr = array('maxlength'=>$inf->lenght);
printf($tpl,'col-md-12', $form->text(array($table_prefix,$key), $inf->label, $data->{$key},'not_null email', $attr));

printf('<div class="col-md-12">%s<div class="clear"></div></div><br/>', $form->submit('Salvar','btn btn-primary button-right'));

echo PHP_EOL . $form->close();



$table_tpl = '
<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
	<tbody role="alert" aria-live="polite" aria-relevant="all">
		%s
	</tbody>
</table>';
echo '<div class="max-800 center view">';
$rows = '';

/*
$params = array_keys((array)$data);
foreach($params as $k):
	if(empty($info->{$k}->label)) continue;
	$rows .= sprintf( "<tr class='grid'><td>%s</td><td>%s</td></tr>", $info->{$k}->label, $data->{$k});
endforeach;
printf($table_tpl, $rows);
*/

$labels = array(
	'address_vrc_postcode'=>'CEP',
	'address_vrc_country'=>'País',
	'address_vrc_zone_name'=>'Estado',
	'address_vrc_city'=>'Cidade',
	'address_vrc_neighborhood'=>'Bairro',
	'address_vrc_number'=>'Número',
	'address_vrc_complement'=>'Complemento'
);
foreach($addresses as $k=>$addr):
	$rows = sprintf('<tr class="grid"><th colspan="2">Endereço %s</th></tr>', $k + 1);
	
	foreach($labels as $k=>$l):
		if(empty($addr->{$k})) continue;
		$rows .= sprintf( "<tr class='grid'><td style='width: 30%%'>%s</td><td>%s</td></tr>", $l, $addr->{$k});
	endforeach;
	printf($table_tpl, $rows);
endforeach;
echo '</div>';