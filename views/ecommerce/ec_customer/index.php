<div class="box-body table-responsive" style='padding: 10px;'>
<?php 
$grid = new gridView();
foreach(array('customer_vrc_name', 'customer_vrc_email') as $k)
  $grid->addColumn(new gridViewColumn($k,$info->{$k}->label));
  
$grid->addActionColumn(H::module(),'customer_int_id', '80px',array(
	array('action' => 'update', 'class' => 'fa-pencil', 'title' => 'Editar'),
	#array('action' => 'view', 'class' => 'fa-search-plus', 'title' => 'Visualizar'),
	array('action' => 'delete', 'class' => 'fa-close', 'title' => 'Remover')
	),
	false
);
$form = new form();
echo $form->open('product-form',H::link(H::module(), 'index'),'form-product', array('class'=>'default-form', 'style'=>'margin: 10px 20px;', 'method'=>'get'));
$find = $form->text(array('find'), 'Digite sua busca', @$_REQUEST['find'], '', array('maxlength'=>255), array('style'=>'display: none;'));
printf('<div class="col-xs-8">%s</div>', $find);
printf('<div class="col-xs-4">%s</div>',$form->submit('Procurar','btn btn-primary'));
echo $form->close();

if(!!$data):
	echo $grid->renderTable($data);
	$pag = new Pagination(false, array(), current($data)->total_records, 10);
	echo '<div class="row">';
	printf('<div class="col-md-6">
		<ul class="pagination">
			%s
			%s
			%s
			%s
		</ul>
	</div>',
		$pag->linkPrev(), 
		$pag->getCurrentPages(), 
		$pag->getNavigationGroupLinks(), 
		$pag->linkNext()
	);
	$start = 1 + (($pag->currentPage - 1) * $pag->recordByPage);
	$end = ($start + $pag->recordByPage - 1);
	$to = $pag->totalRecords > $end ? $end : $pag->totalRecords;
	printf('<div class="col-md-6"> %s até %s de %s registros.</div>',
	$start, $to, $pag->totalRecords);
	echo '</div>';
else:
	echo '<br/><br/>'.H::msgBox('Nenhum item encontrado',true, H::WARNING);
endif;

?>
</div>