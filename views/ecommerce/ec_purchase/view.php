﻿<?php

printf('<h3 class="max-800 center">Venda Nº %s <span class="pull-right">%s<span></h3>', 
	str_pad(URL::friend(2), 10, 0, STR_PAD_LEFT),
	CData::format('d/m/Y H:i', $purchase->purchase_dtt_open)
	);
printf('<h3 class="max-800 center text-center">Cliente: %s</h3>', $purchase->customer_vrc_name);
printf('<h3 class="max-800 center text-center">Situação: %s</h3>', $purchase->purchase_vrc_status);
echo '<div class="default-form max-800 center">';
echo '<table class="table table-bordered table-shopping-cart">';
echo '
<tr>
	<th>Produto</th>
	<th style="width: 20%">Preço Unit.</th>
	<th style="width: 20%">Qtd.</th>
	<th style="width: 20%">Subtotal</th>
</tr>
';
if($purchase->purchase_status_vrc_alias == 'start'):
	$link = H::link(H::module(), 'set-status', H::cod(), 'canceled') . '?hash=' . urlencode($purchase->purchase_vrc_hash);
	$msg = sprintf('
		<h4>O cliente não selecionou nenhuma forma de pagamento ainda.</h4>
		<a href="%s" class="h4">Clique aqui para cancelar o pagamento.</a>
	', $link );
	echo H::msgBox($msg, false, H::WARNING);

elseif($purchase->purchase_status_vrc_alias == 'payment_denied'):
	$link = H::link(H::module(), 'set-status', H::cod(), 'canceled') . '?hash=' . urlencode($purchase->purchase_vrc_hash);
	$msg = sprintf('
		<h4>O pagamento foi negado pelo gateway.</h4>
		<a href="%s" class="h4">Clique aqui para cancelar o pagamento.</a>
	', $link );
	echo H::msgBox($msg, false, H::DANGER);
		
elseif($purchase->purchase_status_vrc_alias == 'payment_check'):
	$link = H::link(H::module(), 'set-status', H::cod(), 'payment_accepted') . '?hash=' . urlencode($purchase->purchase_vrc_hash);
	$msg = sprintf('
		<h4>Aguardando confirmação de pagamento pelo gateway.</h4>
		<a href="%s" class="h4">Clique aqui para confirmar manualmente o pagamento.</a>
	', $link );
	echo H::msgBox($msg, false, H::WARNING);
elseif($purchase->purchase_status_vrc_alias == 'payment_accepted'):
	$link = H::link(H::module(), 'set-status', H::cod(), 'shipped') . '?hash=' . urlencode($purchase->purchase_vrc_hash);
	$msg = sprintf('
		<h4>O pagamento foi aceito.</h4>
		<a href="%s" class="h4">Se você já enviou a mercadoria clique aqui.</a>
	', $link );
	echo H::msgBox($msg, false, H::SUCCESS);
elseif($purchase->purchase_status_vrc_alias == 'shipped'):
	echo H::msgBox('Aguardando confirmação de recebimento pelo cliente.', false);
elseif($purchase->purchase_status_vrc_alias == 'received'):
	$link = H::link(H::module(), 'set-status', H::cod(), 'completed') . '?hash=' . urlencode($purchase->purchase_vrc_hash);
	$msg = sprintf('
		<h4>A encomenda foi recebida pelo cliente.</h4>
		<a href="%s" class="h4">Clique para concluir.</a>
	', $link );
	echo H::msgBox($msg, false, H::SUCCESS);
endif;

echo '<div class="clear"></div><br/>';

$action_column = '<td class="text-center"><a href="%s" class="remove-from-cart"><i class="fa fa-close"></i></a></td>';
$item_tpl = '
		<tr>
			<td class="product-name">%s</td>
			<td>%s %s</td>
			<td>%s</td>
			<td>%s %s</td></tr>
	';
foreach($items as $i):
	printf($item_tpl, 
		$i->name . (!$i->code ? '' : ' ['. $i->code .']'),
		$purchase->purchase_vrc_currency_simble,
		number_format($i->price,2,',','.'),
		number_format($i->quantity,0,',','.'),
		$purchase->purchase_vrc_currency_simble,
		number_format($i->subtotal,2,',','.')
	);
endforeach;

	printf('
		<tr class="info">
			<td colspan="3">Sub-total</td><td>%s %s</td>
		</tr>',
		$purchase->purchase_vrc_currency_simble,
		number_format($purchase->purchase_dcm_subtotal,2,',','.')
	);

	printf('
		<tr class="info">
			<td colspan="3">Modo de Envio (%s)</td><td>%s %s</td>
		</tr>',
		$purchase->purchase_vrc_shipping_method,
		$purchase->purchase_vrc_currency_simble,
		number_format($purchase->purchase_dcm_shipping_price,2,',','.')
	);

	printf('
		<tr class="warning">
			<td colspan="3">Total</td><td>%s %s</td>
		</tr>',
		$purchase->purchase_vrc_currency_simble,
		number_format($purchase->purchase_dcm_total,2,',','.')
	);
echo '</table>';
$a = $address;
if(!empty($a->address_vrc_complement))
		$a->address_vrc_complement = ' / '.$a->address_vrc_complement;
printf('	
	<fieldset class="text-center">
		<legend>Endereço de Entrega</legend>
		<p><strong>%s - %s - %s</strong></p>
		<p><strong>CEP</strong> %s</p>
		<p><strong>Bairro </strong> %s</p>
		<p><strong>Endereço </strong>%s <strong>Nº </strong>%s %s </p>
	</fieldset>
	',
$a->address_vrc_city, $a->address_vrc_zone, $a->address_vrc_country, $a->address_vrc_postcode, 
$a->address_vrc_neighborhood, $a->address_vrc_address, $a->address_vrc_number, $a->address_vrc_complement
);



if(!isset($errors)) $errors = array();
printf('
	<div class="warnings field" style="background-color: #fff; border: 1px solid #fff;margin: 0 10px;padding: 0;">
		<div class="box box-solid box-danger center" %s>
			<div class="box-header">
				<h3 class="box-title">Atenção</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-danger btn-sm" data-widget="collapse" onclick="return false;"><i class="fa fa-minus"></i></button>
					<button class="btn btn-danger btn-sm" data-widget="remove" onclick="return false;"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">
				%s
			</div>
		</div>
	</div>',
	(count($errors) ? '' : ' style="display: none;"'),
	(count($errors) ? sprintf('<ul><li>%s</li><ul>', implode('</li><li>', $errors)) : '')
);
printf('<form action="%s" enctype="multipart/form-data" method="post" class="default-form" id="form-comment">',
	URL::link(H::module(), H::action(), URL::friend(2))
);
echo '<fieldset><legend>Painel de chamado</legend>';

echo '
	<div class="field" style="margin: 10px 30px;">';

if(!in_array($purchase->purchase_status_vrc_alias, array('completed', 'canceled'))):
	echo '
		<label for="field-comment">Digite um comentário</label>
		<textarea maxlength="1200" placeholder="Digite aqui" class="not_null" id="field-comment" name="comment" style="height: 110px;"></textarea>
		<input type="file" name="purchase_comment_vrc_file" accept=".jpg,.jpeg,.gif,.bmp,.txt,.pdf" />
		<button type="submit" class="btn btn-primary btn-block">
			<span><i class="fa fa-comment"></i> &nbsp;Clique aqui para Comentar</span>
		</button>';
endif;

$template_comment = '<div class="field" style="margin: 5px 0;background-color: #f0f6f0;padding: 5px 20px;">
	<h4 class="text-%s" >%s <span class="pull-right">%s</span></h4>
	<p>%s</p>
	%s
</div>';

if(!$comments) echo '<h4 class="text-center">Nenhum comentário</h4>';
foreach($comments as $comment):
	$ext = empty($comment->file) ? '' : pathinfo($comment->file, PATHINFO_EXTENSION);
	if(empty($comment->file)):
		$content_file = '';
	elseif(in_array(strtolower($ext), array('jpg', 'jpeg', 'png', 'bmp', 'gif'))):
		$content_file = sprintf('<a href="%1$s" target="_blank"><img src="%1$s" style="max-width: 300px;"/></a>', $path . '/' . $comment->file);
	else:
		$content_file = sprintf('<h4><a href="%s" target="_blank"><i class="fa fa-file-text"></i> Anexo %s</a></h4>', $path . '/' . $comment->file, strtoupper($ext));
	endif;
	
	
	printf($template_comment, 
		!$comment->me ? 'success' : 'danger',
		(!$comment->me ? $users[$comment->sender] : $purchase->customer_vrc_name) .' disse:' ,
		CData::format('d/m/Y H:i', $comment->dtt_added),
		str_replace("\n",'</p><p>', $comment->comment),
		$content_file
	);
endforeach;


echo '</div>';
echo '</form>';
echo '</fieldset>';
echo '</div>';
