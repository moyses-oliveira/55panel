<div class="box-body table-responsive" style='padding: 10px;'>
<?php 
$grid = new gridView();
$fields = array(
	'customer' => 'Nome do Cliente',
	'status' =>  'Situação da Venda',
	'total' => 'Total'
);


#$grid->addColumn(new gridViewColumn('tny_question', '&nbsp;', array('width'=>'40px')));
foreach($fields as $k=>$label)
  $grid->addColumn(new gridViewColumn($k, $label));
  
$grid->addActionColumn(H::module(),'purchase_int_id', '90px',array(
	array('action' => 'view', 'class' => 'fa-search-plus', 'title' => 'Visualizar')
	),
	false
);

foreach($data as $k=>$i)
	$data[$k]->customer = !$i->tny_question ? $i->customer : '<i class="fa fa-comment-o text-success" style="font-size: 1.4em;"></i>  &nbsp; ' . $i->customer;

	
$form = new form();
echo $form->open('purchase-form',H::link(H::module(), 'index'),'form-purchase', array('class'=>'default-form', 'style'=>'margin: 10px 20px;', 'method'=>'get'));
$find = $form->text(array('find'), 'Digite sua busca', @$_REQUEST['find'], '', array('maxlength'=>255), array('style'=>'display: none;'));
printf('<div class="col-xs-4">%s</div>', $find);
$stat = $form->select(array('status'), 'Digite sua busca', @$_REQUEST['status'], $status,'', array(), array('style'=>'display: none;'));
printf('<div class="col-xs-4">%s</div>', $stat);
printf('<div class="col-xs-4">%s</div>',$form->submit('Procurar','btn btn-primary'));
echo $form->close();
if(!!$data):

	echo $grid->renderTable($data);
	$current_page = URL::getVar('page');
	$limit = 10;
	if(!$current_page) $current_page = 1;
	$start = $limit * ($current_page - 1);
	$totalRecords = 0;
	$totalRecords = current($data)->total_records;
	$pagination = new Pagination($current_page,$limit,$totalRecords);
	printf(
		'<div class="row">
			<div class="col-md-6">%s</div>
			<div class="col-md-6">%s</div>
		</div>
		',
		$pagination->getPaginationInfo(), 
		$pagination->getPaginationBar()
	);
else:
	echo '<br/><br/>'.H::msgBox('Nenhum item encontrado',true, H::WARNING);
endif;
?>
</div>