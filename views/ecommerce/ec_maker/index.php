<div class="box-body table-responsive" style="padding: 10px;">
<?php 

if(count($data)):
	$grid = new gridView();
	foreach(array('product_maker_vrc_name') as $col)
		$grid->addColumn(new gridViewColumn($col, $info->{$col}->label));
	
	$grid->addActionColumn(H::module(), 'product_maker_int_id', '80px');
	echo $grid->renderTable($data);

	$current_page = URL::getVar('page');
	$limit = 10;
	if(!$current_page) $current_page = 1;
	$start = $limit * ($current_page - 1);
	$totalRecords = 0;
	$totalRecords = current($data)->total_records;
	$pagination = new Pagination($current_page,$limit,$totalRecords);
	printf(
		'<div class="row">
			<div class="col-md-6">%s</div>
			<div class="col-md-6">%s</div>
		</div>
		',
		$pagination->getPaginationInfo(), 
		$pagination->getPaginationBar()
	);
else:
	echo '<br/><br/>'.H::msgBox('Nenhum item encontrado',true, H::WARNING);
endif;
?>
</div>