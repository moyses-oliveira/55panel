<?php
$table_tpl = '
<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
	<tbody role="alert" aria-live="polite" aria-relevant="all">
		%s
	</tbody>
</table>';
echo '<div class="max-800 center view">';
$rows = '';
$params = array_keys((array)$data);
foreach($params as $k):
	if(empty($info->{$k}->label)) continue;
	$rows .= sprintf( "<tr class='grid'><td>%s</td><td>%s</td></tr>", $info->{$k}->label, $data->{$k});
endforeach;
printf($table_tpl, $rows);

$params = array_keys((array)$desc_info);
foreach($desc_data_list as $desc_data):
	$rows = '';
	foreach($params as $k):
		if(empty($desc_info->{$k}->label)) continue;
		$rows .= sprintf( "<tr class='grid'><td>%s</td><td>%s</td></tr>", $desc_info->{$k}->label, $desc_data->{$k});
	endforeach;
	echo '<div class="default-form"><fieldset><legend>Português</legend><div style="margin: 10px 20px;">';
	printf($table_tpl, $rows);
	echo '</div></fieldset></div>';
endforeach;

echo '</div>';