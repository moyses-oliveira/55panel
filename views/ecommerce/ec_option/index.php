<div class="box-body table-responsive" style='padding: 10px;'>
<?php 
$grid = new gridView();
foreach(array('option_label_vrc_label') as $k)
  $grid->addColumn(new gridViewColumn($k,$info->{$k}->label));
  
$grid->addActionColumn(H::module(),'option_int_id', '90px', array(
	array('action' => 'update', 'class' => 'fa-pencil', 'title' => 'Editar'),
	array('action' => 'delete', 'class' => 'fa-close', 'title' => 'Remover')
	),
	false
);

if(!!$data):
	$data = (array)$data;
	echo $grid->renderTable($data);

	
	$current_page = URL::getVar('page');
	$limit = 10;
	if(!$current_page) $current_page = 1;
	$start = $limit * ($current_page - 1);
	$totalRecords = 0;
	$pagination = new Pagination($current_page,$limit,$totalRecords);
	$totalRecords = current($data)->total_records;
	printf(
		'<div class="row">
			<div class="col-md-6">%s</div>
			<div class="col-md-6">%s</div>
		</div>
		',
		$pagination->getPaginationInfo(), 
		$pagination->getPaginationBar()
	);
else:
	echo '<br/><br/>'.H::msgBox('Nenhum item encontrado',true, H::WARNING);
endif;

?>
</div>