<?php
$form = new form();

echo $form->sucessBox(@$isSaved, 'max-800 center');
echo $form->warningBox($errors,'max-800 center');

$category_prefix = 'category';
$desc_prefix = 'desc';
echo $form->open($category_prefix,URL::atual(),'form-'.$category_prefix, array('class'=>'default-form max-800 center'));
$tpl = '<div class="%s">%s</div>';

#$key = 'category_int_parent';$inf = $info->{$key};$attr = array('maxlength'=>$inf->lenght);
#printf($tpl,'col-md-12', $form->select(array('parent'),$inf->label, $data->{$key}, $category_parent_options, $inf->type, $attr));

#echo '<fieldset>';
#echo '<legend>Descrição da Categoria';
#printf('<a href="#desc-br"><img src="%sarquivos/flags/%s" /></a>',H::root(),'br.png');
#echo '</legend>';
foreach($desc_data_list as $k=>$desc_data):
	echo '<div id="desc-br">';
	echo $form->hidden(array($desc_prefix,$k,'category_description_chr_language'),'pt_br');
	
	$key = 'category_description_vrc_name';$inf = $info_desc->{$key};$attr = array('maxlength'=>$inf->lenght);
	printf($tpl,'col-md-12', $form->text(array($desc_prefix,$k, $key),$inf->label, $desc_data->{$key},$inf->type, $attr));

	$key = 'category_description_txt_description';$inf = $info_desc->{$key};$attr = array('maxlength'=>$inf->lenght);
	printf($tpl,'col-md-12', $form->textarea(array($desc_prefix,$k, $key),$inf->label, $desc_data->{$key},$inf->type, $attr));

	$key = 'category_description_vrc_icon';$inf = $info_desc->{$key};$attr = array('maxlength'=>$inf->lenght);
	#printf($tpl,'col-md-12', $form->text(array($desc_prefix,$k,$key),$inf->label, $desc_data->{$key},$inf->type, $attr));
	echo $form->hidden(array($desc_prefix,$k,$key), $desc_data->{$key});
	
	
	$key = 'category_description_vrc_meta_keyword';$inf = $info_desc->{$key};$attr = array('maxlength'=>$inf->lenght);
	printf($tpl,'col-md-12', $form->text(array($desc_prefix,$k, $key),$inf->label, $desc_data->{$key},$inf->type, $attr));
	
	$key = 'category_description_vrc_meta_description';$inf = $info_desc->{$key};$attr = array('maxlength'=>$inf->lenght);
	printf($tpl,'col-md-12', $form->textarea(array($desc_prefix,$k, $key),$inf->label, $desc_data->{$key},$inf->type, $attr));
	echo '</div>';
endforeach;
#echo '</fieldset>';
printf('<div class="col-md-12">%s<div class="clear"></div></div><br/>', $form->submit('Salvar','btn btn-primary button-right'));
echo $form->close();