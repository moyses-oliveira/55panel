<div class="attr" id="{div_id}" >
	<input type="hidden" name="attr[{attr_count}][product_attribute_int_id]" value="{product_attribute_int_id}" />
	<input type="hidden" name="attr[{attr_count}][product_attribute_chr_language]" value="{product_attribute_chr_language}" />
	<input type="hidden" name="attr[{attr_count}][product_attribute_vrc_label]" value="{escape_label}" />
	<input type="hidden" name="attr[{attr_count}][product_attribute_txt_description]" value="{escape_description}" />
	<div class="col-xs-6"><strong>Propriedade:</strong> {product_attribute_vrc_label}</div>
	<div class="col-xs-6">
		<a class="btn btn-primary bg-red no-border button-right" style="margin: 0 5px;" href="" onclick="$(this).closest('div.attr').remove();return false;" ><i class="fa fa-close"></i></a>
		<a class="btn btn-primary no-border button-right load_modal_url" href="{edit_url}"><i class="fa fa-pencil"></i></a>
	</div>
	<div class="col-xs-12"><strong>Descrição:</strong> {product_attribute_txt_description}</div>
	<div class="clear"></div>
</div>