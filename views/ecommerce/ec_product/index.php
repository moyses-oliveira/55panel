<div class="box-body table-responsive" style='padding: 10px;'>
<?php 
$grid = new gridView();
$items = array(
	'product_description_vrc_name'=>'Nome',
	'category_description_vrc_name'=>'Categoria',
	'product_description_vrc_summary'=>'Resumo'
);

foreach($data as &$i):
	$i->product_description_vrc_summary = sprintf('
	<div class="summary">
		<div class="full-text text-nowrap">%s</div>
		<div class="text-right more"><a href="#" ><i class="fa fa-plus"></i> mais</a> &nbsp; &nbsp;</div>
		<div class="text-right less" style="display: none;"><a href="#"><i class="fa fa-minus"></i> menos</a> &nbsp; &nbsp;</div>
	</div>',
	$i->product_description_vrc_summary);
	$i = (object)$i;
endforeach;

foreach($items as $k=>$label)
  $grid->addColumn(new gridViewColumn($k,$label));
  
$grid->addActionColumn(H::module(),'product_int_id', '90px', array(
	array('action' => 'update', 'class' => 'fa-pencil', 'title' => 'Editar'),
	array('action' => 'delete', 'class' => 'fa-close', 'title' => 'Remover')
	),
	false
);

$form = new form();
echo $form->open('product-form',H::link(H::module(), 'index'),'form-product', array('class'=>'default-form', 'style'=>'margin: 10px 20px;', 'method'=>'get'));
$find = $form->text(array('find'), 'Digite sua busca', @$_REQUEST['find'], '', array('maxlength'=>255), array('style'=>'display: none;'));
printf('<div class="col-xs-8">%s</div>', $find);
printf('<div class="col-xs-4">%s</div>',$form->submit('Procurar','btn btn-primary'));
echo $form->close();

if(!!$data):
	$data = (array)$data;
	echo $grid->renderTable($data);

	printf(
		'<div class="row">
			<div class="col-md-6">%s</div>
			<div class="col-md-6">%s</div>
		</div>
		',
		$pagination->getPaginationInfo(), 
		$pagination->getPaginationBar()
	);
else:
	echo '<br/><br/>'.H::msgBox('Nenhum item encontrado',true, H::WARNING);
endif;

?>
</div>
<style type="text/css">
div.text-nowrap { white-space: nowrap; overflow: hidden; text-overflow: ellipsis; width: 100%;max-width: 300px;}
</style>
<script type="text/javascript">
	$('.summary').each(function(){
		var summary = $(this);
		$(this).find('.more a').click(function(e){
			e.preventDefault();
			summary.find('.full-text').removeClass('text-nowrap');
			$(this).parent().hide();
			summary.find('.less').show();
		});
		$(this).find('.less a').click(function(e){
			e.preventDefault();
			summary.find('.full-text').addClass('text-nowrap');
			$(this).parent().hide();
			summary.find('.more').show();
		});
		
	});
</script>
