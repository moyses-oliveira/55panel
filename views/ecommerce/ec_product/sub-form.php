<?php

printf('<h3 class="text-center" style="color: #1A5D8B;"><span style="color: #999;">Variação do Produto: </span> %s</h3>' . PHP_EOL, $product_name);
$form = new form();

echo $form->warningBox($errors,'max-800 center');

if(isset($_GET['first'])):
	echo '<div class="clear"></div>';
	echo '<div class="max-800 center">';
	echo H::msgBox('
		<strong>Seu produto foi salvo com sucesso.</strong><br/> 
		Configure uma variação deste produto para finalizar o cadastro. <br/>
		Em seguida você poderá criar outras variações clicando no botão <strong>Voltar ao Produto</strong>.', true, H::WARNING);
	echo '<div class="clear"></div></div>';
endif;

printf('<div class="max-800 center"><a href="%s" class="btn btn-warning btn-block"  style="font-size: 16px;"><i class="fa fa-arrow-circle-left"></i> &nbsp;Voltar ao Produto</a></div>', H::link(H::module(), 'update', H::cod()));
	



echo $form->sucessBox($isUpdated, 'max-800 center');

echo $form->open('product-sub-form',URL::atual(),'form-product-sub', array('class'=>'default-form max-800 center'));
$tpl = '<div class="%s">%s</div>';
echo '<fieldset id="tab-properties" class="">';
echo '<legend>Propriedades</legend>';

$key = 'product_sub_vrc_code';
$inf = $info->{$key};$attr = array('maxlength'=>$inf->lenght);
printf($tpl,'col-md-12', $form->text(array('product', $key), $inf->label, $data->{$key}, $inf->type, $attr));

$key = 'product_sub_tny_enabled';
$inf = $info->{$key};$attr = array('maxlength'=>$inf->lenght);$options=array(array('0','Não'),array('1','Sim'));
printf($tpl,'col-md-3', $form->select(array('product', $key), $inf->label, $data->{$key}, $options, $inf->type, $attr));


$fields = array('product_sub_dcm_stock_current', 'product_sub_dcm_stock_alert', 'product_sub_dcm_stock_minimal', 'product_sub_dcm_length', 'product_sub_dcm_width', 'product_sub_dcm_height', 'product_sub_int_size_class', 'product_sub_dcm_weight','product_sub_int_weight_class');
$selects = array('product_sub_int_size_class'=>'size_classes','product_sub_int_weight_class'=>'weight_classes');
foreach($fields as $key):
	$inf = $info->{$key};
	$attr = array('maxlength'=>$inf->lenght);
	$col = !substr_count($key, 'dcm_stock') ? 'col-md-3': 'col-md-3';
	if(in_array($key, array('product_sub_dcm_length', 'product_sub_dcm_width', 'product_sub_dcm_height')))
		$inf->label .= ' (m)';
	if(in_array($key, array('product_sub_dcm_weight')))
		$inf->label .= ' (Kg)';
	
	$col = !substr_count($key, 'dcm_stock') ? 'col-md-3': 'col-md-3';
	if(array_key_exists($key, $selects)):
		$options = $$selects[$key];
		printf($tpl, $col . ' hide', $form->select(array('product', $key), $inf->label, $data->{$key}, $options, $inf->type, $attr));
		continue;
	endif;
	if($key == 'product_sub_dcm_stock_current' && H::action() == 'update'):
		$inf->type = 'decimal_3';
		$attr['disabled'] = 'disabled';
	endif;
	if(substr_count($key,'dcm') > 0 && H::action() == 'create') $data->{$key} = '0.000';
	printf($tpl, $col, $form->text(array('product', $key), $inf->label, $data->{$key}, $inf->type, $attr));
endforeach;


echo '</fieldset>';
if($filter_list)
	include('sub-form-filter.php');

if($option_list)
	include('sub-form-option.php');


printf('<button type="submit" class="btn btn-primary pull-right">Salvar</button><div class="clear"></div>');

echo $form->close();

if(!!URL::friend(3)):
	/*
	$desc_prefix = 'desc';
	echo '<div class="default-form" style="margin: 10px 20px;">';
	$tpl = '<div class="%s">%s</div>';

	echo '<fieldset class="col-md-12">';
		printf('<legend>Cadastro de Preço</legend>');
		
		echo $form->warningBox($errors2,'max-800 center');
		
		echo $form->open('product-sub-price-form',URL::atual(),'form-product-sub-price', array('class'=>'default-form max-800 center'));
		
		$key = 'product_sub_price_dcm_cost';$inf = $info_price->{$key};$attr = array('maxlength'=>$inf->lenght);
		printf($tpl,'col-md-4', $form->text(array('product_sub_price', $key), $inf->label, $data_price->{$key}, $inf->type . ' not_zero', $attr));

		$key = 'product_sub_price_dcm_price';$inf = $info_price->{$key};$attr = array('maxlength'=>$inf->lenght);
		printf($tpl,'col-md-4', $form->text(array('product_sub_price', $key), $inf->label, $data_price->{$key}, $inf->type . ' not_zero', $attr));

		
		
		printf($tpl,'col-md-3', '<label>&nbsp;</label><input type="submit" class="btn btn-primary btn-block" value="Definir Preço" />');
		
		$clear_price_url = H::link(H::module(), 'sub-item-price-clear', H::cod(), URL::friend(3));
		$clear_price = sprintf('<label>&nbsp;</label><buttom class="btn btn-danger btn-block clear-price" href="%s"><i class="fa fa-remove"></i></button>', $clear_price_url);
		if(!!$data_price->product_sub_price_dcm_price)
			printf($tpl,'col-md-1', $clear_price);
		
		echo $form->close();

	echo '</fieldset>';
	


	echo '<fieldset class="" style="padding: 0 20px;"><legend>Histórico de Alteração de Preços</legend>';
		$grid = new gridView();
		$items = array(
			'dtt_added'=>'Alterado em:',
			'int_added'=>'Alterado por:',
			'cost'=>'Custo',
			'price'=>'Preço'
		);
		foreach($items as $k=>$label)
		  $grid->addColumn(new gridViewColumn($k,$label));
		 
		 
		foreach($price_history as &$p):
			foreach(array('int_deleted', 'int_added') as $j):
				if(!empty($p->{$j}))
					$p->{$j} = isset($users[$p->{$j}]) ? $users[$p->{$j}] : 'Developer';
				else
					$p->{$j} = '<span class="text-center col-md-12"> - </span>';
			endforeach;
			
			foreach(array('dtt_deleted', 'dtt_added') as $j):
				if(!empty($p->{$j}))
					$p->{$j} = CData::format('d/m/Y H:i', $p->{$j});
				else
					$p->{$j} = '<span class="text-center col-md-12"> - </span>';
			endforeach;
		endforeach;
		echo '<div style="max-height: 140px; overflow-y: auto;padding: 5px;">';
		echo $grid->renderTable($price_history);
		echo '</div>';
	echo '</fieldset>';
	*/


	echo '<fieldset>';
	echo '<legend>Informações de Estoque</legend>';
	echo '<form class="default-form">';
	
	$fields = array('product_sub_dcm_stock_current', 'product_sub_dcm_stock_alert', 'product_sub_dcm_stock_minimal');
	foreach($fields as $key):
		$inf = $info->{$key};
		$attr = array('maxlength'=>$inf->lenght,'disabled'=>'disabled');
		printf($tpl, 'col-md-3', $form->text(array('product', $key), $inf->label, $data->{$key}, '', $attr));

	endforeach;
	$btnAdd = sprintf('<a class="btn btn-primary col-xs-12 load_form_modal" title="Corrigir Estoque" href="%s" id="upgrade-stock" ><i class="fa fa-pencil"></i> Corrigir Estoque</a>', H::link(H::module(), 'upgrade-stock', H::cod(), URL::friend(3)));
	printf($tpl, 'col-md-3', '<label>&nbsp;</label>'.$btnAdd);
	
	echo '<form>';
	echo '</fieldset>';


	echo '<fieldset class="col-md-12"><legend>Histórico de estoque</legend>';
	$grid = new gridView();

	foreach($stock_history as &$p):
		$j = 'prod_stock_int_added';
		if(!empty($p->{$j}))
			$p->{$j} = isset($users[$p->{$j}]) ? $users[$p->{$j}] : 'Sistema';
		else
			$p->{$j} = '<span class="text-center col-md-12"> - </span>';
		
		$j = 'prod_stock_dtt_added';
		if(!empty($p->{$j}))
			$p->{$j} = CData::format('d/m/Y', $p->{$j});
		else
			$p->{$j} = '<span class="text-center col-md-12"> - </span>';
			
	endforeach;


	$grid->addColumn(new gridViewColumn('prod_stock_dtt_added','Data de Alteração', array('width'=>'130px')));
	$grid->addColumn(new gridViewColumn('prod_stock_int_added','Modificado por:', array('width'=>'130px')));
	$grid->addColumn(new gridViewColumn('prod_stock_dcm_correction','Qtd.', array('width'=>'100px')));
	$grid->addColumn(new gridViewColumn('prod_stock_vrc_note','Observações'));


	echo $grid->renderTable($stock_history);
	
	echo '</fieldset>';

	echo '<div class="clear"></div></div>';
endif;