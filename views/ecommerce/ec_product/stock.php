<?php
$form = new form();
/*
if($isUpdated):
	printf('<div class="max-800 center" style="padding-top: 20px;" >%1$s%2$s%1$s</div>',
		PHP_EOL, H::msgBox('Os dados foram salvos com sucesso!', true, H::SUCCESS)
	);
endif;
*/
$msg = ''; 
 if(count($errors) > 0): 
	foreach($errors as $k=>$v) $msg .= sprintf('<li>%s</li>',$v);
	$msg = '<ul>' . $msg . '</ul>';
endif;
printf('<div class="warnings">%s</div>',$form->warningBox($msg, 'max-800 center','Atenção', count($errors) < 1));
$desc_prefix = 'desc';
echo $form->open('product-form',URL::atual(),'form-product', array('class'=>'default-form', 'style'=>'margin: 10px 20px;'));
$tpl = '<div class="%s">%s</div>';

echo '<fieldset id="tab-properties" class="col-md-12 tab-content">';
echo '<legend>Informações de Estoque</legend>';

$fields = array('product_dcm_stock_current', 'product_dcm_stock_alert', 'product_dcm_stock_minimal');
foreach($fields as $key):
	$inf = $info->{$key};
	$attr = array('maxlength'=>$inf->lenght,'disabled'=>'disabled');
	printf($tpl, 'col-md-3', $form->text(array('product', $key), $inf->label, $data->{$key}, '', $attr));

endforeach;
$btnAdd = sprintf('<a class="btn btn-primary col-xs-12 load_form_modal" title="Corrigir Estoque" href="%s" id="upgrade-stock" ><i class="fa fa-pencil"></i> Corrigir Estoque</a>', H::link(H::module(), 'upgrade-stock', H::cod()));
printf($tpl, 'col-md-3', '<label>&nbsp;</label>'.$btnAdd);
echo '</fieldset>';


echo '<fieldset class="col-md-12"><legend>Histórico</legend>';
$grid = new gridView();

foreach($stock_history as &$p):
	$j = 'prod_stock_int_added';
	if(!empty($p->{$j}))
		$p->{$j} = isset($users[$p->{$j}]) ? $users[$p->{$j}] : 'Sistema';
	else
		$p->{$j} = '<span class="text-center col-md-12"> - </span>';
	
	$j = 'prod_stock_dtt_added';
	if(!empty($p->{$j}))
		$p->{$j} = CData::format('d/m/Y', $p->{$j});
	else
		$p->{$j} = '<span class="text-center col-md-12"> - </span>';
		
endforeach;
#, , prod_stock_int_added, prod_stock_dtt_added
$items = array(
	'prod_stock_dtt_added'=>'Data de Cadastro',
	'prod_stock_int_added'=>'Modificado por:',
	'prod_stock_vrc_note'=>'Observações',
	'prod_stock_dcm_correction'=>'Quantidade Alterada'
);
foreach($items as $k=>$label)
  $grid->addColumn(new gridViewColumn($k,$label));

echo $grid->renderTable($stock_history);

echo '</fieldset>';

echo $form->close();
?>
<script type="text/javascript">
/*
$(document).ready(function(){
	$('#upgrade-stock').click(function(){
		
		return false;
	});
});
*/
</script>