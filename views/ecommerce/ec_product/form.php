<?php
$form = new form();

echo $form->sucessBox($isUpdated, 'max-800 center');
echo $form->warningBox($errors,'max-800 center');

$desc_prefix = 'desc';
echo $form->open('product-form',URL::atual(),'form-product', array('class'=>'default-form', 'style'=>'margin: 10px 20px;'));
$tpl = '<div class="%s">%s</div>';

printf('<input type="hidden" id="action-value" value="%s" />', H::action());
echo '<ul class="steps col-md-4 col-md-pull-right">';
$step = 1;
printf('<li><a class="btn btn-primary act" href="#tab-properties">Passo %s: Propriedades</a></li>', $step);
$step++;
printf('<li><a class="btn btn-primary" href="#tab-desc-br">Passo %s: Descrição</a></li>', $step);
$step++;
printf('<li><a class="btn btn-primary" href="#tab-filters">Passo %s: Filtros</a></li>', $step);
$step++;
printf('<li><a class="btn btn-primary" href="#tab-price">Passo %s: Preço</a></li>', $step);
$step++;
printf('<li><a class="btn btn-primary" href="#tab-images">Passo %s: Imagens</a></li>', $step);

if(H::action() == 'update'):
	$step++;
	printf('<li><a class="btn btn-primary" href="#tab-sub-products">Variações do Produto</a></li>', $step);
endif;

echo '<li>'.$form->submit('Salvar','btn btn-primary').'</li>';
echo '</ul>';

echo '<fieldset id="tab-properties" class="col-md-8 tab-content">';
echo '<legend>Passo 1: Propriedades</legend>';

$key = 'product_dtt_start';$inf = $info->{$key};$attr = array('maxlength'=>$inf->lenght);
$data->{$key} = !$data->{$key} ? date('d/m/Y'): CData::format('d/m/Y',$data->{$key});
printf($tpl,'col-md-4', $form->text(array('product', $key), $inf->label, $data->{$key}, $inf->type . ' newest', $attr));

$key = 'product_dtt_end';$inf = $info->{$key};$attr = array('maxlength'=>$inf->lenght);
$data->{$key} = !$data->{$key} ? null: CData::format('d/m/Y',$data->{$key});
printf($tpl,'col-md-4', $form->text(array('product', $key), $inf->label, $data->{$key}, $inf->type . ' newest date_exhibition', $attr));

echo '<div class="clear"></div>';
$key = 'product_tny_featured';
$inf = $info->{$key};$attr = array('maxlength'=>$inf->lenght);$options=array(array('0','Não'),array('1','Sim'));
printf($tpl,'col-md-4', $form->select(array('product', $key), $inf->label, $data->{$key}, $options, $inf->type, $attr));

$key = 'product_int_category';$inf = $info->{$key};$attr = array('maxlength'=>$inf->lenght);
printf($tpl,'col-md-4', $form->select(array('product', $key), $inf->label, $data->{$key}, $categories, $inf->type, $attr));

$key = 'product_int_product_maker';$inf = $info->{$key};$attr = array('maxlength'=>$inf->lenght);
printf($tpl,'col-md-4', $form->select(array('product', $key), $inf->label, $data->{$key}, $makers, $inf->type, $attr));
echo '<div class="clear"></div>';
echo '</fieldset>';


$step = 1;
// Descrição e propriedades em multiliguagem
$prop_tpl = file_get_contents('views/ecommerce/ec_product/attr-item.tpl');

foreach($desc_data_list as $k=>$desc_data):
	$step++;
	$lang = 'pt_br';
	echo '<fieldset id="tab-desc-br" class="col-md-8 tab-content">';
	printf('<legend>Passo %s: Descrição</legend>'.PHP_EOL, $step);
	echo $form->hidden(array($desc_prefix,$k,'product_description_chr_language'), $lang);
	
	$fields = array('vrc_name','vrc_summary','txt_description','vrc_meta_keyword','vrc_meta_description');

	foreach($fields as $s):
		$key = 'product_description_' . $s;$inf = $info_desc->{$key};
		$attr = array('maxlength'=>$inf->lenght);
		if($s == 'txt_description'): 
			$attr['style']='height: 184px;';
			$inf->type .= ' html_textfield';
		endif;
		$fieldType = $inf->lenght > 999 ? 'textarea' : 'text';
		printf($tpl,'col-md-12', $form->$fieldType(array($desc_prefix,$k, $key),$inf->label, $desc_data->{$key},$inf->type, $attr));
	endforeach;
	
		echo '<fieldset class="attributes"><legend>Propriedades do Produto</legend>';
		$http_query_add_attr = http_build_query(array('product_attribute_chr_language'=>$lang));
		printf('
			<div style="padding: 0 15px;">	
				<a href="%s" _title="Adicionar Propriedade" class="btn btn-primary bg-green no-border col-xs-12 load_modal_url">Adicionar Propriedade</a>
				<div class="clear"></div>
			</div>',
			H::link(H::module(), 'attr','?'.$http_query_add_attr)
		);
		if(!isset($attr_count)) $attr_count = 0;
		
		foreach($attr_data_list as $ka=>$attr):
			if($attr->product_attribute_chr_language == $lang):		
				$arAttr = (array)$attr;
				$arAttr['div_id'] = 'attr-' . md5(str_shuffle(uniqid()).str_shuffle(uniqid()));
				$http_query_edit = http_build_query($arAttr);
				$arAttr['escape_label'] = str_replace('\"','\\"',$attr->product_attribute_vrc_label);
				$arAttr['escape_description'] = str_replace('\"','\\"',$attr->product_attribute_txt_description);
				$arAttr['attr_count'] = $attr_count;
				$arAttr['edit_url'] = H::link(H::module(), 'attr','?'.$http_query_edit);
				$keysAttr = array_keys($arAttr);
				foreach($keysAttr as &$keyAttr) $keyAttr = '{'.$keyAttr.'}';
				echo str_replace(
					$keysAttr,
					array_values($arAttr),
					$prop_tpl
				);
				$attr_count++;
			endif;
		endforeach;
		echo '</fieldset>';
	echo '</fieldset>';
endforeach;


$step++;
echo '<fieldset id="tab-filters" class="col-md-8 tab-content">';
	printf('<legend>Passo %s: Filtros</legend>'.PHP_EOL, $step);
	$label = null;
	echo '<ul class="filters col-xs-4">';
	$count = 0;
	foreach($filter_list as $f):
		if($label != $f->label):
			$label = $f->label;
			$act = $count == 0 ? 'btn-success' : 'btn-primary';
			printf('<li class="btn btn-block %s" for="#tab-filter-%s">%s</li>', $act, $count, $label);
			$count++;
		endif;
	endforeach;
	echo '</ul>';


	$count = 0;
	$label = null;
	
	foreach($filter_list as $f):
		if($label != $f->label):
			if($label != null) echo '</fieldset>';
			$label = $f->label;
			printf('<fieldset class="col-xs-8 tab-content" id="tab-filter-%s"><legend>%s</legend><div style="height: 250px; overflow-y: auto;padding: 5px;">', $count, $label);
			$count++;
			if(!!$f->single):
				echo '<div class="btn btn-default btn-block uncheck" style="text-align: left;padding-left:15px;"><i class="fa fa-toggle-off"></i> Nenhum item</div>';
			endif;
		endif;
		$type = !!$f->single ? 'radio' : 'checkbox';
		echo '<label class="btn btn-default btn-block" style="text-align:left; padding: 3px 0 2px 15px; margin: -1px;">';
		printf('<input type="%s" name="filter[%s][]" value="%s" %s /> &nbsp;%s', $type, $count, $f->item_id, $f->checked, $f->value);
		echo '</label>';
	endforeach;
	if(count($filter_list)) echo '</div></fieldset>';
echo '</fieldset>';


$step++;
echo '<div id="tab-price" class="col-md-8 tab-content">';
echo '<fieldset class="col-md-12">';
printf('<legend>Cadastro de Preços Base deste Produtos</legend>',$step);

$key = 'product_price_dcm_cost';$inf = $info_price->{$key};$attr = array('maxlength'=>$inf->lenght);
printf($tpl,'col-md-4', $form->text(array('product', $key), $inf->label, $data_price->{$key}, $inf->type . ' not_zero', $attr));

$key = 'product_price_dcm_price';$inf = $info_price->{$key};$attr = array('maxlength'=>$inf->lenght);
printf($tpl,'col-md-4', $form->text(array('product', $key), $inf->label, $data_price->{$key}, $inf->type . ' not_zero', $attr));

echo '</fieldset>';

echo '<fieldset class="col-md-12"><legend>Histórico de Preço</legend>';
$grid = new gridView();
$items = array(
	'dtt_added'=>'Alterado em:',
	'int_added'=>'Alterado por:',
	'cost'=>'Custo',
	'price'=>'Preço'
);
foreach($items as $k=>$label)
  $grid->addColumn(new gridViewColumn($k,$label));
 
foreach($price_history as &$p):
	foreach(array('int_deleted', 'int_added') as $j):
		if(!empty($p->{$j}))
			$p->{$j} = isset($users[$p->{$j}]) ? $users[$p->{$j}] : 'Developer';
		else
			$p->{$j} = '<span class="text-center col-md-12"> - </span>';
	endforeach;
	
	foreach(array('dtt_deleted', 'dtt_added') as $j):
		if(!empty($p->{$j}))
			$p->{$j} = CData::format('d/m/Y H:i', $p->{$j});
		else
			$p->{$j} = '<span class="text-center col-md-12"> - </span>';
	endforeach;
endforeach;

echo $grid->renderTable($price_history);
echo '</fieldset>';

echo '</div>';

$step++;
echo '<fieldset id="tab-images" class="col-md-8 tab-content">';
printf('<legend>Passo %s: Imagens</legend>',$step);

$gallery_tpl = file_get_contents('views/ecommerce/ec_product/gallery-item.tpl');

$root_path = URL_ALIAS;
$gallery_images = '';
foreach($img_data_list as $ki=>$img):
	
	$img->name = basename($img->img);
	$path = substr($img->img, 0,-1 * strlen(basename($img->img)));
	
	$img_path = H::path_combine(array('files', 'personal', $root_path, 'gallery', 'images'), false, '/');
	$thumb_path = H::path_combine(array('files', 'personal', $root_path, 'gallery', 'thumbs'), false, '/') . $path;
	$img->thumb = H::root() . ImagePlugin::resize($img_path . $img->img, trim($thumb_path,'/'), 180, 135, false);
	$img->input_key = $ki;
	$params = array_keys((array)$img); 
	foreach($params as &$v) $v = '{' . $v . '}';
	$values = array_values((array)$img);
	$gallery_images .= str_replace($params, $values, $gallery_tpl);
	$gallery_images .= PHP_EOL . PHP_EOL;
endforeach;

printf('<div class="gallery-list product-gallery">%s</div>', $gallery_images);
printf('<a class="col-xs-12 btn btn-primary add_img_from_gallery" href="%s" _title="Galeria" single="0" target="#tab-images .gallery-list" input_name="product_img" template="product_img"><i class="fa fa-plus"></i>&nbsp;Adicionar Imagem</a>', H::link(H::module(),'gallery','modal:1'));
echo '</fieldset>';


if(H::action() == 'update'):
	$step++;

	echo '<fieldset id="tab-sub-products" class="col-md-8 tab-content">';
	printf('<legend>Variações do Produto</legend>',$step);
	ECHO PHP_EOL . '<div class="box-body table-responsive" style="padding: 10px 50px;">' . PHP_EOL;

	printf('<a href="%s" class="btn btn-primary btn-block">Adicionar Variação deste Produto</a>', H::link(H::module(), 'sub-item', H::cod()));
	if(!!$subproducts):
		$grid = new gridView();
		$labels = array(
			'product_sub_vrc_code'=>'Código do Produto',
			'filter'=>'Filtro'
		);
		foreach($labels as $k=>$label)
		  $grid->addColumn(new gridViewColumn($k,$label));
		  
		$grid->addActionColumn(H::module(),'product_sub_int_id', '90px', array(
			array('action' => 'update', 'class' => 'fa-pencil', 'title' => 'Editar', 'url'=>H::link(H::module(), 'sub-item', H::cod(), '{product_sub_int_id}')),
			array('action' => 'delete', 'class' => 'fa-close', 'title' => 'Remover', 'url'=>H::link(H::module(), 'sub-item-delete', H::cod(), '{product_sub_int_id}'))
			),
			false
		);

		echo $grid->renderTable($subproducts);
	else:
		echo '<br/><br/>'.H::msgBox('Nenhum item encontrado',true, H::WARNING);
	endif;

	echo '<div class="clear"></div>';
	echo PHP_EOL . '</fieldset>' . PHP_EOL;
endif;

echo $form->close();
?>



<script type="text/javascript">
var is_new_product = <?php echo H::action() == 'create' ? 'true' : 'false'; ?>;
var attributes_html = "<?php echo str_replace(array('"',"\n","\r","\r\n"), array('\\"',"\\n","\\r","\\r\\n"), $prop_tpl);?>";
var attr_count = <?php echo $attr_count * 1;?>;
$(document).ready(function(){
	staticGalleryPlugin.templates["product_img"] = "<?php echo str_replace(array('"',"\n","\r","\r\n",'{pk}','{position}'), array('\\"',"\\n","\\r","\\r\\n",'',''), $gallery_tpl);?>";
	lsProductImg.forcePositions();
	$('#tab-images .product-gallery').on("afterDelete", function(){
		lsProductImg.forcePositions();
	});
	$('#tab-images .product-gallery').on("afterInsert", function(){
		lsProductImg.forcePositions();
	});
	
//	if (tinymce)
//		tinymce.execCommand('mceRemoveEditor', true, 'field-textblock-HTML');    
	tinymce.init({
		selector: "textarea.html_textfield",
		language: 'pt_BR',
		theme: 'modern',
		menu: {
			edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
			insert: {title: 'Insert', items: 'template hr'},
			view: {title: 'View', items: 'visualaid preview'},
			format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
			table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
		},
		plugins: [
			"advlist autolink lists link image charmap preview anchor textcolor",
			"searchreplace visualblocks code fullscreen",
			"media table contextmenu paste"
		],
		toolbar: [
			" styleselect | fontselect | fontsizeselect | print preview | image charmap | table | forecolor ",
			"insertfile | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | searchreplace | code "
		],
		setup: function (editor) {
			editor.on('keyup', function () {
				$(editor).find("textarea").first().html(editor.getContent());
			});
			editor.on('change', function () {
				$(editor).find("textarea").first().html(editor.getContent());
			});
			editor.on('keydown', function (e) {
				if (e.keyCode == 9) {
					editor.execCommand(e.shiftKey ? 'Outdent' : 'Indent');
					e.preventDefault();
					return false;
				}
			});
		}
	});
	
});
</script>