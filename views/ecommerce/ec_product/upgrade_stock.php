<?php
$form = new form();

echo $form->warningBox($errors,'max-800 center');

$desc_prefix = 'desc';
echo $form->open('form-product-stock',URL::atual(),'form-product-stock', array('class'=>'default-form', 'style'=>'margin: 10px 20px;'));
$tpl = '<div class="%s">%s</div>';

$key = 'tny_add';
$options = array(array(1,'Adicionar'),array(-1,'Remover'));
printf($tpl,'col-xs-12', $form->select(array('stock', $key), 'Ação', '0.000', $options,''));


$key = 'prod_stock_dcm_correction';$inf = $info->{$key};$attr = array('maxlength'=>$inf->lenght);
printf($tpl,'col-xs-12', $form->text(array('stock', $key), $inf->label, '0.000', $inf->type . ' not_zero', $attr));

$key = 'prod_stock_vrc_note';$inf = $info->{$key};$attr = array('maxlength'=>$inf->lenght, 'style'=>'height: 55px;');
printf($tpl,'col-xs-12', $form->textarea(array('stock', $key), $inf->label, '', $inf->type, $attr));

echo '<div class="col-xs-12"><button type="submit" class="btn btn-primary col-xs-12"><i class="fa fa-save"></i> Salvar</button></div>';

echo $form->close();