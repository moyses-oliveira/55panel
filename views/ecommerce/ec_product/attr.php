<?php
$tpl = '<div class="%s">%s</div>';
$form = new form();
printf('<div class="warnings">%s</div>',$form->warningBox([], 'max-800 center','Atenção', true));
echo $form->open('attribute-form',URL::atual(),'attribute-form', array('class'=>'default-form', 'style'=>'margin: 10px 20px;'));

$key = 'div_id';$data = isset($_REQUEST[$key]) ? $_REQUEST[$key] : '';
echo $form->hidden(array($key), $data) . PHP_EOL;

$key = 'product_attribute_int_id';$data = isset($_REQUEST[$key]) ? $_REQUEST[$key] : '';
echo $form->hidden(array($key), $data) . PHP_EOL;

$key = 'product_attribute_chr_language';$data = isset($_REQUEST[$key]) ? $_REQUEST[$key] : '';
echo $form->hidden(array($key), $data) . PHP_EOL;

#$key = 'product_attribute_vrc_alias';$type = '';$attr = array('maxlength'=>45);$label = 'Filtro';
#$data = isset($_REQUEST[$key]) ? $_REQUEST[$key] : '';
#printf($tpl,'col-md-12', $form->text(array($key), $label, $data, $type, $attr));

$key = 'product_attribute_vrc_label';$type = 'not_null';$attr = array('maxlength'=>255);$label = 'Propriedade';
$data = isset($_REQUEST[$key]) ? $_REQUEST[$key] : '';
printf($tpl,'col-md-12', $form->text(array($key), $label, $data, $type, $attr));

$key = 'product_attribute_txt_description';$type = 'not_null';$attr = array();$label = 'Descrição';
$data = isset($_REQUEST[$key]) ? $_REQUEST[$key] : '';
printf($tpl,'col-md-12', $form->textarea(array($key), $label, $data, $type, $attr));
$btn_title = isset($_GET['product_attribute_vrc_label']) ? 'Salvar' : 'Adicionar';
$btn = sprintf('<button type="submit" class="btn btn-primary button-right" id="btn-product_attribute_insert">%s</button>', $btn_title);
printf($tpl,'col-md-12', $btn);

echo $form->close();

$type = isset($_REQUEST['product_attribute_int_id']) ? 'update' : 'insert';
?>
<script>
var modal_attr_type = '<?php echo $type;?>';
$(document).ready(function(){
	 var update_target = '#modal_' + msgBoxStatic.modal.uid + ' .modal-body';
	globalFormConfig(update_target);
	DivMsgWidget(update_target);
	$('#btn-product_attribute_insert').click(function(){
		try {
			if(!validator.formValidate('#attribute-form'))
				return false;
			
			var prop = {};
			var properties = [];
			if(modal_attr_type == 'insert')
				$('#field-div_id').val((new Date()).getTime());
				
			$('#attribute-form input, #attribute-form textarea').each(function(){
				properties.push(['{' + $(this).attr('name') + '}',$(this).val()]);
				prop[$(this).attr('name')] = $(this).val();
			});		
			properties.push(['{escape_label}', prop.product_attribute_vrc_label.replace(/\"/g,'\\"')]);
			properties.push(['{escape_description}', prop.product_attribute_txt_description.replace(/\"/g,'\\"')]);
			
			var edit_query = $.param(prop);
			var edit_url = BASE_URL + 'ecommerce.ls-product/attr?' + edit_query;
			
			properties.push(['{attr_count}', attr_count]);
			properties.push(['{edit_url}', edit_url]);
			
			if(modal_attr_type == 'insert') {
				var htmlData = attributes_html + '';
				$(properties).each(function(k,item){
					while(htmlData.indexOf(item[0])  > -1) {
						htmlData = htmlData.replace(item[0],item[1]);
					}
				});
				$('fieldset.tab-content:visible fieldset.attributes').append(htmlData);
				DivMsgWidget('fieldset.tab-content:visible fieldset.attributes div.attr:last-child');
				attr_count++;
			} else {
				var htmlData = $(attributes_html).html();
				$(properties).each(function(k,item){
					while(htmlData.indexOf(item[0])  > -1) {
						htmlData = htmlData.replace(item[0],item[1]);
					}
				});
				console.log(htmlData);
				$('#' + prop.div_id).html(htmlData);
				DivMsgWidget('#' + prop.div_id);
				
			}
			msgBoxStatic.modal.close();
		}
		catch(err) {
			console.log(err);
		}
		return false;
	});
});

</script>