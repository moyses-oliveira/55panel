<?php
echo '<fieldset class="filters-tab-group">';
	echo '<legend>Filtros</legend>';
	$label = null;
	echo '<ul class="filters col-xs-4">';
	$count = 0;
	foreach($filter_list as $f):
		if($label != $f->label):
			$label = $f->label;
			$act = $count == 0 ? 'btn-success' : 'btn-primary';
			printf('<li class="btn btn-block %s" for="#tab-filter-%s">%s</li>', $act, $count, $label);
			$count++;
		endif;
	endforeach;
	echo '</ul>';


	$count = 0;
	$label = null;
	
	foreach($filter_list as $f):
		if($label != $f->label):
			if($label != null) echo '</fieldset>';
			$label = $f->label;
			printf('<fieldset class="col-xs-8 tab-content" id="tab-filter-%s"><legend>%s</legend><div style="height: 250px; overflow-y: auto;padding: 5px;">', $count, $label);
			$count++;
			if(!!$f->single):
				echo '<div class="btn btn-default btn-block uncheck" style="text-align: left;padding-left:15px;"><i class="fa fa-toggle-off"></i> Nenhum item</div>';
			endif;
		endif;
		$type = !!$f->single ? 'radio' : 'checkbox';
		echo '<label class="btn btn-default btn-block" style="text-align:left; padding: 3px 0 2px 15px; margin: -1px;">';
		printf('<input type="%s" name="filter[%s][]" value="%s" %s /> &nbsp;%s', $type, $count, $f->item_id, $f->checked, $f->value);
		echo '</label>';
	endforeach;
	if(count($filter_list)) echo '</div></fieldset>';
echo '</fieldset>';