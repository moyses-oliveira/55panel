<div class="box-img" id="box-img-{input_key}" >
	<div class="msg">
		<span class="loading"><i class="fa fa-refresh fa-spin"></i></span>
		<div class="info">Deseja excluir?</div>
		<div>
			<span class="btn btn-default bg-green">Sim</span>
			<span class="btn btn-default bg-red">Não</span>
		</div>
	</div>
	<div class="actions">
		<div class="first">
			<a href="#" onclick="return false;" class="file-name">{name}</a>
			<a href="#" title="Remover" class="delete button-right"><i class="fa fa-trash"></i></a>
			<div class="clear"></div>
		</div>
		<div class="img" style="background-image: url('{thumb}');">
			&nbsp;
		</div>
		<input name="product_img[{input_key}][prod_img_int_id]" class="field_img_id" type="hidden" value="{pk}" />
		<input name="product_img[{input_key}][prod_img_int_position]" class="field_img_position" type="hidden" value="{position}" />
		<input name="product_img[{input_key}][prod_img_vrc_img]" class="field_img_img" type="hidden" value="{img}" />
		<div class="btns">
			<div class="btn btn-default up" onclick="return lsProductImg.posUp(this);"><i class="fa fa-caret-up"></i></div>
			<div class="btn btn-default down" onclick="return lsProductImg.posDown(this);"><i class="fa fa-caret-down"></i></div>
		</div>
		<div class="alt">
			<label>Alt</label>
			<input name="product_img[{input_key}][prod_img_vrc_alt]" type="text" value="{alt}">
		</div>
		<div class="title">
			<label>Title</label>
			<input name="product_img[{input_key}][prod_img_vrc_title]" type="text" value="{title}">
		</div>
		<div class="clear"></div>
	</div>
</div>