<?php
$form = new form();

$msg = ''; 
 if(count($errors) > 0): 
	foreach($errors as $k=>$v) $msg .= sprintf('<li>%s</li>',$v);
	$msg = '<ul>' . $msg . '</ul>';
endif;
printf('<div class="warnings">%s</div>',$form->warningBox($msg, 'max-800 center','Atenção', count($errors) < 1));
$label_prefix = 'cshr';
$val_prefix = 'value';

echo $form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center'));

if($saved):
	printf('<div class="max-800 center" style="padding-top: 20px;" >%1$s%2$s%1$s</div>',
		PHP_EOL, H::msgBox('Os dados foram salvos com sucesso!', true, H::SUCCESS)
	);
endif;

$tpl = '<div class="%s">%s</div>';

$key = 'region_int_zone'; $inf = $info->{$key}; $attr = array('maxlength'=>$inf->lenght, 'placeholder'=>$inf->label);
if(H::action() == 'update'):
	$attr['disabled'] = 'disabled';
	$inf->label .= ' <small class="text-warning">( Não é possivel alterar o Estado após criar. )</small>';
	printf($tpl,'col-md-12', $form->select(array($label_prefix,$key. '-readonly'), $inf->label, $data->{$key}, $zones, 'not_null', $attr));
	echo $form->hidden(array($label_prefix,$key), $data->{$key});
else:
	printf($tpl,'col-md-12', $form->select(array($label_prefix,$key), $inf->label, $data->{$key}, $zones, 'not_null', $attr));
endif;

$key = 'region_vrc_name'; $inf = $info->{$key}; $attr = array('maxlength'=>$inf->lenght, 'placeholder'=>$inf->label);
printf($tpl,'col-md-12', $form->text(array($label_prefix,$key), $inf->label, $data->{$key},'not_null', $attr));

$prefix = 'regw';
$lastLimit = 0;

$limits = array();
for($i=0; $i < 20; $i++)
	$limits[] = ($i+1) * 5;

$limits[] = 99999;
foreach($limits as $k=>$limit):
	
	$price = isset($items[$k]) ? number_format($items[$k]->regw_dcm_price,2,'.','') : '0.00';

	$key = 'regw_dcm_max_weight'; 
	echo $form->hidden(array($prefix, $k,$key), $limit);
	
	$label = $limit < 99999 ? sprintf('De %s até %s Kg R$',$lastLimit, $limit) : sprintf('Acima de %s', $lastLimit);
	$key = 'regw_dcm_price'; $inf = $info_regw->{$key}; $attr = array('maxlength'=>$inf->lenght);
	printf($tpl,'col-md-4', $form->text(array($prefix,$k,$key), $label, $price, $inf->type, $attr));
	$lastLimit = $limit;
endforeach;

printf('<div class="col-md-12">%s<div class="clear"></div></div><br/>', $form->submit('Salvar','btn btn-primary button-right'));

echo PHP_EOL . $form->close();
?>