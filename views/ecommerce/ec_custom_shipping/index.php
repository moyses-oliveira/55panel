<div class="box-body table-responsive" style='padding: 10px;'>
<?php 
$grid = new gridView();

foreach(array('region_int_zone', 'region_vrc_name') as $k)
  $grid->addColumn(new gridViewColumn($k,$info->{$k}->label));


$grid->addActionColumn(H::module(),'region_int_id', '90px', array(
	array('action' => 'update', 'class' => 'fa-pencil', 'title' => 'Editar'),
	array('action' => 'delete', 'class' => 'fa-close', 'title' => 'Remover')
	),
	false
);

if(!!$data):
	echo $grid->renderTable($data);
	$pag = new Pagination(false, array(), current($data)->total_records, 10);
	echo '<div class="row">';
	printf('<div class="col-md-6">
		<ul class="pagination">
			%s
			%s
			%s
			%s
		</ul>
	</div>',
		$pag->linkPrev(), 
		$pag->getCurrentPages(), 
		$pag->getNavigationGroupLinks(), 
		$pag->linkNext()
	);
	$start = 1 + (($pag->currentPage - 1) * $pag->recordByPage);
	$end = ($start + $pag->recordByPage - 1);
	$to = $pag->totalRecords > $end ? $end : $pag->totalRecords;
	printf('<div class="col-md-6"> %s até %s de %s registros.</div>',
	$start, $to, $pag->totalRecords);
	echo '</div>';
else:
	echo '<br/><br/>'.H::msgBox('Nenhum item encontrado',true, H::WARNING);
endif;

?>
</div>