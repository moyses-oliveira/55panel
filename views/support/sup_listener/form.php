<script type="text/javascript">
	
	$(document).on('change', '.btn-file :file', function () {
		var input = $(this), numFiles = input.get(0).files ? input.get(0).files.length : 1, label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [
			numFiles,
			label
		]);
	});
	$(document).ready(function () {
		$('.btn-file :file').on('fileselect', function (event, numFiles, label) {
			var input = $(this).parents('.input-group').find(':text'), log = numFiles > 1 ? numFiles + ' files selected' : label;
			if (input.length) {
				input.val(log);
			} else if (log) {
				
					alert(log);
			}
		});
	});
    
</script>
<style type="text/css">
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
</style>
<?php 
$form = new form();
$msg = ''; 

if($isUpdated):
	printf('<div class="max-800 center" style="padding-top: 20px;" >%1$s%2$s%1$s</div>',
		PHP_EOL, H::msgBox('Os dados foram salvos com sucesso!', true, H::SUCCESS)
	);
endif;

if(count($errors) > 0): 
	$msg = '<ul>';
	foreach($errors as $k=>$v) $msg .= sprintf('<li>%s</li>',$v);
	$msg .= '</ul>';
endif;
printf('<div class="warnings">%s</div>',H::warningBox($msg, 'max-800 center','Atenção', count($errors) < 1));

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));
$tpl = '<div class="%s">%s</div>';

$key = 'listener_int_user'; 
printf($tpl,'col-md-12',$form->select(array($table_prefix,$key),$labels[$key],$data->{$key},$options,$types[$key]));

$key = 'listener_vrc_name'; $attr=array('maxlength'=>1500);
printf($tpl,'col-md-12', $form->text(array($table_prefix,$key),$labels[$key],$data->{$key},$types[$key], $attr));

$key = 'listener_vrc_description'; $attr=array('maxlength'=>1500, 'style'=>'height: 110px;  max-height: 300px; resize: vertical;');
printf($tpl,'col-md-12', $form->textarea(array($table_prefix,$key),$labels[$key],$data->{$key},$types[$key], $attr));

$key = 'listener_dcm_price'; $attr=array('maxlength'=>13);
printf($tpl,'col-md-12', $form->text(array($table_prefix,$key),$labels[$key],$data->{$key},$types[$key], $attr));

echo '
<div class="col-md-12">
	<div class="input-group">
		<span class="input-group-btn">
			<span class="btn btn-primary btn-file">
				Browse… <input type="file" name="listener_vrc_image" accept="image/x-png, image/gif, image/jpeg">
			</span>
		</span>
		<input type="text" class="form-control" readonly="">
	</div>
</div>';
if(!empty($data->listener_vrc_image)):
	$thumb = ImagePlugin::resize($pathImgs  . $data->listener_vrc_image, trim($pathImgs . 'thumbs','/'), 320, 240, false);
	printf($tpl,'col-md-12 text-center', tag::img($thumb, '', ''));
endif;
printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));

echo $form->close();


