<?php 
$errors = array();
$msg = '';
if(count($errors) > 0): 
	$msg = '<ul>';
	foreach($errors as $k=>$v) $msg .= sprintf('<li>%s</li>',$v);
	$msg .= '</ul>';
endif;
printf('<div class="warnings hide">%s</div>',H::warningBox($msg, 'max-800 center','Atenção', count($errors) < 1));

echo '<form name="busca" action="" id="form-busca" class="default-form" style="margin: 20px 20px; 0px 20px;" method="get">';

$form = new form();
$val = isset($_GET['listener_int_id']) ? $_GET['listener_int_id'] : null;
$options = array(array('','Selecione um atendente'));
foreach($listeners as $l)
	$options[] = array($l->listener_int_id, $l->listener_vrc_name);

$field = $form->select(array('listener_int_id'), 'Atendente', $val, $options, 'not_null', array(), array('class'=>'hide'));
printf('<div class="col-md-4">%s</div>', $field);
echo '
	<div class="col-md-4" style="padding: 0;">';
	
printf('
	<div class="col-xs-5" style="margin-bottom: 3px;padding-right: 0;">
			<label for="field-busca_operacao-dte_de" class="required hide">Data Início</label>
			<input type="text" name="dte_start" id="field-busca_operacao-dte_de" value="%s" class="date not_null hasDatepicker number_mask" placeholder="xx/xx/xxxx" mask="**/**/****">
	</div>',
	( isset($_GET['dte_start']) ? $_GET['dte_start'] : date('d/m/Y'))
);
	
printf('
	<div class="col-xs-2" style="font-size: 1.4em;text-align: center;"><span class="fa fa-arrows-h">&nbsp;</span></div>
	<div class="col-xs-5" style="margin-bottom: 3px;padding-left: 0;">
			<label for="field-busca_operacao-dte_ate" class="required hide">Data Fim</label>
			<input type="text" name="dte_end" id="field-busca_operacao-dte_ate" value="%s" class="date not_null hasDatepicker number_mask" placeholder="xx/xx/xxxx" mask="**/**/****">
			</div>
	</div>',
	( isset($_GET['dte_end']) ? $_GET['dte_end'] : date('d/m/Y'))
);


echo '
	<div class="col-md-4" style="padding: 0;">
	<button type="submit" class="btn btn-primary col-xs-4">Buscar</button>
	</div>
	<div class="clear"></div>
';

if(!$results):
	printf('<div class="max-800 center" style="padding-top: 20px;" >%1$s%2$s%1$s</div>',
		PHP_EOL, H::msgBox('Selecione o periodo e o atendente!', true, H::INFO)
	);
elseif(!$summary):
	printf('<div class="max-800 center" style="padding-top: 20px;" >%1$s%2$s%1$s</div>',
		PHP_EOL, H::msgBox('Nenhum resultado encontrado!', true, H::WARNING)
	);
else:
	printf('<fieldset><legend>Resumo do Atendente: %s</legend>' . PHP_EOL, $results->listener_vrc_name);
	$rows = '';
	
	$params = array(
		'profit_total' => 'Lucro no periodo ',
		'minutes_pause' => 'Em pausa ',
		'minutes_active' => 'Em atendimento ',
		'minutes' => 'Em atividade ',
		'days' => 'Total de dias no periodo ',
		'profit_day' => 'Lucro médio por dia '
	);
	$tpl = '
			<tr><td><strong>%s</strong></td><td>%s</td></tr>';
	$rows .= sprintf($tpl, 'Periodo', sprintf('De %s até %s', $_GET['dte_start'], $_GET['dte_end']));
	foreach($params as $p=>$lb):
		if(in_array($p, array('profit_day', 'profit_total')))
			$results->{$p} = 'R$ '.$results->{$p};
		if(in_array($p, array('minutes_pause', 'minutes_active', 'minutes')))
			$results->{$p} = $results->{$p} . ' minutos';
		if(in_array($p, array('days')))
			$results->{$p} = $results->{$p} . ' dias';
		
		$rows .= sprintf($tpl, $lb, $results->{$p});
	endforeach;
	
	
	printf('
		<div class="box-body table-responsive" style="padding: 10px;margin-bottom: -20px;margin: 0 auto; width: 450px;">
			<div class="table-container">
				<table class="table table-bordered">
				<tbody role="alert">
					%s
				</tbody>
				</table>
			</div>
		</div>',
	$rows
	);
	
	echo '</fieldset>';
	echo '<fieldset><legend>Resumo diário</legend>' . PHP_EOL;
	echo '<div class="box-body table-responsive" style="padding: 10px;margin-bottom: -20px;">' . PHP_EOL;
	$grid = new gridView();
	 
	$params = array(
		'room_dtt_day' => 'Data',
		'opening' => 'Chats Abertos no dia',
		'total_profit' => 'Lucro total'
	);
	foreach($params as $col=>$lb)
		$grid->addColumn(new gridViewColumn($col, $lb));
		
	echo $grid->renderTable($summary_day);

	echo '</div>';
	echo '</fieldset>';

	echo '<fieldset><legend>Resumo detalhado</legend>' . PHP_EOL;
	echo '<div class="box-body table-responsive" style="padding: 10px;">' . PHP_EOL;
	$grid = new gridView();
	 
	$params = array(
		'room_dtt_open_formated' => 'Data - Hora',
		'customer_vrc_name' => 'Nome do Cliente',
		'room_int_minutes_pause' => 'Minutos em pausa',
		'room_int_minutes_active' => 'Minutos em atividade',
		'room_int_minutes' => 'Minutos em atendimento',
		'room_dcm_profit' => 'R$'
	);
	foreach($params as $col=>$lb)
		$grid->addColumn(new gridViewColumn($col, $lb));
		
	echo $grid->renderTable($summary);

	echo '</div>';
	echo '</fieldset>';
endif;	

echo '</form>';