<?php
printf('<input type="hidden" id="field-comment-target" name="field-comment-target" value="#listener-chat" />');
printf('<input type="hidden" id="field-comment_bgn_id" name="field-comment_bgn_id" value="0" />');
printf('<input type="hidden" id="field-sound" name="sound" value="%s" />', URL::root() . 'files/beep_2');

if(!!$message):
	echo '<br/><br/>';
	echo H::msgBox($message, false, $status);
else:
	?>
	<br/>
	<div class="max-800 center">
		
		<div class="panel panel-primary" id="listener-chat-panel">
			<div class="panel-heading">
				<h4>
					<i class="fa fa-comments-o"></i>  
					<span>Chat</span>
					<span class="pull-right">
						Minutos Restantes: 
						<span id="time_avaliable"></span>
					</span>
				</h4>
			</div>
			
			<div class="panel-body" id="listener-chat-comments-panel">
				<div class="panel-block offline-msg-box chat-msg-box hide" style="margin-top: 50px;">
					<?php echo H::msgBox('<h3>Você está offline.</h3>', false, H::DANGER); ?>
					<div class="clear"></div>
				</div>
				
				
				<div class="panel-block waiting-msg-box chat-msg-box hide" style="margin-top: 10px;">
					<?php echo H::msgBox('
					<h3>O cliente está solicitando atendimento.</h3>
			
					<button class="btn btn-primary btn-block" id="btn-start-chat">Clique aqui para iniciar o atendimento.</button><br/>', false, H::INFO);?>
					<div class="clear"></div>
				</div>
				
				<div class="panel-block paused-msg-box chat-msg-box hide" style="margin-top: 50px;">
					<?php echo H::msgBox('<h3>A conexão com o cliente foi perdida.</h3>
					<h4>Os minutos inativos não serão contados.</h4>
					<h4><a href="#" class="btn-close-chat">Se não quiser esperar clique aqui para finalizar o atendimento</a>.</h4>' , false, H::DANGER); ?>
					<div class="clear"></div>
				</div>
								
				<ul class="chat hide" id="listener-chat-comments">
				
				</ul>
			</div>
			<div class="panel-footer">
				<div class="input-group" style="width: 100%">
					<textarea id="field-chat-comment" type="text" class="col-xs-12 form-control" maxlength='1200' placeholder="Escreva sua mensagem aqui..." style="resize: none;"></textarea>
				</div>
				<div  style="width: 100%;margin-top: 5px;">
					<button class="btn btn-success pull-rigth" id="btn-chat" style="float: right;">
						<i class="fa fa-comment-o"></i> Enviar
					</button>
					<button class="btn btn-danger pull-rigth btn-close-chat" style="float: right;margin-right: 10px;">
						<i class="fa fa-sign-out"></i> Finalizar Conversa
					</button>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
<?php
endif;