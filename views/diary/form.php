<?php 
$form = new form();


echo $form->sucessBox(@$isSaved, 'max-800 center');
echo $form->warningBox($errors,'max-800 center');


echo $form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center'));

$tpl = '<div class="%s">%s</div>';

$key = 'diary_dte_date';
$data->{$key} = CData::format('d/m/Y', $data->{$key});
$inf = $info_label->{$key};
printf($tpl,'col-md-4', $form->text(array($table_prefix,$key),$inf->label,$data->{$key},$inf->type));

$key = 'diary_tme_time';
$inf = $info_label->{$key};
$data->{$key} = CData::format('H:i', $data->{$key});
printf($tpl,'col-md-3', $form->text(array($table_prefix,$key),$inf->label,$data->{$key},$inf->type,array('maxlength'=>5)));


$key = 'diary_emn_status';
$inf = $info_label->{$key};
$opt = array(array('OPEN','ABERTO'),array('CLOSED','FECHADO'),array('PENDING','PENDENTE'));
printf($tpl,'col-md-5',$form->select(array($table_prefix,$key),$inf->label,$data->{$key},$opt,$inf->type));

echo '<div class="clear"></div>';

$key = 'diary_vrc_event';
$inf = $info_label->{$key};
printf($tpl,'col-md-12', $form->text(array($table_prefix,$key),$inf->label,$data->{$key},$inf->type, array('maxlength'=>255)));


$key = 'diary_vrc_description';
$inf = $info_label->{$key};
printf($tpl,'col-md-12', $form->textarea(array($table_prefix,$key),$inf->label,$data->{$key},$inf->type, array('maxlength'=>1000)));


printf('<div class="col-md-12">%s<div class="clear"></div></div><br/>', $form->submit('Salvar','btn btn-primary button-right'));

echo $form->close();
?>
