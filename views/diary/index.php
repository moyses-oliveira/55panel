<div style="height:600px;" class="col-md-12">
<div style="margin:20px;" id='calendar'></div>
</div>
<div class="clear" />
<script>
$(document).ready(function () {

	$('#calendar').fullCalendar({
		defaultDate: new Date(),
		editable: false,
		lang: 'pt-br',
                height:500,
		eventLimit: true,
		events: '<?php echo H::link(H::module(), 'eventos'); ?>'
	});
});
function calendar_click(elm)
{
	msgBoxStatic.load.content(elm);
	return false;
}
</script>
