<ul class="nav nav-tabs">
  <?php
	$menu = array();
	//$menu[] = array($action, $title, $allow);
	$menu[] = array('index','Calendário', true);
	$menu[] = array('create', 'Novo', true);
	$menu[] = array('update', 'Editar', H::action() == 'update');
	foreach($menu as $act):
		if(!$act[2]) continue;
		$link = H::link(H::module(), $act[0], isset($act[3]) ? $act[3] : '');
		
		$class = isset($act[4]) ? sprintf('class="%s"', $act[4]) : '';
		if(H::action() == $act[0]) 
			printf('<li class="active"><a href="#" onclick="return false;">%s</a></li>', $act[1]);
		else
			printf('<li %s><a href="%s">%s</a></li>',$class, $link, $act[1]);
			
	endforeach;
?>
</ul>