<?php
printf('
	<div style="margin-top:10px;margin-bottom:10px;" class="text-right">
		<a class="btn btn-primary" href="%s">Editar</a>
		<a href="%s" class="delete btn btn-primary">Excluir</a>
	</div>'
	, H::link(H::module(), 'update', H::cod())
	, H::link(H::module(), 'delete', H::cod())
);
?>
<div class='max-800 center view'>
    <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
			$opt = array('OPEN'=>'ABERTO','CLOSED'=>'FECHADO','PENDING'=>'PENDENTE');
            foreach ($info_label as $k=>$inf):
				if(empty($inf->label)) continue;
				if($k == 'diary_vrc_description') continue;
				if($k == 'diary_emn_status'):
					$data->{$k} = $opt[$data->{$k}];
				endif;
				
				printf('<tr class="grid"> 
					<td><strong>%s</strong>: %s</td>
				</tr>', 
					$inf->label, 
					H::limit($data->{$k}, 200)
				);
				
            endforeach;
			$k = 'diary_vrc_description';
			$inf = $info_label->{$k};
			printf('<tr class="grid"> 
					<td><strong>%s</strong>:<br/>%s</td>
				</tr>',
				$inf->label, 
				$data->{$k}
			);
            ?>
        </tbody>
    </table>
	
</div>

<script type="text/javascript">
    $('a.delete').click(function () {
        console.log(URI);
        msgBoxStatic.confirm.redirect('Atenção', 'Você deseja excluir o item?', $(this).attr('href') + '?curpage=' + URI);
        //console.log($(this).attr('href'));
        return false;
    });
</script>
