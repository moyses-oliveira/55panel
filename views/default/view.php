<?php 
$rows = '';
foreach($view_params as $K):
	$rows .= sprintf('
		<tr class="grid"> 
			<td>%s</td>
			<td>%s</td>
		</tr>',
	$labels[$K],
	$data[0]->{$K}
);
endforeach;
printf('
<div class="max-800 center view">
  <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
    <tbody role="alert" aria-live="polite" aria-relevant="all">
		%s
    </tbody>
  </table>
</div>
',
$rows
);