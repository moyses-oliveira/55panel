<div class="error-page center" style="max-width: 600px;">
	<h1 class="headline text-danger text-center"> 403</h1>
	<div class="error-content text-center">
		<h3><i class="fa fa-warning text-yellow"></i> Ops! </h3>
		<h3>Você não tem permissão para acessar esta página.</h3>
	</div>
</div>