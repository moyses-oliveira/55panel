<div class="error-page center" style="max-width: 600px;">
	<h1 class="headline text-info text-center"> 404</h1>
	<div class="error-content text-center">
		<h3><i class="fa fa-warning text-yellow"></i> Ops! </h3>
		<h3>Página não encontrada!</h3>
	</div>
</div>