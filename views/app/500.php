<div class="error-page center" style="max-width: 600px;">
	<h1 class="headline text-danger text-center"> 500</h1>
	<div class="error-content text-center">
		<h3><i class="fa fa-warning text-yellow"></i> Erro</h3>
		<h3>Ocorreu um erro ao tentar realizar uma operação, entre em contato com a central.</h3>
	</div>
</div>
<div class="error-page max-800 center">
<?php 
if(count($errors) > 0): 
	$msg = '<ul>' . PHP_EOL;
	if(!DEBUG && isset($errors['message']))
		unset($errors['message']);
		
	foreach($errors as $k=>$v) $msg .= sprintf('<li><strong>%s</strong>: %s</li>',$k,$v) . PHP_EOL;
	$msg .= '</ul>';
	echo H::warningBox($msg, '', 'Warnings', false); 
endif;
?>
</div>