<div class="box-img" style="background-image: url('{thumb}');">
	<input name="__[{input_key}][pk]" class="field_pk" type="hidden" value="{input_key}" />
	<input name="__[{input_key}][image]" class="field_img" type="hidden" value="{img}" />
	<input name="__[{input_key}][name]" class="field_name" type="hidden" value="{name}" />
	<input name="__[{input_key}][thumb]" class="field_thumb" type="hidden" value="{thumb}" />
	<div class="msg">
		<span class="loading"><i class="fa fa-refresh fa-spin"></i></span>
		<div class="info">Deseja excluir?</div>
		<div>
			<span class="btn btn-default bg-green" >Sim</span>
			<span class="btn btn-default bg-red" >Não</span>
		</div>
	</div>
	<div class="actions" >
		<div>
			<a href="#" title="Selecionar" class="check"><i class="fa fa-check"></i></a>
			<a href="#" title="Remover" class="delete pull-right"><i class="fa fa-trash"></i></a>
			<div class="clear"></div>
		</div>
	</div>
	<div class="name"><a href="{img}" title="{name}" _title="{name}" alt="{name}" >{name}</a></div>
</div>