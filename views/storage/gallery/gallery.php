<?php
$isUpdate = URL::getVar('update');
$form = new form();

# Inicia a div principal se a variavel modal existe;
if(!$isUpdate) echo '<div id="file-manager-content" class="box-body table-responsive" style="margin: 0 auto;padding: 10px;height: 380px; overflow: auto;">';

$prefix = 'image';
#echo $form->warningBox($errors,'max-800 center');

echo $form->open('form-select-dir', URL::atual(), 'form-select-dir', array('class' => 'default-form'));
	printf('<div class="%s">%s</div>', 'col-md-8 col-xs-12', $form->select(array($prefix, 'componente_site_int_componente'), 'Galeria', $path ? : '', $directories, 'string'));
echo '
<div class="col-md-4 col-xs-8">
	<span id="add_directory" class="btn btn-default" style="width:100%;">
		<i class="fa fa-plus"></i>&nbsp;Adicionar Pasta
	</span>
</div>';
echo $form->close();

echo $form->open('form-directory', URL::atual(), 'form-directory', array('class' => 'default-form', 'style'=>'display: none;'));
printf('<div class="col-md-7 col-xs-12">%s</div>', $form->text(array('add_directory', 'name'), 'Nome do Diretório', '', ''));
echo '
	<div class="col-md-3 col-xs-8">
		<button id="btn-submit-directory" type="submit" class="btn btn-primary" style="width:100%;">
			<i class="fa fa-plus"></i>&nbsp;Adicionar
		</button>
	</div>
	<div class="col-md-2 col-xs-4">
	<a id="btn-close-add-dir" class="btn btn-default delete" style="width: 100%;" href="%s" title="cancelar">
		<i class="fa fa-close"></i>
	</a>
	</div>
';
echo $form->close();



echo '
<div class="file_manager">
	<input id="file-input" name="imagens[]" type="file" multiple class="col-md-12 file-loading" />
</div>
<br/>';


$thumb_tpl = file_get_contents('views/storage/gallery/gallery-item.tpl');
echo '<div id="file-grid" class="gallery-list">';
	
printf('<div class="folder-info" %s>', !!$lista ? 'style="display: none;"' : '');
	echo H::msgBox('<h4 style="color: #555;">Esta pasta está vazia.</h4>', false);
	if(!!URL::getVar('p')):
		$rmDirLink = URL::atual() . '?rmdir=1';
		echo H::msgBox('
			<h4 style="color: #555;"></h4>
			<div>
				<h4 style="color: #555;">Deseja excluir esta pasta?</h4>
				<span class="btn btn-default bg-red" id="rmdir" href="' . $rmDirLink . '" >Sim, eu desejo excluir.</span>
			</div>
		', true, H::DANGER);
	endif;
echo '</div>';

foreach($lista as $img)
	echo str_replace(array('{input_name}','{input_key}', '{name}', '{img}', '{thumb}'), array('image', md5($img->name), $img->name, $img->img, $img->thumb), $thumb_tpl);

echo '<div class="clear"></div></div>';
	
# Finaliza a div principal se a variável modal existe;
if(!$isUpdate && H::module() != 'storage.gallery'):
	echo '</div>';
	echo '
	<div style="padding: 7px 0;border-top: 1px dotted #ddd;">
		<a href="#" title="Remover" id="add-to-list" class="btn btn-default button-right bg-green">
			<i class="fa fa-plus"></i>&nbsp;Adicionar Imagens
		</a>
		<div class="clear"></div>
	</div>';
endif;
$p = URL::getVar('p');
$page = URL::getVar('page');
$refresh_url = URL::root() . implode('/',URL::urls()) . '/modal:1/update:1' . (!$p ? '' : '/p:' . $p) . (!$page ? '' : '/page:' . $page);
?>
<script type="text/javascript">
var oURL = {
	select_path: '<?php echo URL::root() . implode('/',URL::urls()) . '/modal:1/update:1';?>',
	refresh_url: '<?php echo $refresh_url;?>',
	current: '<?php echo URL::atual();?>'
};
$(document).ready(function(){
	staticGalleryPlugin.INIT('<?php echo URL::getVar('p');?>');
});
</script>