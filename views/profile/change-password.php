<?php 
$form = new form();
$msg = '';

echo $form->sucessBox($isUpdated, 'max-800 center');
echo $form->warningBox($errors,'max-800 center');

printf($form->open($table_prefix,URL::atual(),'form-'.$table_prefix, array('class'=>'default-form max-800 center')));
$tpl = '<div class="%s">%s</div>';

$key = 'old_pass'; $val = isset($_POST[$table_prefix][$key]) ? $_POST[$table_prefix][$key] : '';
printf($tpl,'col-md-12', $form->pass(array($table_prefix,$key),'Senha atual', $val, 'not_null password'));

$key = 'new_pass'; $val = isset($_POST[$table_prefix][$key]) ? $_POST[$table_prefix][$key] : '';
printf($tpl,'col-md-12', $form->pass(array($table_prefix,$key),'Nova senha', $val, 'not_null password'));

$key = 'rnew_pass'; $val = isset($_POST[$table_prefix][$key]) ? $_POST[$table_prefix][$key] : '';
printf($tpl,'col-md-12', $form->pass(array($table_prefix,$key),'Repita a nova senha', $val, 'not_null password'));

printf('<div class="col-md-12">%s<div class="clear"></div></div>', $form->submit('Salvar','btn btn-primary button-right'));

printf($form->close());


 